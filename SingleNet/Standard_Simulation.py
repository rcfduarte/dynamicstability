import sys
sys.path.append('../')
import nest
import numpy as np
import pylab as pl
import itertools
import nest.raster_plot
from CommonModules.Parameters import *
from CommonModules.Plotting_routines import *
from CommonModules.Analysis_fcns import *
from CommonModules.Auxiliary_fcns import *

PD = set_params()
pars_dict = set_params_derived(PD)

########################################################################
# Extract Parameters
########################################################################
for k, v in pars_dict.items():
	globals()[k] = v
	for k1, v1 in (globals()[k]).items():
		globals()[k1] = v1

########################################################################
# Reset NEST kernel
########################################################################
nest.ResetKernel()
nest.SetKernelStatus({'resolution': dt,
                      'print_time': True,
                      'data_path': data_path,
                      'data_prefix': data_label,
                      'overwrite_files': True,
                      'local_num_threads': proc_per_node})
########################################################################
# 					CREATE NEURONS AND DEVICES
########################################################################
tp_dict = set_topology(pars_dict)
weights = set_synapses(pars_dict)
tp_dict = connect_network(weights, pars_dict, tp_dict)

E_neurons = tp_dict['E_neurons']
I_neurons = tp_dict['I_neurons']

#-----------------------------------------------------------------------
# Randomize initial Vm
#-----------------------------------------------------------------------
nest.SetStatus(E_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nE))))
nest.SetStatus(I_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nI))))

#-----------------------------------------------------------------------
# Create Devices
#-----------------------------------------------------------------------
SpkDetector = nest.Create('spike_detector')
nest.SetStatus(SpkDetector, {'start': PlasticityT+TransientT, 'record_to': ['file'],
                             'to_file': True, 'to_memory': False,
                             'label': 'SpkDetector'})
nest.CopyModel('multimeter', 'singleNeuron_multimeter')
nest.CopyModel('multimeter', 'population_multimeter')
nest.SetDefaults('singleNeuron_multimeter', {'record_from': ['g_ex', 'g_in', 'V_m'],
                                'record_to': ['file'], 'to_file': True,
                                'interval': dt,
                                'to_memory': False,
                                'label': 'SingleNeuron_multimeter',
                                'withtime': True, 'start': PlasticityT})
nest.SetDefaults('population_multimeter', {'record_from': ['V_m'],
                                'record_to': ['accumulator'], 'start': PlasticityT+TransientT})

#single_multimeter = nest.Create('singleNeuron_multimeter')
multiple_mults = nest.Create('singleNeuron_multimeter', 1, {'record_from': ['V_m'], 'label':
	'multiple_multimeter'})
neuronIdx = np.random.randint(min(itertools.chain(*[E_neurons, I_neurons])), max(itertools.chain(*[E_neurons,
                                                                                                  I_neurons])), 1)
population_multimeter = nest.Create('population_multimeter')

PG = nest.Create('poisson_generator')
nest.SetStatus(PG, {'rate': Ext_rate * (pEE * nE)})

########################################################################
# 							CONNECT
########################################################################
connect_standard_devices(E_neurons, I_neurons, g_bar_E, delays, PG, SpkDetector, population_multimeter)
#nest.Connect(single_multimeter, neuronIdx)
nest.DivergentConnect(multiple_mults, E_neurons)
#for ctrr, nnn in enumerate(multiple_mults):
#	nest.Connect([nnn], [E_neurons[ctrr]])

########################################################################
#						      SIMULATE
########################################################################
'''
wEE = np.zeros([len(E_neurons), len(E_neurons)])
wEI = np.zeros([len(I_neurons), len(E_neurons)])

aEE = nest.GetConnections(E_neurons, E_neurons)
aEI = nest.GetConnections(I_neurons, E_neurons)
c_EE = nest.GetStatus(aEE, keys='weight')
c_EI = nest.GetStatus(aEI, keys='weight')

for idx, n in enumerate(aEE):
	wEE[n[0] - min(E_neurons), n[1] - min(E_neurons)] += c_EE[idx]
for idx, n in enumerate(aEI):
	wEI[n[0] - min(I_neurons), n[1] - min(E_neurons)] += c_EI[idx]

with open(data_path + data_label + 'Weights_EE_Step={0}.npy'.format(str(0)), 'w') as fp1:
	np.save(fp1, wEE)
with open(data_path + data_label + 'Weights_EI_Step={0}.npy'.format(str(0)), 'w') as fp2:
	np.save(fp2, wEI)
'''
nest.Simulate(PlasticityT+TransientT+SimT)
'''
wEE = np.zeros([len(E_neurons), len(E_neurons)])
wEI = np.zeros([len(I_neurons), len(E_neurons)])

aEE = nest.GetConnections(E_neurons, E_neurons)
aEI = nest.GetConnections(I_neurons, E_neurons)
c_EE = nest.GetStatus(aEE, keys='weight')
c_EI = nest.GetStatus(aEI, keys='weight')

for idx, n in enumerate(aEE):
	wEE[n[0] - min(E_neurons), n[1] - min(E_neurons)] += c_EE[idx]
for idx, n in enumerate(aEI):
	wEI[n[0] - min(I_neurons), n[1] - min(E_neurons)] += c_EI[idx]

with open(data_path + data_label + 'Weights_EE_Step={0}.npy'.format(str('End')), 'w') as fp1:
	np.save(fp1, wEE)
with open(data_path + data_label + 'Weights_EI_Step={0}.npy'.format(str('End')), 'w') as fp2:
	np.save(fp2, wEI)
'''
#########################################################################
# Plot
#########################################################################
nest.raster_plot.from_device(SpkDetector, hist=True)
pl.show(block=False)
'''
spk_data = extract_data_fromfile(nest.GetStatus(SpkDetector)[0]['filenames'])
#analogue_data = extract_data_fromfile(nest.GetStatus(single_multimeter)[0]['filenames'])

Vms = nest.GetStatus(population_multimeter)[0]['events']['V_m']/SimT

analogue_vars = dict()
analogue_vars['Vms'] = Vms
analogue_vars['g_e'] = analogue_data[:, 2]
analogue_vars['g_i'] = analogue_data[:, 3]
analogue_vars['V'] = analogue_data[:, 4]

neuron_ids = np.random.randint(min(E_neurons), max(E_neurons), 500.)
time_interval = [PlasticityT+TransientT+SimT-1100., PlasticityT+TransientT+SimT-100.]

plot_singlenet_activity(spk_data, analogue_vars, neuron_ids, time_interval, SimT+TransientT+PlasticityT, neuronIdx)

Mean_fRate, Std_fRate, CV_ISI, CC = compute_spiking_stats(SpkDetector, E_neurons, I_neurons, 500, time_interval,
                                                        display=True, splitEI=False)

#fig = pl.figure()
#pl.hist(CC['pearson'], 100)
#pl.show(block=False)
'''