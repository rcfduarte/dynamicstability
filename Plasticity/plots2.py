import pylab as pl
import numpy as np

fig1 = pl.figure()

ax1 = fig1.add_subplot(121)
ax1.plot(final_rates, 'b', linewidth=3)
ax1.plot(range(len(final_rates)), np.ones(len(final_rates))*5., '--k', linewidth=2)
#ax1.set_xscale('log')
ax1.set_title('Output Rate [Final]')
ax1.set_ylabel('Mean Rate [Hz]')
ax1.set_xticks(np.arange(0., len(x), 1.))
ax1.set_xticklabels(x[::1])
#ax1.set_xticklabels(x)
ax1.set_xlabel('alpha = (1/w) + ')
ax1.set_ylim(0., 10.)

ax2 = fig1.add_subplot(122)

Ws = []
for n in range(len(x)):
	Ws.append(np.mean(final_weights[n])*gBarEx)

ax2.plot(Ws, 'r', linewidth=3)
ax2.plot(range(len(final_rates)), np.ones(len(final_rates))* 1.8, '--k', linewidth=2)
#ax2.set_xscale('log')
ax2.set_ylim(0., 5.)
ax2.set_title('Synaptic Weights [Final]')
ax2.set_ylabel('Mean W [nS]')
ax2.set_xticks(np.arange(0., len(x), 1.))
ax2.set_xticklabels(x[::1])
ax2.set_xlabel('alpha = (1/w) + ')

pl.show()