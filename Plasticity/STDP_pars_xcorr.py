__author__ = 'neuro'
import nest
import numpy as np
import pylab as pl
import time
import itertools
import nest.raster_plot
import NeuroTools.signals_mine as signals
from Auxiliary_fcns import *

###########################################################################
# Analyse weight and rate dynamics in the different plasticity conditions
# - single output neuron...
#==========================================================================

startTime = time.time()
print 'Setting Parameters...'

### Simulation
TransientT = 200000.
dt = 0.1							# Simulation time step [ms]
AnalysisT = 100000.                 # Time for data acquisition [ms]
Plasticity = 'None'
duration = TransientT+AnalysisT
nPairs = 800

### Network
N = 10000.
nE = 0.8*N
nI = 0.2*N
E_rate = 5.
I_rate = 5.

### Neuron
model = 'iaf_cond_exp'		      # neuron model
g_leak = 16.7				      # [nS] leak conductance (other value: 10.nS)
E_L = -70.      			      # [mV] RMP (-60.)
V_th = -50. 					  # [mV] spiking threshold
C_m = 250.      				  # [pF] membrane capacitance (200.)
I_b = 0.					      # [pA] external current (200.)
tau_ref = 2.					  # [ms] refractory time
V_reset = -60.					  # [mV] reset potential

### Synapses
E_AMPA = 0.						  # Excitatory reversal potential [mV]
E_GABA = -80.					  # Inhibitory reversal potential [mV]
tau_E = 5.						  # Excitatory synaptic time contant [ms]
tau_I = 10.						  # Inhibitory synaptic time constant [ms]

### Connectivity
eps = 0.1                         # connection density
gBarEx = 2.4				          # Scaling factor of the E synaptic conductances
inh_scale = -13.                   # balance parameter
gBarIn = inh_scale*gBarEx		  # Scaling factor of the I synaptic conductances [nS]
mu_E = 1.                         # mean of Gaussian distribution (wE)
sig_E = 0.25                      # variance of Gaussian...
mu_I = 1.
sig_I = 0.25

### Input
Ext_rate = 5.

### Plasticity
eta = 1e-2							# Learning Rate
ro = 5.							# Target Rate [Hz]
tau_iSTDP = 20.						# Time window of the learning rule [ms]
alpha = 2.*ro*(tau_iSTDP*0.001)		# PreSynaptic offset
w_max = 100.*(-gBarIn)

tau_eSTDP = 20.					  # [ms] - time window...
lamb = 0.024261					      # [pS] - average amount of potentiation after one pairing (from
# experimental
# data)
alpha_E = 1340.    		      # [pS] - average amount of depression after one pairing (tune)
W_MAX = 100.*gBarEx                 # max weight value


########################################################################
# Preliminaries
########################################################################
nest.ResetKernel()
nest.SetKernelStatus({'resolution': dt,
					  'print_time': True,
					  'local_num_threads': 4})

# Initial weight distributions
wE = sig_E*np.random.randn(1, int(eps*nE))+mu_E
wI = sig_I*np.random.randn(1, int(eps*nI))+mu_I


Build_time = time.time()
print 'Elapsed Time {0} [ms]'.format((Build_time-startTime)*1000.)

########################################################################
# Create Neuronal Populations
########################################################################
Build_time = time.time()
print 'Setting up...'

nest.CopyModel('iaf_cond_exp', 'Out_neuron')
nest.SetDefaults('Out_neuron', {
	'C_m': C_m, 'E_L': E_L, 'E_ex': E_AMPA,
	'E_in': E_GABA, 'V_m': (V_th - E_L) * np.random.random_sample() + E_L,
	'V_reset': V_reset,	'V_th': V_th, 'g_L': g_leak, 't_ref': tau_ref,
	'tau_syn_ex': tau_E, 'tau_syn_in': tau_I,
	'tau_minus': tau_iSTDP})

Out_neuron = nest.Create('Out_neuron')

nest.SetDefaults('inh_stdp_synapse', {'Wmax': w_max, 'Wmin': 0.,
                                      'eta': eta, 'ro': ro, 'alpha': alpha,
                                      'tau_iSTDP': tau_iSTDP,
                                      'scale': gBarIn})
nest.SetDefaults('stdp_synapse', {'Wmax': W_MAX, 'alpha': alpha_E,
                                  'lambda': lamb, 'mu_minus': 1.,
                                  'mu_plus':0., 'scale': gBarEx,
                                  'tau_plus': tau_eSTDP})

# Create poisson generators to generate the input spikes
SpkGenE = nest.Create('poisson_generator', 1, {'rate': E_rate})
SpkGenI = nest.Create('poisson_generator', 1, {'rate': I_rate})
SpkGenX = nest.Create('poisson_generator', 1, {'rate': Ext_rate*(eps*nE)})

PG = nest.Create('poisson_generator',1, {'rate':5.})

# Create parrot neurons to deliver the input
E_Input_neurons = nest.Create('parrot_neuron', int(nE))
I_Input_neurons = nest.Create('parrot_neuron', int(nI))

Toy_neuron = nest.Create('parrot_neuron', 1)

# Connect
nest.DivergentConnect(SpkGenE, E_Input_neurons, 1., 1., 'static_synapse')
nest.DivergentConnect(SpkGenI, I_Input_neurons, 1., 1., 'static_synapse')

if 'eSTDP' in Plasticity:
	nest.RandomConvergentConnect(E_Input_neurons, Out_neuron, int(eps*nE), list(wE[0]), 1.5, 'stdp_synapse')
else:
	nest.RandomConvergentConnect(E_Input_neurons, Out_neuron, int(eps*nE), list(gBarEx * wE[0]), 1.5, 'static_synapse')
if 'iSTDP' in Plasticity:
	nest.RandomConvergentConnect(I_Input_neurons, Out_neuron, int(eps*nI), list(wI[0]), 1.5,
	                             'inh_stdp_synapse')
else:
	nest.RandomConvergentConnect(I_Input_neurons, Out_neuron, int(eps*nI), list(gBarIn * wI[0]), 1.5, 'static_synapse')

nest.Connect(SpkGenX, Out_neuron, gBarEx, 1.5)
nest.Connect(PG, Toy_neuron, 1., 1.)

# Set recording devices
SpkDetector_Einp = nest.Create('spike_detector')
SpkDetector2 = nest.Create('spike_detector')
SpkDetector_Toy = nest.Create('spike_detector')

nest.SetStatus(SpkDetector2, {'start': TransientT})
nest.SetStatus(SpkDetector_Einp, {'start': TransientT})
nest.SetStatus(SpkDetector_Toy, {'start': TransientT})

nest.Connect(Out_neuron, SpkDetector2)
nest.ConvergentConnect(E_Input_neurons, SpkDetector_Einp)
nest.Connect(Toy_neuron, SpkDetector_Toy)

Build_time2 = time.time()
print 'Elapsed Time {0} [ms]'.format((Build_time2-startTime)*1000.)

##########################################################################
# Simulate
##########################################################################

nest.Simulate(duration)

##########################################################################
# Extract and analyse data:
##########################################################################
OutSpkTimes = nest.GetStatus(SpkDetector2)[0]['events']['times']
NeuronId = nest.GetStatus(SpkDetector2)[0]['events']['senders']
OutSpkTrain = signals.SpikeTrain(OutSpkTimes, t_start=TransientT, t_stop=duration)

E_SpkTimes = nest.GetStatus(SpkDetector_Einp)[0]['events']['times']
E_SpkIds = nest.GetStatus(SpkDetector_Einp)[0]['events']['senders']

tmp = [(E_SpkIds[n], E_SpkTimes[n]) for n in range(len(E_SpkTimes))]
InpSpk_list = signals.SpikeList(tmp, np.unique(E_SpkIds).tolist(), t_start=TransientT, t_stop=duration)

# Compute correlation coefficient:
tmp = np.unique(E_SpkIds)
inp_idxs = np.random.randint(min(tmp), max(tmp), nPairs)

F1 = OutSpkTrain.filter_states(signals.bi_exponential_kernel, 1., [50., 250.])
tmp = InpSpk_list.spiketrains

CC = np.zeros(nPairs)
for idx, n in enumerate(inp_idxs):

	S2 = tmp[n]
	F2 = S2.filter_states(signals.bi_exponential_kernel, 1., [50., 250.])

	CC[idx] = np.sum(F1 * F2) / np.sqrt(np.sum(F1 * F1) * np.sum(F2 * F2))

# Compute cross-correlogram

Corrs = []
for idx, n in enumerate(inp_idxs):

	S2 = tmp[n].spike_times
	Corrs.append(correlogram(OutSpkTimes, S2.tolist(), width=500., bin=1.))


#################################################################################
# Plot
#################################################################################
fig1 = pl.figure()
ax1 = fig1.add_subplot(121)
ax1.plot(np.mean(Corrs, 0), linewidth=2)
ax1.set_xlabel('tau [ms]')
ax1.set_title('Cross-correlogram')
ax1.set_xticks(np.array([0., 125., 250., 375., 500., 725., 850., 1000.]))
ax1.set_xticklabels(['-500', '', '-250','', '0','','250','', '500'])

ax2 = fig1.add_subplot(122)
plt2 = ax2.hist(CC)
ax2.set_xlabel('CC')
ax2.set_title('Pair-Wise Correlation Coefficient')
ax2.annotate(str(np.round(np.mean(CC), 2)), xy=(np.mean(CC), max(plt2[0])), xytext=(np.mean(CC), max(plt2[0])+1),
             arrowprops=dict(facecolor='black', shrink=0.05))

#==================================================================================
# Repeat for the 'Toy' neuron
#==================================================================================
##########################################################################
# Extract and analyse data:
##########################################################################
ToySpkTimes = nest.GetStatus(SpkDetector_Toy)[0]['events']['times']
Id = nest.GetStatus(SpkDetector2)[0]['events']['senders']
ToySpkTrain = signals.SpikeTrain(ToySpkTimes, t_start=TransientT, t_stop=duration)

# Compute correlation coefficient:
F1 = ToySpkTrain.filter_states(signals.bi_exponential_kernel, 1., [50., 250.])
tmp = InpSpk_list.spiketrains

CC = np.zeros(nPairs)
for idx, n in enumerate(inp_idxs):

	S2 = tmp[n]
	F2 = S2.filter_states(signals.bi_exponential_kernel, 1., [50., 250.])

	CC[idx] = np.sum(F1 * F2) / np.sqrt(np.sum(F1 * F1) * np.sum(F2 * F2))

# Compute cross-correlogram

Corrs = []
for idx, n in enumerate(inp_idxs):

	S2 = tmp[n].spike_times
	Corrs.append(correlogram(ToySpkTimes, S2.tolist(), width=500., bin=1.))


#################################################################################
# Plot
#################################################################################
fig2 = pl.figure()
ax3 = fig2.add_subplot(121)
ax3.plot(np.mean(Corrs, 0), linewidth=2)
ax3.set_xlabel('tau [ms]')
ax3.set_title('Cross-correlogram')
ax3.set_xticks(np.array([0., 125., 250., 375., 500., 725., 850., 1000.]))
ax3.set_xticklabels(['-500', '', '-250','', '0','','250','', '500'])

ax4 = fig2.add_subplot(122)
plt4 = ax4.hist(CC)
ax4.set_xlabel('CC')
ax4.set_title('Pair-Wise Correlation Coefficient')
ax4.annotate(str(np.round(np.mean(CC), 2)), xy=(np.mean(CC), max(plt2[0])), xytext=(np.mean(CC), max(plt2[0])+1),
             arrowprops=dict(facecolor='black', shrink=0.05))



pl.show()