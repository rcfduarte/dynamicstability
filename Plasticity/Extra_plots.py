__author__ = 'duarte'
import numpy as np
import pylab as pl
import pickle
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='serif')

with open('PlotSingleNeuronActs1', 'w') as fp:
	data = dict()
	data['Vm4a'] = Vm4
	data['TT4a'] = TT4
	data['OutSpkTimesa'] = OutSpkTimes
	data['E_C4a'] = E_C4
	data['I_C4a'] = I_C4

	pickle.dump(data, fp)

C_m = 250.
g_leak = 16.7


fig = pl.figure()
ax1 = fig.add_subplot(311)
#ax2 = fig.add_subplot(322)
ax3 = fig.add_subplot(312)
#ax4 = fig.add_subplot(324)
ax5 = fig.add_subplot(313)
#ax6 = fig.add_subplot(326)
#ax7 = fig.add_subplot(327)
#ax8 = fig.add_subplot(338)
#ax9 = fig.add_subplot(339)

with open('PlotSingleNeuronActs1', 'r') as fp:
	data = pickle.load(fp)

for k, v in data.items():
	globals()[k] = v

ax1.plot(TT4a, Vm4a, 'k')
for n in OutSpkTimesa:
	if TT4a[0] < n < TT4a[-1]:
		ax1.vlines(n, -50., 0., color='k', linewidth=1.)
ax1.set_xlim(TT4a[0], TT4a[0]+200.)
ax1.set_ylim(-70., -45.)
ax1.set_ylabel(r'V_{m} [mV]', fontsize=12)
ax1.set_xlabel(r'Time [ms]', fontsize=12)

ax3.plot(TT4a, E_C4a/1000, 'b', label='Exc. Currents', linewidth=1)
ax3.plot(TT4a, I_C4a/1000, 'r', label='Inh. Currents', linewidth=1)
#ax3.plot(TT4a, (E_C4a+I_C4a)/1000, 'k', label='Total', linewidth=1)
ax3.set_xlim(TT4a[0], TT4a[0]+200.)
ax3.set_ylim((min(E_C4a)/1000)-1, (max(I_C4a)/1000)+1)
ax3.set_ylabel('Synaptic Currents [nA]', fontsize=10)

ax5.plot(TT4a, C_m/(E_C4a/1000. + I_C4a/1000. + g_leak), color='gray', linewidth=2.)
ax5.set_ylabel(r'\tau_{eff} [ms]', fontsize=12)
ax5.set_xlabel(r'Time [ms]')
ax5.set_xlim(TT4a[0], TT4a[0]+200.)

ax1.grid(True)
ax3.grid(True)
ax5.grid(True)



ax2.plot(TT4b, Vm4b, 'k')
for n in OutSpkTimesb:
	if TT4b[0] < n < TT4b[-1]:
		ax2.vlines(n, -50., 0., color='k', linewidth=1.)
ax2.set_xlim(TT4b[0], TT4b[-1])
ax2.set_ylim(-70., 5.)
ax2.set_ylabel(r'V_{m} [mV]', fontsize=12)
ax2.set_xlabel(r'Time [ms]', fontsize=12)

ax4.plot(TT4b, E_C4b/1000, 'b', label='Exc. Currents', linewidth=1)
ax4.plot(TT4, I_C4/1000, 'r', label='Inh. Currents', linewidth=1)
ax4.plot(TT4, (E_C4+I_C4)/1000, 'k', label='Total', linewidth=1)
ax4.set_xlim(TT4[0], TT4[-1])
ax4.set_ylim((min(E_C4)/1000)-1, (max(I_C4)/1000)+1)
ax4.set_ylabel('Synaptic Currents [nA]', fontsize=10)

ax6.plot(TT4, C_m/(E_C4/1000. + I_C4/1000. + g_leak), color='gray', linewidth=2.)
ax6.set_ylabel(r'\tau_{eff} [ms]', fontsize=12)
ax6.set_xlabel(r'Time [ms]')
ax6.set_xlim(TT4[0], TT4[-1])

pl.show()