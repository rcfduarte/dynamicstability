__author__ = 'duarte'

import nest
import numpy as np
import time
import itertools
import nest.raster_plot

###########################################################################
# Analyse weight and rate dynamics in the different plasticity conditions
# - single output neuron...
#==========================================================================

startTime = time.time()
print 'Setting Parameters...'

### Simulation
TransientT = 2500000.
dt = 0.1							# Simulation time step [ms]
AnalysisT = 100000.                 # Time for data acquisition [ms]
Plasticity = 'eSTDP'
duration = TransientT+AnalysisT
SamplInterval = 1000.

### Network
N = 10000.
nE = 0.8*N
nI = 0.2*N
E_rate = 5.
I_rate = 5.

### Neuron
model = 'iaf_cond_exp'		      # neuron model
g_leak = 16.7				      # [nS] leak conductance (other value: 10.nS)
E_L = -70.      			      # [mV] RMP (-60.)
V_th = -50. 					  # [mV] spiking threshold
C_m = 250.      				  # [pF] membrane capacitance (200.)
I_b = 0.					      # [pA] external current (200.)
tau_ref = 5.					  # [ms] refractory time
V_reset = -60.					  # [mV] reset potential

### Synapses
E_AMPA = 0.						  # Excitatory reversal potential [mV]
E_GABA = -80.					  # Inhibitory reversal potential [mV]
tau_E = 5.						  # Excitatory synaptic time contant [ms]
tau_I = 10.						  # Inhibitory synaptic time constant [ms]

### Connectivity
eps = 0.1                         # connection density
gBarEx = 1.8				          # Scaling factor of the E synaptic conductances
inh_scale = -12.                   # balance parameter
gBarIn = inh_scale*gBarEx		  # Scaling factor of the I synaptic conductances [nS]
mu_E = 1.                         # mean of Gaussian distribution (wE)
sig_E = 0.25                      # variance of Gaussian...
mu_I = 1.
sig_I = 0.25

### Input
Ext_rate = 5.

### Plasticity
eta = 1e-2							# Learning Rate
ro = 5.							# Target Rate [Hz]
tau_iSTDP = 20.						# Time window of the learning rule [ms]
alpha = 2.*ro*(tau_iSTDP*0.001)		# PreSynaptic offset
w_max = 100.*(-gBarIn)

tau_eSTDP = 20.					  # [ms] - time window...
lamb = 0.01     			      # [pS] - average amount of potentiation after one pairing (
# from
# experimental
# data)
alpha_E = 1.5 #0.95 #0.917556     		      # [pS] - average amount of depression after one pairing (tune)
W_MAX = 1.                 # max weight value (ALWAYS SET TO 1!!)


########################################################################
# Preliminaries
########################################################################
nest.ResetKernel()
nest.SetKernelStatus({'resolution': dt,
					  'print_time': True,
					  'local_num_threads': 4})

# Initial weight distributions
wE = sig_E*np.random.randn(1, int(eps*nE))+mu_E
wI = sig_I*np.random.randn(1, int(eps*nI))+mu_I


Build_time = time.time()
print 'Elapsed Time {0} [ms]'.format((Build_time-startTime)*1000.)

########################################################################
# Create Neuronal Populations
########################################################################
Build_time = time.time()
print 'Setting up...'

nest.CopyModel('iaf_cond_exp', 'Out_neuron')
nest.SetDefaults('Out_neuron', {
	'C_m': C_m, 'E_L': E_L, 'E_ex': E_AMPA,
	'E_in': E_GABA, 'V_m': (V_th - E_L) * np.random.random_sample() + E_L,
	'V_reset': V_reset,	'V_th': V_th, 'g_L': g_leak, 't_ref': tau_ref,
	'tau_syn_ex': tau_E, 'tau_syn_in': tau_I,
	'tau_minus': tau_iSTDP})

Out_neuron = nest.Create('Out_neuron')

nest.SetDefaults('inh_stdp_synapse', {'Wmax': w_max, 'Wmin': 0.,
                                      'eta': eta, 'ro': ro, 'alpha': alpha,
                                      'tau_iSTDP': tau_iSTDP,
                                      'scale': gBarIn})
nest.SetDefaults('stdp_synapse', {'Wmax': W_MAX, 'alpha': alpha_E,
                                  'lambda': lamb, 'mu_minus': 1.,
                                  'mu_plus':0., 'scale': gBarEx,
                                  'tau_plus': tau_eSTDP})

# Create poisson generators to generate the input spikes
SpkGenE = nest.Create('poisson_generator', 1, {'rate': E_rate})
SpkGenI = nest.Create('poisson_generator', 1, {'rate': I_rate})
SpkGenX = nest.Create('poisson_generator', 1, {'rate': Ext_rate*(eps*nE)})

# Create parrot neurons to deliver the input
E_Input_neurons = nest.Create('parrot_neuron', int(nE))
I_Input_neurons = nest.Create('parrot_neuron', int(nI))

# Connect
nest.DivergentConnect(SpkGenE, E_Input_neurons, 1., 1., 'static_synapse')
nest.DivergentConnect(SpkGenI, I_Input_neurons, 1., 1., 'static_synapse')

if 'eSTDP' in Plasticity:
	nest.RandomConvergentConnect(E_Input_neurons, Out_neuron, int(eps*nE), list(wE[0]), 1.5, 'stdp_synapse')
else:
	nest.RandomConvergentConnect(E_Input_neurons, Out_neuron, int(eps*nE), list(gBarEx * wE[0]), 1.5, 'static_synapse')
if 'iSTDP' in Plasticity:
	nest.RandomConvergentConnect(I_Input_neurons, Out_neuron, int(eps*nI), list(wI[0]), 1.5,
	                             'inh_stdp_synapse')
else:
	nest.RandomConvergentConnect(I_Input_neurons, Out_neuron, int(eps*nI), list(gBarIn * wI[0]), 1.5, 'static_synapse')

nest.Connect(SpkGenX, Out_neuron, gBarEx, 1.5)

# Set recording devices
SpkDetector = nest.Create('spike_detector')
SpkDetector2 = nest.Create('spike_detector')

nest.SetStatus(SpkDetector2, {'start': TransientT})
nest.SetDefaults('multimeter', {'record_from': ['g_ex', 'g_in', 'V_m'],
								'withtime': True,
								'interval': dt})
Multimeter = nest.Create('multimeter')
nest.Connect(Out_neuron, SpkDetector)
nest.Connect(Out_neuron, SpkDetector2)
nest.Connect(Multimeter, Out_neuron)

Build_time2 = time.time()
print 'Elapsed Time {0} [ms]'.format((Build_time2-startTime)*1000.)

##########################################################################
# Simulate
##########################################################################
## Initialize vectors to store information
Time = np.arange(dt, duration, dt)  # vector of time, in steps of dt
Ex_Currents = Time * 0.				# store the synaptic currents
In_Currents = Time * 0.
E_Syn_Ws = []
I_Syn_Ws = []
Events = []
Rate = np.zeros((1, duration/SamplInterval))
T = np.zeros((1, duration/SamplInterval+1))

#######

Steps = np.arange(0., duration, SamplInterval)
ctr = 0
Events.append(0.)
for i, n in enumerate(Steps):

	nest.SetStatus(SpkGenE, {'rate': E_rate, 'start': Steps[ctr], 'stop': Steps[ctr+1]})

	ctr += 1
	nest.Simulate(SamplInterval)

	########################################
	## Analyse and store data:
	########################################
	# Extract Synaptic weights:
	I_Synapses = nest.GetConnections(I_Input_neurons, Out_neuron)
	I_Syn_Weights = np.array(nest.GetStatus(I_Synapses, keys='weight'))
	E_Synapses = nest.GetConnections(E_Input_neurons, Out_neuron)
	E_Syn_Weights = np.array(nest.GetStatus(E_Synapses, keys='weight'))

	I_Syn_Ws.append(I_Syn_Weights)
	E_Syn_Ws.append(E_Syn_Weights)

	# Firing Rate:
	Events.append(nest.GetStatus(SpkDetector, 'n_events')[0])
	T[0, ctr] = ctr*SamplInterval/1000.

	SpkT = nest.GetStatus(SpkDetector, 'n_events')[0]
	tt = nest.GetKernelStatus()['time']*0.001
	fRate = SpkT / tt

	print "\nFiring Rate: %.2f Hz" % fRate

	Rate[0, ctr-1] = fRate
	E_rate = fRate

SSPPKK2 = nest.GetStatus(SpkDetector2, 'n_events')[0]
fRate_Final = SSPPKK2/(AnalysisT/1000.)

print "FIRING RATE (FINAL): %.2f Hz" % fRate_Final


# Extract synaptic and membrane data:
V = nest.GetStatus(Multimeter)[0]['events']['V_m']
G_ex = nest.GetStatus(Multimeter)[0]['events']['g_ex']
G_in = nest.GetStatus(Multimeter)[0]['events']['g_in']
OutSpkTimes = nest.GetStatus(SpkDetector)[0]['events']['times']

# Track E/I synaptic currents
Ex_Currents = G_ex * (V - E_AMPA)
In_Currents = G_in * (V - E_GABA)


#######################################################################
# Plot
#======================================================================
TT = np.round(Time[0:int(np.where(np.round(Time, 1) == duration-10.)[0][0])], 1) #1999990]
TimeVec = T[0][1:]
RateVec = Rate[0]
Mean_EWs = []
Mean_IWs = []
E_id = np.random.randint(0, int(eps*nE))
I_id = np.random.randint(0, int(eps*nI))
Sample_E_Syn = []
Sample_I_Syn = []
wE_der = []
wI_der = []


for i in range(len(E_Syn_Ws)):
	if 'eSTDP' in Plasticity:
		Mean_EWs.append(np.mean(E_Syn_Ws[i]*gBarEx))
		Sample_E_Syn.append(E_Syn_Ws[i][E_id]*gBarEx)
	else:
		Mean_EWs.append(np.mean(E_Syn_Ws[i]))
		Sample_E_Syn.append(E_Syn_Ws[i][E_id])
	if 'iSTDP' in Plasticity:
		Mean_IWs.append(np.mean(I_Syn_Ws[i] * gBarIn))
		Sample_I_Syn.append(I_Syn_Ws[i][I_id]*gBarIn)
	else:
		Mean_IWs.append(np.mean(I_Syn_Ws[i]))
		Sample_I_Syn.append(I_Syn_Ws[i][I_id])

	if i > 0:
		prE = E_Syn_Ws[i-1]
		di = (E_Syn_Ws[i]-prE)/SamplInterval
		wE_der.append(np.linalg.norm(di))
		#wE_der.append(np.mean(di))
		prI = I_Syn_Ws[i-1]
		di = (I_Syn_Ws[i]-prI)/SamplInterval
		wI_der.append(np.linalg.norm(di))
		#wI_der.append(np.mean(di))

# initial snapshot
idx1 = np.where(np.round(TT, 1) == 0.1)[0][0]
idx2 = np.where(np.round(TT, 1) == 500.1)[0][0]
TT1 = TT[idx1:idx2]
Vm1 = V[idx1:idx2]
E_C1 = Ex_Currents[idx1:idx2]
I_C1 = In_Currents[idx1:idx2]
if 'eSTDP' in Plasticity:
	E_w1 = E_Syn_Ws[0]*gBarEx
else:
	E_w1 = E_Syn_Ws[0]
if 'iSTDP' in Plasticity:
	I_w1 = I_Syn_Ws[0]*gBarIn
else:
	I_w1 = I_Syn_Ws[0]

# 1/4 snapshot
idx1 = np.where(np.round(TT, 1) == duration/4.)[0][0]
idx2 = np.where(np.round(TT, 1) == (duration/4.)+500.)[0][0]
TT2 = TT[idx1:idx2]
Vm2 = V[idx1:idx2]
E_C2 = Ex_Currents[idx1:idx2]
I_C2 = In_Currents[idx1:idx2]
if 'eSTDP' in Plasticity:
	E_w2 = E_Syn_Ws[int((duration/4.)/SamplInterval)]*gBarEx
else:
	E_w2 = E_Syn_Ws[int((duration/4.)/SamplInterval)]
if 'iSTDP' in Plasticity:
	I_w2 = I_Syn_Ws[int((duration/4.)/SamplInterval)]*gBarIn
else:
	I_w2 = I_Syn_Ws[int((duration/4.)/SamplInterval)]

# half-way snapshot
idx1 = np.where(np.round(TT, 1) == duration/2.)[0][0]
idx2 = np.where(np.round(TT, 1) == (duration/2.)+500.)[0][0]
TT3 = TT[idx1:idx2]
Vm3 = V[idx1:idx2]
E_C3 = Ex_Currents[idx1:idx2]
I_C3 = In_Currents[idx1:idx2]
if 'eSTDP' in Plasticity:
	E_w3 = E_Syn_Ws[int((duration/2.)/SamplInterval)]*gBarEx
else:
	E_w3 = E_Syn_Ws[int((duration/2.)/SamplInterval)]
if 'iSTDP' in Plasticity:
	I_w3 = I_Syn_Ws[int((duration/2.)/SamplInterval)]*gBarIn
else:
	I_w3 = I_Syn_Ws[int((duration/2.)/SamplInterval)]

# Final
idx1 = np.where(np.round(TT, 1) == TT[-1]-500.)[0][0]
idx2 = np.where(np.round(TT, 1) == TT[-1])[0][0]
TT4 = TT[idx1:idx2]
Vm4 = V[idx1:idx2]
E_C4 = Ex_Currents[idx1:idx2]
I_C4 = In_Currents[idx1:idx2]
if 'eSTDP' in Plasticity:
	E_w4 = E_Syn_Ws[-1]*gBarEx
else:
	E_w4 = E_Syn_Ws[-1]
if 'iSTDP' in Plasticity:
	I_w4 = I_Syn_Ws[-1]*gBarIn
else:
	I_w4 = I_Syn_Ws[-1]


execfile('plots.py')

