__author__ = 'neuro'
import pylab as pl
import numpy as np
from matplotlib import rc

# Create axes
rc('text', usetex=False)#True)
fig = pl.figure()
fig.suptitle('eSTDP + iSTDP', fontsize=14)
ax1 = pl.subplot2grid((11, 14), (0, 0), colspan=4, rowspan=1)
ax2 = pl.subplot2grid((11, 14), (1, 0), colspan=4, rowspan=1)
ax3 = pl.subplot2grid((11, 14), (0, 4), colspan=2, rowspan=2)
ax4 = pl.subplot2grid((11, 14), (0, 6), colspan=2, rowspan=2)
ax5 = pl.subplot2grid((11, 14), (3, 0), colspan=4, rowspan=1)
ax6 = pl.subplot2grid((11, 14), (4, 0), colspan=4, rowspan=1)
ax7 = pl.subplot2grid((11, 14), (3, 4), colspan=2, rowspan=2)
ax8 = pl.subplot2grid((11, 14), (3, 6), colspan=2, rowspan=2)
ax9 = pl.subplot2grid((11, 14), (6, 0), colspan=4, rowspan=1)
ax10 = pl.subplot2grid((11, 14), (7, 0), colspan=4, rowspan=1)
ax11 = pl.subplot2grid((11, 14), (6, 4), colspan=2, rowspan=2)
ax12 = pl.subplot2grid((11, 14), (6, 6), colspan=2, rowspan=2)
ax13 = pl.subplot2grid((11, 14), (9, 0), colspan=4, rowspan=1)
ax14 = pl.subplot2grid((11, 14), (10, 0), colspan=4, rowspan=1)
ax15 = pl.subplot2grid((11, 14), (9, 4), colspan=2, rowspan=2)
ax16 = pl.subplot2grid((11, 14), (9, 6), colspan=2, rowspan=2)
ax17 = pl.subplot2grid((11, 14), (0, 9), colspan=5, rowspan=5)
ax17a = ax17.twinx()
ax18 = pl.subplot2grid((11, 14), (6, 9), colspan=5, rowspan=2)
ax19 = pl.subplot2grid((11, 14), (9, 9), colspan=5, rowspan=2)


#ax1.plot(TT1, E_C1/1000, 'b', label='Exc. Currents', linewidth=1)
#ax1.plot(TT1, I_C1/1000, 'r', label='Inh. Currents', linewidth=1)
#ax1.plot(TT1, (E_C1+I_C1)/1000, 'k', label='Total', linewidth=1)
#ax1.set_xlim(TT1[0], TT1[-1])
#ax1.set_ylim((min(E_C1)/1000)-5, (max(I_C1)/1000)+5)
#ax1.set_ylabel('Synaptic Currents [nA]', fontsize=10)

ax2.plot(TT1, Vm1, 'k')
for n in OutSpkTimes:
	if TT1[0] < n < TT1[-1]: #and n < duration:
		ax2.vlines(n, -50., 0., color='k', linewidth=1.)
ax2.set_xlim(TT1[0], TT1[-1])
ax2.set_ylim(-70, 5)
ax2.set_ylabel('V_{m} [mV]', fontsize=10)
ax2.set_xlabel('Time [ms]', fontsize=10)

#...

#ax5.plot(TT2, E_C2/1000, 'b', label='Exc. Currents', linewidth=1)
#ax5.plot(TT2, I_C2/1000, 'r', label='Inh. Currents', linewidth=1)
#ax5.plot(TT2, (E_C2+I_C2)/1000, 'k', label='Total', linewidth=1)
#ax5.set_xlim(TT2[0], TT2[-1])
#ax5.set_ylim((min(E_C2)/1000)-5, (max(I_C2)/1000)+5)
#ax5.set_ylabel('Synaptic Currents [nA]', fontsize=10)

ax6.plot(TT2, Vm2, 'k')
for n in OutSpkTimes:
	if TT2[0] < n < TT2[-1]: #and n < duration:
		ax6.vlines(n, -50., 0., color='k', linewidth=1.)
ax6.set_xlim(TT2[0], TT2[-1])
ax6.set_ylim(-70, 5)
ax6.set_ylabel('V_{m} [mV]', fontsize=10)
ax6.set_xlabel('Time [ms]', fontsize=10)


#ax9.plot(TT3, E_C3/1000, 'b', label='Exc. Currents', linewidth=1)
#ax9.plot(TT3, I_C3/1000, 'r', label='Inh. Currents', linewidth=1)
#ax9.plot(TT3, (E_C3+I_C3)/1000, 'k', label='Total', linewidth=1)
#ax9.set_xlim(TT3[0], TT3[-1])
#ax9.set_ylim((min(E_C3)/1000)-5, (max(I_C3)/1000)+5)
#ax9.set_ylabel('Synaptic Currents [nA]', fontsize=10)

ax10.plot(TT3, Vm3, 'k')
for n in OutSpkTimes:
	if TT3[0] < n < TT3[-1]: #and n < duration:
		ax10.vlines(n, -50., 0., color='k', linewidth=1.)
ax10.set_xlim(TT3[0], TT3[-1])
ax10.set_ylim(-70, 5)
ax10.set_ylabel('V_{m} [mV]', fontsize=10)
ax10.set_xlabel('Time [ms]', fontsize=10)


#ax13.plot(TT4, E_C4/1000, 'b', label='Exc. Currents', linewidth=1)
#ax13.plot(TT4, I_C4/1000, 'r', label='Inh. Currents', linewidth=1)
#ax13.plot(TT4, (E_C4+I_C4)/1000, 'k', label='Total', linewidth=1)
#ax13.set_xlim(TT4[0], TT4[-1])
#ax13.set_ylim((min(E_C4)/1000)-5, (max(I_C4)/1000)+5)
#ax13.set_ylabel('Synaptic Currents [nA]', fontsize=10)

ax14.plot(TT4, Vm4, 'k')
for n in OutSpkTimes:
	if TT4[0] < n < TT4[-1]: #and n < duration:
		ax14.vlines(n, -50., 0., color='k', linewidth=1.)
ax14.set_xlim(TT4[0], TT4[-1])
ax14.set_ylim(-70, 5)
ax14.set_ylabel('V_{m} [mV]', fontsize=10)
ax14.set_xlabel('Time [ms]', fontsize=10)


ax3.hist(E_w1, facecolor='blue')
ax3.set_ylabel('Counts')
ax3.set_xlabel('Weight')
ax3.set_title('W^{E}')

ax4.hist(I_w1, facecolor='red')
ax4.set_ylabel('')
ax4.set_xlabel('')
ax4.set_title('W^{I}')


ax7.hist(E_w2, facecolor='blue')
ax7.set_ylabel('Counts')
ax7.set_xlabel('Weight')
ax7.set_title('W^{E}')

ax8.hist(I_w2, facecolor='red')
ax8.set_ylabel('')
ax8.set_xlabel('')
ax8.set_title('W^{I}')


ax11.hist(E_w3, facecolor='blue')
ax11.set_ylabel('Counts')
ax11.set_xlabel('Weight')
ax11.set_title('W^{E}')

ax12.hist(I_w3, facecolor='red')
ax12.set_ylabel('')
ax12.set_xlabel('')
ax12.set_title('W^{I}')


ax15.hist(E_w4, facecolor='blue')
ax15.set_ylabel('Counts')
ax15.set_xlabel('Weight')
ax15.set_title('W^{E}')

ax16.hist(I_w4,facecolor='red')
ax16.set_ylabel('')
ax16.set_xlabel('')
ax16.set_title('W^{I}')


ax17.plot(TimeVec, Mean_EWs, color='b', label=r'Exc')
ax17.plot(TimeVec, Mean_IWs, color='r', label=r'Inh')
ax17.set_ylabel('Mean Synaptic Strength [nS]', color='b')
ax17.set_xlabel('Time [s]')
ax17.set_xlim([0., 300.])
ax17.legend(fontsize = 9)
for tl in ax17.get_yticklabels():
	tl.set_color('b')

ax17a.plot(TimeVec, RateVec, color='k')
ax17a.set_ylabel('Mean Output Rate [Hz]', color='g')
ax17a.set_title('Mean Equilibrium Rate = {0} Hz'.format(str(fRate_Final)))
ax17a.set_ylim(min(RateVec)-1, max(RateVec)+1)
ax17a.set_xlim([0., 300.])
for tl in ax17a.get_yticklabels():
	tl.set_color('g')


ax18.plot(TimeVec, Sample_E_Syn, color='b', label=r'Example E')
ax18.plot(TimeVec, Sample_I_Syn, color='r', label=r'Example I')
#ax18.legend(fontsize=8)
ax18.set_xlim([0., 300.])
ax18.set_ylabel('Syn. Strength [nS]')

ax19.plot(TimeVec[1:], wE_der, color='b', label=r'E')
ax19.plot(TimeVec[1:], wI_der, color='r', label=r'I')
#ax19.legend()
ax19.set_xlim([0., 300.])
ax19.set_ylabel('$|\dot{W}|$')

pl.show()












