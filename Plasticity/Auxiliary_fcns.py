__author__ = 'neuro'
import numpy as np

def correlogram(T1, T2, width=20., bin=0.1):
    """
    Returns a cross-correlogram with lag in [-width,width] and bin size bin
        - T1 / T2 are lists of spike times
        - width and bin should be specified in ms
    The result is in Hz (rate of coincidences in each bin).
    """

    if (T1 == []) or (T2 == []):  # empty spike train
        return np.NaN
    # Remove units
    width = float(width)
    T1 = np.array(T1)
    T2 = np.array(T2)

    i = 0
    j = 0
    n = int(np.ceil(width/bin))  # Histogram length

    l = []
    for t in T1:
        while i < len(T2) and T2[i] < t - width:
            i += 1
        while j < len(T2) and T2[j] < t + width:
            j += 1
        l.extend(T2[i:j] - t)
    H, _ = np.histogram(l, bins=np.arange(2*n + 1)*bin - n*bin)

    # Divide by time to get rate
    #if T is None:
    T = max(T1[-1], T2[-1]) - min(T1[0], T2[0])
    # Windowing function (triangle)
    W = np.zeros(2 * n)
    W[:n] = T - bin * np.arange(n - 1, -1, -1)
    W[n:] = T - bin * np.arange(n)

    return H / W
