import os
import sys
sys.path.append('../')
from CommonModules.Analysis_fcns import *
from CommonModules.Auxiliary_fcns import *
import nest
import numpy as np
import pickle
import itertools
import NeuroTools.signals_mine as signals


def kernel_quality(pars_dict, spike_templates, i):

	########################################################################
	# Extract Parameters
	########################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1

	RANKS = []
	########################################################################################
	nest.ResetKernel()
	nest.SetKernelStatus({'resolution': dt,
	                      'print_time': False,
	                      'data_path': data_path,
	                      'data_prefix': data_label + str(i),
	                      'overwrite_files': True,
	                      'local_num_threads': proc_per_node})
	########################################################################
	# 					CREATE NEURONS AND DEVICES
	########################################################################
	weights = set_synapses(pars_dict)

	E_neurons, I_neurons = set_network(pars_dict)

	#-----------------------------------------------------------------------
	# Randomize initial Vm
	#-----------------------------------------------------------------------
	nest.SetStatus(E_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nE))))
	nest.SetStatus(I_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nI))))

	#-----------------------------------------------------------------------
	# Create Devices
	#-----------------------------------------------------------------------
	SpkDetector, population_multimeter, PG = set_standard_devices(pars_dict)

	Inputs = nest.Create('spike_generator', 4)
	Input_Neurons = nest.Create('parrot_neuron', 4)

	########################################################################
	# 							CONNECT
	########################################################################
	connect_network(weights, pars_dict, E_neurons, I_neurons)
	connect_standard_devices(E_neurons, I_neurons, g_bar_E, delays, PG, SpkDetector, population_multimeter)

	for n in range(4):
		nest.Connect([Inputs[n]], [Input_Neurons[n]])
		nest.DivergentConnect([Input_Neurons[n]], list(itertools.chain(*[E_neurons, I_neurons])), weight=g_bar_E,
		                      delay=delays, model='Ext_Synapse')

	########################################################################
	########################################################################
	STATE_MATRIX = np.zeros((int(N), len(spike_templates)))
	Starts = np.round(np.arange(TransientT, TransientT+((dur+IStimI)*500.), (dur+IStimI)))

	# Generate spike times based on templates...
	ctr = 0
	Spkslist = [[], [], [], []]
	for nTmp in spike_templates:
		for ii in range(4):
			SpkTim = (Starts[ctr]+nTmp[ii])
			Spkslist[ii].extend(SpkTim.tolist())
		ctr += 1

	# ... and set the spike times in the generators
	for ii in range(4):
		nest.SetStatus([Inputs[ii]], {'spike_times': np.round(Spkslist[ii], 1)})

	#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	nest.Simulate(TransientT)

	#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	ctr = 0
	for nTmp in range(len(spike_templates)):

		nest.Simulate(dur)

		if nest.GetStatus(SpkDetector, 'n_events')[0]:
			SpkData = extract_data_fromfile(nest.GetStatus(SpkDetector)[0]['filenames'])
			Times = SpkData[:, 1]
			Ids = SpkData[:, 0]
			SpkIds = np.where(Times > Starts[nTmp])
			SpkTimes = Times[SpkIds]
			NeuronId = Ids[SpkIds]
			tmp = [(NeuronId[n], SpkTimes[n]) for n in range(len(SpkTimes))]
			Spk_list = signals.SpikeList(tmp, np.unique(NeuronId).tolist())

			if len(SpkTimes) > 0:
				State = Spk_list.spikes_to_states_exp(N, dt, 30.)
			else:
				State = np.zeros((int(N), 2))
		else:
			State = np.zeros((int(N), 2))
		###

		STATE_MATRIX[:, nTmp] = State[:, -1]

		nest.Simulate(IStimI)
		ctr += 1

		nest.SetStatus(SpkDetector, {'n_events': 0.})
		
		print 'Template {0}/{1}'.format(str(ctr), str(500))

		a = nest.GetKernelStatus()['time']

		print 'Elapsed Simulation Time: {0}'.format(str(a))

	RANKS.append(np.linalg.matrix_rank(STATE_MATRIX))

	print RANKS

	Results = dict()
	Results['Ranks'] = RANKS
	Results['Mean_KernelQuality'] = np.mean(RANKS)
	Results['Std_KernelQuality'] = np.std(RANKS)
	###############################################################################################
	# Store Result in Parameters dictionary
	pars_dict['Results'] = Results

	for f in nest.GetStatus(SpkDetector)[0]['filenames']:
		os.remove(f)

	return pars_dict
