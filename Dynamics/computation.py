import os
import settings
import pickle
import sys
sys.path.append('../')
from CommonModules.Parameters import *
from Dynamics_Analysis import *

PD = set_params()

array_id = os.getenv("PBS_ARRAYID")


def computation(x, x_var, y, y_var, PD, i):
	"""
	Main function: Gets the parameters to scan and their values as arguments and
	runs the main simulations
	Starts by setting the correct parameters in the dictionary prior to calling the main simulation
	functions
	"""

	print("=======================================================================")
	print("                      {0} Task".format(PD['Options']['Task']))
	print("=======================================================================")
	print(" Computing for {0} = {1}, {2} = {3}".format(x_var, x, y_var, y))

	specific_sim_name = "Simulating {0} = {1}_{2} = {3}".format(x_var, x, y_var, y)

	# Set variables in dictionary
	for k, v in PD.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			if k1 == x_var:
				PD[k][k1] = x
			if k1 == y_var:
				PD[k][k1] = y

	PD = set_params_derived(PD)

	#### Run Main Simulation ###
	print specific_sim_name
	#PD_Result = dynamics_analysis(PD, i)
	PD_Result = dynamics_analysis_activePlasticity(PD, i)
	############################

	### Save Data #####
	label = PD['Data_Pars']['data_label']
	fil = open("%s" % label + "_{0}={1}_{2}={3}".format(x_var, x, y_var, y), "w")
	pickle.dump(PD_Result, fil)
	fil.close()


def run_single(i):
	print("Starting job for index: {0}".format(i))

	ix = i % len(settings.dom_x)
	iy = (i // len(settings.dom_x)) % len(settings.dom_y)

	x = settings.dom_x[ix]
	x_var = settings.x_var
	y = settings.dom_y[iy]
	y_var = settings.y_var

	computation(x, x_var, y, y_var, PD, i)


def run_multiple():
	print("Starting job for all parameter values")
	ctr = 0
	for x in settings.dom_x:
		for y in settings.dom_y:
			ctr =+ 1
			computation(x, settings.x_var, y, settings.y_var, PD, ctr)


if array_id is not None:
	run_single(int(array_id))
else:
	run_multiple()