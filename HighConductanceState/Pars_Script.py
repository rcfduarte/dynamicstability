#***********************************************************************
#					    	PARAMETERS                                 *
#***********************************************************************
########################################################################
# Simulation Parameters
#=======================================================================
dt = 0.1 		            	  # [ms] - Simulation Time step
SimT = 10000. 			          # [ms] - Total Simulation Time
TransientT = 300.				  # [ms] - time for initial transient (if applicable)
PlasticityT = 0.                  # [ms] - duration of initial plasticity phase
									# (Set to 0. if not applicable)
########################################################################
# DATA Parameters
#=======================================================================
data_label = 'HighCondState'
data_path = '/scratch/duarte'
########################################################################
# SYSTEM Parameters
#=======================================================================
nodes = 1                        # how many compute nodes to use
proc_per_node = 20               # how many processors to use per compute node
mem = 40                         # how much total memory requited (in GB)
walltime = '12:00:00'
queue = 'medium'
########################################################################
# Network Parameters
#=======================================================================
### Population Size
N = 50000.                        # Total Number of neurons
nE = 0.8*N				          # E neurons
nI = 0.2*N				          # I neurons

### Neuron
model = 'iaf_cond_alpha'		      # neuron model
g_leak = 16.7				      # [nS] leak conductance (other value: 10.nS)
E_L = -70.      			      # [mV] RMP (-60.)
E_GABA = -80.					  # [mV] inhibitory reversal potential
E_AMPA = 0.						  # [mV] excitatory reversal potential
V_th = -50. 					  # [mV] spiking threshold
C_m = 250.      				  # [pF] membrane capacitance (200.)
I_b = 0.					      # [pA] external current (200.)
tau_ref = 2.					  # [ms] refractory time
V_reset = -70.					  # [mV] reset potential
tau_minus = 20.                   # [mS] length of time window (STDP)


### Connectivity
epsilon = 0.1  	                  # for similar connection probabilities (0.02)
pEE	= epsilon			          # Probability of connection (E->E)
pEI	= epsilon			          # Probability of connection (I->E)
pIE = epsilon			          # Probability of connection (E->I)
pII	= epsilon			          # Probability of connection (I->I)
mu_E = 1.				          # mean of Gaussian distribution from which the w are drawn...
sig_E = 0.                       # (and std.)
mu_I = 1.
sig_I = 0.

### Synapse
tau_E = 0.326						  # [ms] AMPA-synapse time constant (5.0)
tau_I = 0.326			    		  # [ms] GABA-synapse time constant (10.0)
inh_scale = -10.                  # scaling factor for inhibitory conductances
g_bar_E = 0.68                      # average excitatory conductance
g_bar_I = inh_scale*g_bar_E     # average inhibitory conductance
delays = 1.5                      # conduction delays (constant)

### Plasticity - inhibitory STDP
eta_iSTDP = 1e-2				  # learning rate
tau_iSTDP = 20.					  # [ms] STDP time constant
ro = 3.							  # [Hz] target firing rate
alpha = 2.*ro*(tau_iSTDP*0.001)   #
w_min = 0.                        # min weight values
w_max = 10. * (-inh_scale)        # max weight values

### Plasticity - excitatory STDP
tau_eSTDP = 20.					  # [ms] - time window...
lamb = 0.1					      # [pS] - average amount of potentiation after one pairing (from experimental data)
alpha_E = 0.003/lamb    		  # [pS] - average amount of depression after one pairing (tune)
W_MAX = 100.                      # max weight value

########################################################################
# Input Parameteres
#=======================================================================
## Background Input
Ext_rate = 20.					  # [Hz] Rate of external source of background input (Poisson)

########################################################################
# Options
#=======================================================================
Topology = 'None'          # Topological constraints:
									# - 'none' -> no specification
									# - '2Dlattice' -> neurons positioned
									# in the vertices of a 2D grid
									# - '2Drandom' -> neurons positioned
									# randomly in 2D space
									# - '3Dlattice' -> neurons positioned
									# in the vertices of a 3D grid (*)
									# - '3Drandom' -> neurons positioned
									# randomly in 3D space
									# - 'columnar' -> neurons arranged
									# in a 3D columnar architecture (**)
Input_population = 'EI'   # Nature of input neurons (several properties may be selected):
									# - 'random' -> symbol-specific neurons
									# picked randomly
									# - 'strong' -> connections within each
									# assembly are strengthened (**)
									# - 'positional' -> symbol-specific
									# neuronal populations chosen to be
									# spatially clustered
									# - 'max_dist' -> maximize the distance
									# between input populations (time-consuming...)
									# - 'E/I/EI' -> nature of the input neurons
									# -'weighted' - W_in to connect input to all neurons in
									# neuronal population
Input_signal = 'Frozen'     # Type of signal encoding:
									# - 'Frozen' - signal encoded as frozen Poisson template
									# (symbol-specific), embedded in noisy background
Plasticity = 'None'	       # Control plasticity rules:
									# - 'None' -> all synapses are static
									# - 'iSTDP'
									# - 'eSTDP' (...)
Task = 'HCS'          # Nature of the current task (especially for parameter searches):
									# - 'None'
									# - 'Dynamics'
									# - 'InputEncoding'
									# - 'KernelQuality'
									# - 'Generalization'
									# - ('FSG', ...)

Connectivity = 'random'    # Connectivity characteristics (several properties may be selected):
									# - 'random' -> each neuron chooses its
									# targets randomly
									# - 'distance_dependent'-> each neuron
									# connects preferentially with local
									# neighbors (with Gaussian profile, sigC) (**)
									# - 'local_inhibition' -> inhibitory
									# neurons are subdivided into locally
									# and globally inhibiting populations
									# (ratio) (**)
									# - 'explicit_paths' -> connections
									# between symbol-specific subpopulations
									# subjected to transitional constraints (**)
Extra_Pars = {'profile': 'Gaussian', 'sigC': 5., 'ratio': .35}



