import nest
import numpy as np
import itertools
import sys
sys.path.append('../')
from CommonModules.Analysis_fcns import *
from CommonModules.Auxiliary_fcns import *
import os


def high_cond_state(pars_dict, i):

	########################################################################
	# Extract Parameters
	########################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1

	########################################################################
	# Reset NEST kernel
	########################################################################
	nest.ResetKernel()
	nest.SetKernelStatus({'resolution': dt,
	                      'print_time': False,
	                      'data_path': data_path,
	                      'data_prefix': data_label + str(i),
	                      'overwrite_files': True,
	                      'local_num_threads': proc_per_node})
	########################################################################
	# 					CREATE NEURONS AND DEVICES
	########################################################################
	E_neurons, I_neurons = set_network(pars_dict)
	E_neurons_copy = nest.Create(Neuron_model, 100)
	original_neuronIdx = np.random.randint(min(E_neurons), max(E_neurons), 100)
	weights = set_synapses(pars_dict)
	weights['wEE'] = np.ones((int(nE), int(pEE * nE)))
	weights['wEI'] = np.ones((int(nE), int(pEI * nI)))
	weights['wIE'] = np.ones((int(nI), int(pIE * nE)))
	weights['wII'] = np.ones((int(nI), int(pII * nI)))
	#-----------------------------------------------------------------------
	# Randomize initial Vm
	#-----------------------------------------------------------------------
	nest.SetStatus(E_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nE))))
	nest.SetStatus(I_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nI))))
	for idx, n in enumerate(original_neuronIdx):
		nest.SetStatus([E_neurons_copy[idx]], {'V_m': nest.GetStatus([n])[0]['V_m'], 'V_th': 10000000.})

	#-----------------------------------------------------------------------
	# Create and Set Devices
	#-----------------------------------------------------------------------
	SpkDetector, population_multimeter, PG = set_standard_devices(pars_dict)
	nest.CopyModel('multimeter', 'copy_multimeter')
	nest.SetDefaults('copy_multimeter', {'record_from': ['V_m'],
	                                     'record_to': ['accumulator'],
	                                     'label': 'Copy_multimeter',
	                                     'start': PlasticityT + TransientT})
	copy_multimeter = nest.Create('copy_multimeter')
	########################################################################
	# 							CONNECT
	########################################################################
	connect_network(weights, pars_dict, E_neurons, I_neurons)
	connect_standard_devices(E_neurons, I_neurons, g_bar_E, delays, PG, SpkDetector, population_multimeter)
	nest.DivergentConnect(copy_multimeter, E_neurons_copy)

	aEI = nest.GetStatus(nest.GetConnections(I_neurons, original_neuronIdx.tolist()))

	for nn in aEI:
		nn['target'] = E_neurons_copy[np.where(original_neuronIdx == nn['target'])[0][0]]
	nest.DataConnect(aEI)

	aEE = nest.GetStatus(nest.GetConnections(E_neurons, original_neuronIdx.tolist()))
	for nn in aEE:
		nn['target'] = E_neurons_copy[np.where(original_neuronIdx == nn['target'])[0][0]]
	nest.DataConnect(aEE)

	########################################################################
	#						      MAIN
	########################################################################
	nest.Simulate(PlasticityT + TransientT + SimT)

	########################################################################
	#							ANALYZE
	########################################################################
	Results = dict()
	time_interval = [PlasticityT + TransientT, PlasticityT + TransientT + SimT]

	if Plasticity == 'None':
		if nest.GetStatus(SpkDetector)[0]['n_events']:
			Mean_fRate, Std_fRate, CV_ISI, CC = compute_spiking_stats(SpkDetector, E_neurons, I_neurons, 100,
			                                                          time_interval, display=True, splitEI=False)
			AIness, AI_Data = compute_AIness(Mean_fRate, CV_ISI, CC, N)
		else:
			Mean_fRate = 0.
			Std_fRate = 0.
			CV_ISI = np.nan
			CC = np.nan
			AIness = 0.
			AI_Data = []

		Results['Mean_fRate'] = np.mean(Mean_fRate)
		Results['Std_fRate'] = Std_fRate
		Results['CV_ISI'] = np.mean(CV_ISI)
		Results['CC'] = np.mean(CC)
		Results['AIness'] = AIness
		Results['AIness_Data'] = AI_Data

	else:
		if nest.GetStatus(SpkDetector)[0]['n_events']:
			Mean_fRate, Mean_fRate_E, Mean_fRate_I, Std_fRate, Std_fRate_E, Std_fRate_I, CV_ISI, CV_ISI_E, \
			CV_ISI_I, CC, CC_E, CC_I = compute_spiking_stats(SpkDetector, E_neurons, I_neurons, 200,
			                                                 time_interval, display=True, splitEI=True)
			AIness, AI_Data = compute_AIness(Mean_fRate, CV_ISI, CC, N)
			AIness_E, AI_Data_E = compute_AIness(Mean_fRate_E, CV_ISI_E, CC_E, nE)
			AIness_I, AI_Data_I = compute_AIness(Mean_fRate_I, CV_ISI_I, CC_I, nI)

			Total_Inh = list(itertools.chain(*[np.abs(c_EI), np.abs(c_II)]))
			Total_Exc = list(itertools.chain(*[c_EE, c_IE]))
			Real_g = (np.mean(Total_Inh) * tau_I * np.abs(V_Rest - V_I)) / (
			np.mean(Total_Exc) * tau_E * np.abs(V_Rest - V_E))

			Results['Mean_fRate'] = np.mean(Mean_fRate)
			Results['Mean_fRate_E'] = np.mean(Mean_fRate_E)
			Results['Mean_fRate_I'] = np.mean(Mean_fRate_I)
			Results['Std_fRate'] = Std_fRate
			Results['Std_fRate_E'] = Std_fRate_E
			Results['Std_fRate_I'] = Std_fRate_I
			Results['CV_ISI'] = np.mean(CV_ISI)
			Results['CV_ISI_E'] = np.mean(CV_ISI_E)
			Results['CV_ISI_I'] = np.mean(CV_ISI_I)
			Results['CC'] = np.mean(CC)
			Results['CC_E'] = np.mean(CC_E)
			Results['CC_I'] = np.mean(CC_I)
			Results['AIness'] = AIness
			Results['AIness_E'] = AIness_E
			Results['AIness_I'] = AIness_I
			Results['AIness_Data'] = AI_Data
			Results['AI_Data_E'] = AI_Data_E
			Results['AI_Data_I'] = AI_Data_I
			Results['Real_g'] = Real_g

	Mean_GE, Mean_GI, Mean_VM = compute_analogue_stats(population_multimeter, N)
	Cond_Ratio = (Mean_GE + Mean_GI) / g_leak
	Mean_Free_Vm = np.mean(nest.GetStatus(copy_multimeter)[0]['events']['V_m'] / nE)
	Std_Free_Vm = np.std(nest.GetStatus(copy_multimeter)[0]['events']['V_m'] / nE)

	Results['Cond_Ratio'] = Cond_Ratio
	Results['Mean_Free_Vm'] = Mean_Free_Vm
	Results['Std_Free_Vm'] = Std_Free_Vm
	Results['Mean_Vm'] = Mean_VM
	Results['Mean_GE'] = Mean_GE
	Results['Mean_GI'] = Mean_GI

	###############################################################################################
	# Store Result in Parameters dictionary
	pars_dict['Results'] = Results

	for f in nest.GetStatus(SpkDetector)[0]['filenames']:
		os.remove(f)

	return pars_dict
