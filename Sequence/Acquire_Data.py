__author__ = 'duarte'

import os
import sys
sys.path.append('../')
from CommonModules.Analysis_fcns import *
from CommonModules.Auxiliary_fcns import *
from CommonModules.Input_fcns import *
from CommonModules.SymbolSeqTools import *
import numpy as np
import nest


def simulate(pars_dict, i):
	##############################################################
	# Set input variables and options
	##############################################################
	Input_Pars_Dict = pars_dict['Input_Pars']

	for k, v in Input_Pars_Dict.items():
		globals()[k] = v
	options = 'None'
	Grammar = FSG(states, alphabet, transition_pr, start_states, end_states, options)

	Seq = correct_symbolseq(concatenate(generate(Grammar, nStrings), ''), Grammar)

	###############################################################
	# Generate input signals
	###############################################################
	step_signal, rate_signal, time_data = encode_input(Grammar, Seq, Input_Pars_Dict, noise=True)

	input_SpkTrains = generate_burst_spktrains(Grammar, Input_Pars_Dict, rate_signal, step_signal, time_data,
	                                           display=False)

	########################################################################
	# Extract all Parameters
	########################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1

	########################################################################
	# Reset NEST kernel
	########################################################################
	nest.ResetKernel()
	nest.SetKernelStatus({'resolution': dt,
	                     'print_time': False,
	                      'data_path': data_path,
                         'data_prefix': data_label+str(i),
	                      'overwrite_files': True,
	                      'local_num_threads': proc_per_node})
	SimT = (stim_dur+I_stim_I)*len(Seq)

	########################################################################
	# 					CREATE NEURONS AND DEVICES
	########################################################################
	tp_dict = set_topology(pars_dict)
	weights = set_synapses(pars_dict)
	tp_dict = connect_network(weights, pars_dict, tp_dict)

	E_neurons = tp_dict['E_neurons']
	I_neurons = tp_dict['I_neurons']

	create_inputs(pars_dict, tp_dict, input_SpkTrains)

	#-----------------------------------------------------------------------
	# Randomize initial Vm
	#-----------------------------------------------------------------------
	nest.SetStatus(E_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nE))))
	nest.SetStatus(I_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nI))))

	#-----------------------------------------------------------------------
	# Create and Set Devices
	#-----------------------------------------------------------------------
	SpkDetector = nest.Create('spike_detector')
	nest.SetStatus(SpkDetector, {'record_to': ['memory'], 'start': PlasticityT + TransientT,
	                                'to_file': False, 'to_memory': True,
		                             'label': 'SpkDetector'})

	background_PG = nest.Create('poisson_generator')
	nest.SetStatus(background_PG, {'rate': Ext_rate * (pEE * nE)})

	########################################################################
	# 							CONNECT
	########################################################################
	nest.DivergentConnect(background_PG, list(itertools.chain(*[E_neurons, I_neurons])), weight=g_bar_E, delay=delays,
	                      model='Ext_Synapse')

	nest.ConvergentConnect(list(itertools.chain(*[E_neurons, I_neurons])), SpkDetector)

	########################################################################
	# Simulate
	########################################################################

	nest.Simulate(TransientT + SimT)

	#########################################################################
	# Analysis
	#########################################################################
	Results = dict()

	Results['Full_Seq'] = Seq
	Results['Step_signal'] = step_signal
	Results['time_data'] = time_data
	Results['Topology'] = tp_dict
	Results['input_SpkTrains'] = input_SpkTrains

	pars_dict['Results'] = Results

	for f in nest.GetStatus(SpkDetector)[0]['filenames']:
		os.remove(f)

	return pars_dict


def simulate_online_store(pars_dict, i):
	# #############################################################
	# Set input variables and options
	##############################################################
	Input_Pars_Dict = pars_dict['Input_Pars']

	for k, v in Input_Pars_Dict.items():
		globals()[k] = v
	options = 'None'
	Grammar = FSG(states, alphabet, transition_pr, start_states, end_states, options)

	Seq = correct_symbolseq(concatenate(generate(Grammar, nStrings), ''), Grammar)

	###############################################################
	# Generate input signals
	###############################################################
	step_signal, rate_signal, time_data = encode_input(Grammar, Seq, Input_Pars_Dict, noise=True)

	input_SpkTrains = generate_burst_spktrains(Grammar, Input_Pars_Dict, rate_signal, step_signal, time_data,
	                                           display=False)

	########################################################################
	# Extract all Parameters
	########################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1

	########################################################################
	# Reset NEST kernel
	########################################################################
	nest.ResetKernel()
	nest.SetKernelStatus({'resolution': dt,
	                      'print_time': False,
	                      'data_path': data_path,
	                      'data_prefix': data_label + str(i),
	                      'overwrite_files': True,
	                      'local_num_threads': proc_per_node})

	SimT = ((stim_dur + I_stim_I) * len(Seq) - TransientT)

	########################################################################
	# 					CREATE NEURONS AND DEVICES
	########################################################################
	tp_dict = set_topology(pars_dict)
	weights = set_synapses(pars_dict)
	tp_dict = connect_network(weights, pars_dict, tp_dict)

	E_neurons = tp_dict['E_neurons']
	I_neurons = tp_dict['I_neurons']

	create_inputs(pars_dict, tp_dict, input_SpkTrains)

	#-----------------------------------------------------------------------
	# Randomize initial Vm
	#-----------------------------------------------------------------------
	nest.SetStatus(E_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nE))))
	nest.SetStatus(I_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nI))))

	#-----------------------------------------------------------------------
	# Create and Set Devices
	#-----------------------------------------------------------------------
	SpkDetector = nest.Create('spike_detector')
	nest.SetStatus(SpkDetector, {'record_to': ['memory'], 'start': PlasticityT + TransientT,
	                             'to_file': False, 'to_memory': True,
	                             'label': 'SpkDetector'})

	background_PG = nest.Create('poisson_generator')
	nest.SetStatus(background_PG, {'rate': Ext_rate * (pEE * nE)})

	########################################################################
	# 							CONNECT
	########################################################################
	nest.DivergentConnect(background_PG, list(itertools.chain(*[E_neurons, I_neurons])), weight=g_bar_E, delay=delays,
	                      model='Ext_Synapse')

	nest.ConvergentConnect(list(itertools.chain(*[E_neurons, I_neurons])), SpkDetector)

	########################################################################
	# Simulate
	########################################################################
	Sq = Seq[nDiscardSeq:]

	nest.Simulate(PlasticityT)
	nest.Simulate(TransientT)

	for iddd, t in enumerate(Sq):

		nest.Simulate(stim_dur+I_stim_I)

		SpkTimes = nest.GetStatus(SpkDetector)[0]['events']['times']
		NeuronIds = nest.GetStatus(SpkDetector)[0]['events']['senders']

		with open(data_path + data_label + '_nIU={0}_nInputTrains={1}'.format(str(nIU),
		        str(nInputTrains)) + '_Response_{0}_to_{1}'.format(str(iddd), str(t)), 'w') as fp:
			for n in range(len(SpkTimes)):
				fp.write(str(NeuronIds[n])+' '+str(SpkTimes[n])+'\n')
			fp.write('\n\n')

		nest.SetStatus(SpkDetector, {'n_events': 0})
	#########################################################################
	# Analysis
	#########################################################################
	Results = dict()

	Results['Full_Seq'] = Seq
	Results['Step_signal'] = step_signal
	Results['time_data'] = time_data
	Results['Topology'] = tp_dict
	Results['input_SpkTrains'] = input_SpkTrains

	pars_dict['Results'] = Results

	return pars_dict