__author__ = 'neuro'
########################################################################
# Python library to generate structured symbolic sequences using a
# set of rules specified by a formal grammar
########################################################################
import numpy as np
import bisect


class FSG:
	"""
	FINITE STATE GRAMMAR:
	Class representing the set of grammatical rules underlying the
	temporal sequences (strings) to be produced.
	The rules are specified by the allowed transitions of a directed
	graph, along with corresponding transition probabilities. The
	sequences are generated by traversing the graph.
	"""

	def __init__(self, states, alphabet, transition_pr, start_symbols, end_symbols, options=None):
		"""
		The inputs to the class are the following:
		- states: a list containing the states of grammar, i.e., the
		nodes of the directed graph. For simplicity and robustness of
		implementation, the nodes should correspond to the individual
		(unique) symbols that constitute the language
		- alphabet: a list containing the unique symbols that ought to
		be represented. In many cases, these symbols are different from
		the states, given that the same symbol may correspond to
		several different nodes, in which case, the different states
		for the same symbol are numbered (see examples)
		- transition_pr: list of tuples with the structure (source_state,
		target_state, transition probability). e.g. [('a','b',0.1),
		('a','c',0.3)]
		- start_symbols: a list containing the possible start symbols
		- end_symbols: a list containing the terminal symbols
		- options: dictionary containing relevant options. The entries
		of this dictionary are:
				o 'task' (str) -> name given to the task
				o 'match_stats' (bool) -> choose generated strings to be
				matched for their surface features
				o (...)
		"""
		self.states = list(np.sort(states))
		self.alphabet = list(np.sort(alphabet))
		self.transition_pr = transition_pr
		self.start_symbols = start_symbols
		self.end_symbols = end_symbols
		self.options = options

	def print_rules(self):
		"""
		Displays all the relevant information.
		"""
		print ''
		if self.options != 'None':
			print 'Generative mechanism abides to %s rules' % self.options['task']
		print 'Unique states:', self.states
		print 'Alphabet:', self.alphabet
		print 'Transition table: \n{0}'.format(self.generate_transitiontable())
		#print 'Grammar complexity (topological entropy): '

	def generate_transitiontable(self):
		"""
		Creates a look-up table with all allowed transitions and their probabilities
		"""
		t = self.transition_pr

		table = np.zeros((len(self.states), len(self.states)))

		for i, ii in enumerate(self.states):
			for j, jj in enumerate(self.states):
				tmp = [v[2] for v in t if (v[0] == jj and v[1] == ii)]
				if tmp:
					table[i, j] = tmp[0]

		self.transitionTable = table
		return table

	def validate(self, debug=False):
		"""
		Verify that all the start and end states are members of the
		state set and if the alphabet is different from the states
		"""
		assert set(self.start_symbols).issubset(set(self.states)), 'start_symbols not in states'
		assert set(self.end_symbols).issubset(set(self.states)), 'end_symbols not in states'

		if not set(self.alphabet).issubset(set(self.states)):
			TestVar = set(self.alphabet).difference(set(self.states))
			if debug:
				print TestVar
			return TestVar
		else:
			return True


def generate(Grammar, nStrings, debug=False):
	"""
	Generates nStrings symbol sequences abiding to the rules
	of the grammar (string sets).
	"""
	trP = Grammar.transition_pr
	starts = Grammar.start_symbols
	ends = Grammar.end_symbols
	if Grammar.options != 'None':
		opt = Grammar.options
		print 'Generating {0} strings, according to {1} rules...'.format(nStrings, opt['task'])
	else:
		opt = dict()
		opt['task'] = 'None'

	String = []
	GeneratedStrings = []
	String.append(starts[np.random.random_integers(0, len(starts)-1)])

	while len(GeneratedStrings) < nStrings:

		current_state = String[-1]
		allowed_transitions = [x for i, x in enumerate(trP) if x[0] == current_state]

		if current_state in ends:
			if opt['task'] == 'RG1':
				String.remove('#')
			GeneratedStrings.append(String)
			String = []
			String.append(starts[np.random.random_integers(0, len(starts)-1)])
		else:
			assert (allowed_transitions >= 0), 'No allowed transitions from node {0}'.format(current_state)
			if len(allowed_transitions) == 1:
				String.append(allowed_transitions[0][1])
			else:
				Pr = [n[2] for i, n in enumerate(allowed_transitions)]
				cumPr = np.cumsum(Pr)
				idx = bisect.bisect_right(cumPr, np.random.random())
				String.append(allowed_transitions[idx][1])
	if debug:
		print '       Example String: {0}'.format(''.join(GeneratedStrings[np.random.random_integers(0,len(starts)-1)]))
	return GeneratedStrings


def concatenate(Strings, int_sym):
	"""
	Concatenates the generated strings, placing the symbol
	'int_sym' between them (if no symbol is to be added, enter an
	empty string '')
	"""
	[n.insert(0, int_sym) for n in Strings]
	SymbolSeq = np.concatenate(Strings).tolist()
	return SymbolSeq


def correct_symbolseq(SymbolSeq, Grammar):
	"""
	Corrects the symbol sequence. In sequences where the same symbol
	corresponds to different states (i.e. alphabet != states), the
	various states for the same symbol have a numeric index. This
	function removes the index, maintaining only the proper symbol.
	Note: input to this function is the concatenated sequence...
	Returns a Sequence of symbols as a unique string, can be easily
	converted to a list if necessary (list())
	"""
	assert len(SymbolSeq[0]) <= 1, 'Sequence argument should be concatenated prior to calling this function...'

	if Grammar.validate():
		bList = map(lambda x: x, set(Grammar.states).difference(set(Grammar.alphabet)))
		symbol = [x[0] for x in bList]

		assert not(set(symbol).difference(set(Grammar.alphabet))), 'Symbols in states do not match symbols in alphabet'

		# Replace sequence symbols in bList with the corresponding symbols in symbol list
		for i, n in enumerate(bList):
			idx = np.where(np.array(SymbolSeq) == n)
			idx = idx[0].tolist()
			if len(idx) > 1:
				for a in idx:
					SymbolSeq[a] = symbol[i]
			else:
				SymbolSeq[idx] = symbol[i]

		# each entry in the sequence should be one symbol
		tmp = [len(SymbolSeq[n]) <= 1 for n in range(len(SymbolSeq))]
		assert np.mean(tmp) == 1., 'Sequence entries not uniquely defined'

		# Remove possible trailing spaces
		Sequence = ''.join(SymbolSeq)
	return Sequence

#def compute_grammar_from_text()

