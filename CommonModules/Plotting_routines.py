__author__ = 'neuro'
import pylab as pl
import nest
import nest.raster_plot
import numpy as np
from NeuroTools import signals_mine as signals
import NeuroTools.stgen_mine as gen
import itertools
from mpl_toolkits.axes_grid1 import make_axes_locatable
from Analysis_fcns import *


########################################################################################################################
def plot_singlenet_activity(spk_data, analogue_vars, neuron_ids, time_interval, simt, neuronIdx):
	"""
	Plot a sample of network activity:
	- spk_data is a data file containing spk_times and spk_ids
	- analogue_vars is a dictionary containing V, gE, gI for one sample neuron
	plus Vm data for all recorded neurons (Vms)
	- neuron_ids is a list of neuron ids to display in the raster
	- time_interval is the cutoff interval
	"""

	total_time_vector = np.arange(0., simt, 0.1)
	spk_times = spk_data[:, 1]
	spk_ids = spk_data[:, 0]

	spk_times_plot = [spk_times[n] for n in range(len(spk_times)) if (spk_ids[n] in neuron_ids) and (time_interval[1]>
		spk_times[n]>time_interval[0])]
	spk_ids_plot = [spk_ids[n] for n in range(len(spk_times)) if (spk_ids[n] in neuron_ids) and (time_interval[1]>
		spk_times[n]>time_interval[0])]
	spk_times_sing = [spk_times[n] for n in range(len(spk_times)) if (spk_ids[n] == neuronIdx) and (time_interval[1] >
	                                                                                                 spk_times[n] >
	                                                                                                 time_interval[0])]

	tmp = [(spk_ids[n], spk_times[n]) for n in range(len(spk_times))]
	spk_list = signals.SpikeList(tmp, np.unique(spk_ids).tolist())

	#============================ FIG 1 =====================================
	fig1 = pl.figure()
	fig1.suptitle('Example of Background network activity', fontsize=16)
	ax1 = pl.subplot2grid((17, 1), (0, 0), colspan=1, rowspan=8)
	ax2 = pl.subplot2grid((17, 1), (9, 0), colspan=1, rowspan=2)
	ax3 = pl.subplot2grid((17, 1), (12, 0), colspan=1, rowspan=2)
	ax4 = pl.subplot2grid((17, 1), (15, 0), colspan=1, rowspan=2)

	# Raster Plot
	simple_raster(spk_times_plot, spk_ids_plot, neuron_ids, 'k', ax1, 1.)
	ax1.set_xlim([time_interval[0], time_interval[1]])
	ax1.set_xlabel('')
	ax1.set_xticks([])

	# PSTH
	pa = np.fliplr(spk_list.raw_data())
	t_bins, heights = spike_histogram(pa, 1.)
	ax2.bar(t_bins, heights, width=1., color='k', edgecolor='k')
	ax2.set_ylabel('Firing Rate [Hz]')
	ax2.set_xlabel('')
	ax2.set_xlim([time_interval[0], time_interval[1]])
	ax2.set_xticks([])
	ax2.set_ylim((0., max(heights)+1))

	# Synaptic Currents (of one neuron)
	Ex_Currents = analogue_vars['g_e'] * (analogue_vars['V'] - 0.)
	In_Currents = analogue_vars['g_i'] * (analogue_vars['V'] - (-80.))

	ax3.plot(total_time_vector[:len(Ex_Currents)], Ex_Currents/1000., 'b', label='Exc. Currents', linewidth=2)
	ax3.plot(total_time_vector[:len(Ex_Currents)], In_Currents/1000., 'r', label='Inh. Currents', linewidth=2)
	ax3.plot(total_time_vector[:len(Ex_Currents)], (Ex_Currents + In_Currents) / 1000, 'k', label='Total', linewidth=2)
	ax3.set_xlim(time_interval[0], time_interval[1])
	ax3.set_ylim((min(Ex_Currents) / 1000) - 5, (max(In_Currents) / 1000) + 5)
	ax3.set_ylabel('Syn. Currents [nA]', fontsize=10)
	ax3.set_xticks([])

	# Single Neuron Vm
	ax4.plot(total_time_vector[:len(analogue_vars['V'])], analogue_vars['V'], 'k')
	for n in spk_times_sing:
		if time_interval[1] > n > time_interval[0]:
			ax4.vlines(n, -50., 0., color='k', linewidth=1.)
	ax4.set_xlim(time_interval[0], time_interval[1])
	ax4.set_ylim(-75, 2)
	ax4.set_ylabel('Mem. Pot. [mV]')
	ax4.set_xlabel('Time [ms]')

	pl.draw()

	#================================ FIG 2 ====================================
	fig2 = pl.figure()
	fig2.suptitle('Background activity statistics', fontsize=16)
	ax21 = pl.subplot2grid((2, 2), (0, 0), colspan=1, rowspan=1)
	ax22 = pl.subplot2grid((2, 2), (0, 1), colspan=1, rowspan=1)
	ax23 = pl.subplot2grid((2, 2), (1, 0), colspan=1, rowspan=1)
	ax24 = pl.subplot2grid((2, 2), (1, 1), colspan=1, rowspan=1)

	# Rate Distributions
	#spk_list.rate_distribution(nbins=100, normalize=True, display=ax21)
	tmpa = spk_list.mean_rates()
	weights = np.ones_like(tmpa) / len(tmpa)
	n, bins, patches = ax21.hist(tmpa, 100, weights=weights, histtype='stepfilled', alpha=0.8)
	pl.setp(patches, 'facecolor', 'b')
	ax21.set_title('Firing Rates')
	ax21.set_xlim((min(bins)-1, max(bins)+1))
	ax21.set_ylim((min(n)-0.1, max(n)+0.1))

	# Mean Vm
	weights = np.ones_like(analogue_vars['Vms']) / len(analogue_vars['Vms'])
	n, bins, patches = ax22.hist(analogue_vars['Vms'], 100, weights=weights, histtype='stepfilled', alpha=0.8)
	pl.setp(patches, 'facecolor', 'b')#, 'alpha', 0.75)
	ax22.set_title('Average V_{m}')
	ax22.set_xlim((min(bins)-1, max(bins)+1))
	ax22.set_ylim((min(n)-0.1, max(n)+0.1))
	ax22.set_xlabel('V_{m}')
	#ax2.set_ylabel('% of neurons')

	# ISI
	#spk_list.isi_hist(bins=100, display=ax23)
	tmpa = spk_list.isi()
	tmp = list(itertools.chain(*tmpa))
	weights = np.ones_like(tmp) / len(tmp)
	n, bins, patches = ax23.hist(tmp, 100, weights=weights, histtype='stepfilled', alpha=0.8)
	pl.setp(patches, 'facecolor', 'b')
	#ax23.set_title('ISI')
	ax23.set_xlabel('ISI (ms)')
	ax23.set_xlim((min(bins)-1, max(bins)+1))
	ax23.set_ylim((0., max(n)))
	#ax23.set_xscale('log')

	## CV-ISI dist.
	#spk_list.cv_isi_hist(bins=100, display=ax24)
	tmpa = spk_list.cv_isi(True)
	weights = np.ones_like(tmpa) / len(tmpa)
	n, bins, patches = ax24.hist(tmpa, 100, weights=weights, histtype='stepfilled', alpha=0.8)
	pl.setp(patches, 'facecolor', 'b')
	ax24.set_xlim((min(bins)-1, max(bins)+1))
	ax24.set_ylim((0., max(n)+0.1))
	#ax24.set_title('CV-ISI')

	pl.show(block=False)


########################################################################################################################
def plot_weights_mat(E_neurons, I_neurons):
	"""
	Extracts and plots Weight matrices
	"""

	W_EE = np.zeros([len(E_neurons), len(E_neurons)])
	W_EI = np.zeros([len(I_neurons), len(E_neurons)])
	W_IE = np.zeros([len(E_neurons), len(I_neurons)])
	W_II = np.zeros([len(I_neurons), len(I_neurons)])

	a_EE = nest.GetConnections(E_neurons, E_neurons)
	c_EE = nest.GetStatus(a_EE, keys='weight')
	a_EI = nest.GetConnections(I_neurons, E_neurons)
	c_EI = nest.GetStatus(a_EI, keys='weight')
	a_IE = nest.GetConnections(E_neurons, I_neurons)
	c_IE = nest.GetStatus(a_IE, keys='weight')
	a_II = nest.GetConnections(I_neurons, I_neurons)
	c_II = nest.GetStatus(a_II, keys='weight')

	for idx, n in enumerate(a_EE):
		W_EE[n[0]-min(E_neurons), n[1]-min(E_neurons)] += c_EE[idx]
	for idx, n in enumerate(a_EI):
		W_EI[n[0]-min(I_neurons), n[1]-min(E_neurons)] += c_EI[idx]
	for idx, n in enumerate(a_IE):
		W_IE[n[0]-min(E_neurons), n[1]-min(I_neurons)] += c_IE[idx]
	for idx, n in enumerate(a_II):
		W_II[n[0]-min(I_neurons), n[1]-min(I_neurons)] += c_II[idx]

	#===================================================================
	fig = pl.figure()

	fig.suptitle('Weights Matrices', fontsize=14)
	gs = gridspec.GridSpec(4,4)
	ax1 = pl.subplot(gs[:-1,:-1])
	ax2 = pl.subplot(gs[:-1,-1])
	ax3 = pl.subplot(gs[-1,:-1])
	ax4 = pl.subplot(gs[-1,-1])

	plt1 = ax1.imshow(W_EE)
	plt1.set_cmap('jet')
	divider = make_axes_locatable(ax1)
	cax = divider.append_axes("right", "5%", pad="3%")
	pl.colorbar(plt1, cax=cax)
	ax1.set_title('W_{EE}')
	pl.tight_layout()

	plt2 = ax2.imshow(W_IE)
	plt2.set_cmap('jet')
	divider = make_axes_locatable(ax2)
	cax = divider.append_axes("right", "5%", pad="3%")
	pl.colorbar(plt2, cax=cax)
	ax2.set_title('W_{EI}')
	pl.tight_layout()

	plt3 = ax3.imshow(W_EI)
	plt3.set_cmap('jet')
	divider = make_axes_locatable(ax3)
	cax = divider.append_axes("right", "5%", pad="3%")
	pl.colorbar(plt3, cax=cax)
	ax3.set_title('W_{IE}')
	pl.tight_layout()

	plt4 = ax4.imshow(W_II)
	plt4.set_cmap('jet')
	divider = make_axes_locatable(ax4)
	cax = divider.append_axes("right", "5%", pad="3%")
	pl.colorbar(plt4, cax=cax)
	ax4.set_title('W_{II}')
	pl.tight_layout()

	pl.show(block=False)


########################################################################################################################
def plot_spike_template_input(Background, SpkTrains, step_signal, time_interval):
	"""
	Plot an example of the input spiking signal containing spike templates...
	"""

	fig1 = pl.figure()
	ax1 = pl.subplot2grid((20, 1), (0, 0), colspan=1, rowspan=17)
	ax2 = pl.subplot2grid((20, 1), (18, 0), colspan=1, rowspan=2)

	'''
	Background.raster_plot(display=ax1, kwargs={'color': 'gray'})
	Colors = ['red', 'green', 'blue']
	for n in range(len(SpkTrains)):
		SpkTrains[n].raster_plot(display=ax1, kwargs={'color': Colors[n]})
		ax2.plot(step_signal[n], Colors[n], linewidth=2)
	ax1.set_xlim(time_interval)
	ax1.set_xticklabels([])
	ax1.set_xlabel('')
	ax2.set_xlim(time_interval)
	ax2.set_yticks([0., 1.05], [0., 1.])
	ax2.set_xlabel('Time [ms]', fontsize=12)

	pl.show(block=False)
	'''


	fig1 = pl.figure()
	ax1 = pl.subplot2grid((20, 1), (0, 0), colspan=1, rowspan=17)
	ax2 = pl.subplot2grid((20, 1), (18, 0), colspan=1, rowspan=2)

	bg = Background.time_slice(0., 2000.)
	data = bg.raw_data()
	times = data[:, 0]
	ids = data[:, 1]
	neuron_ids = np.arange(0., 100., 1.)

	simple_raster(times, ids, neuron_ids, 'gray', ax1, 1.)
	ax1.set_xlim([0., 2000.])
	ax1.set_xlabel('')
	ax1.set_xticks([])

	Colors = ['DarkOrange', 'Indigo', 'Lime']
	for n in range(len(SpkTrains)):
		spks = SpkTrains[n].time_slice(0., 2000.)
		data = spks.raw_data()
		times = data[:, 0]
		ids = data[:, 1]

		simple_raster(times, ids, neuron_ids, Colors[n], ax1, 1.)

	# PSTH
	pa = np.fliplr(Combined.raw_data())
	t_bins, heights = spike_histogram(pa, 1.)
	ax2.bar(t_bins, heights, width=1., color='k', edgecolor='k')
	ax2.set_ylabel('Firing Rate [Hz]')
	ax2.set_xlabel('')
	ax2.set_xlim([0., 2000.])
	ax2.set_xticks([])
	ax2.set_ylim((0., max(heights) + 1))

########################################################################################################################
def plot_input_example_burst(Input_Pars_Dict, Grammar, rate_signal, step_signal, time_interval):
	"""Displays an example of the input structure for a randomly selected
	input symbol. Arguments:
		- Input_Pars_Dict: dictionary of input parameters (see Parameters)
		- Grammar: FSG object generated by SymbolSeqTools
		- rate_signal: full rate signal
		- step_signal: full step signal
		- time_interval [start, stop]: range for the plots
	"""

	### some variables
	k = len(Grammar.alphabet)
	idx = np.random.random_integers(0, k-1)
	nIU = Input_Pars_Dict['nInputTrains']
	dt  = Input_Pars_Dict['resolution']
	amp = Input_Pars_Dict['peak_amp']
	fRates = rate_signal[idx][time_interval[0]:time_interval[1]]
	step   = step_signal[idx][time_interval[0]:time_interval[1]]
	time_data = np.arange(time_interval[0], time_interval[1], dt)

	### some common plot properties
	title = 'Encoding of symbol {0}'.format(Grammar.alphabet[idx])
	linewidth = 2.5

	#======================
	fig = pl.figure()
	fig.suptitle(title, fontsize=14)
	ax1 = pl.subplot2grid((6, 3), (0, 0), colspan=3, rowspan=4)
	ax2 = pl.subplot2grid((6, 3), (4, 0), colspan=3, sharex=ax1)
	ax3 = pl.subplot2grid((6, 3), (5, 0), colspan=3, sharex=ax1)

	# generate example Poisson spike trains:
	x = gen.StGen()
	STrains = []
	spList = []
	idsList = []
	for i in range(int(nIU)):
		STrains.append(gen.StGen.inh_poisson_generator(x, fRates, time_data, max(time_data)))
		tmp = STrains[i].spike_times
		for t in tmp:
			spList.append((i, t))
		idsList.append(i)

	SpK_list = signals.SpikeList(spList, idsList)

	p = nest.raster_plot.extract_events(spList)

	SpK_list.raster_plot(display=ax1, t_start=time_interval[0], t_stop = time_interval[1])
	ax1.set_xlim([time_interval[0], time_interval[1]])
	ax1.set_xlabel('')
	ax1.set_xticks([])

	t_bins, heights = spike_histogram(p, 5.)
	ax2.bar(t_bins, heights, width=5.)
	ax2.plot(time_data, fRates, linewidth=linewidth, color='red')
	ax2.set_yticks(np.arange(0., amp, amp/4).tolist())
	ax2.set_ylabel('Firing Rate [Hz]')
	ax2.set_xlabel('')
	ax2.set_xlim([time_interval[0], time_interval[1]])
	ax2.set_xticks([])

	ax3.plot(time_data, step, linewidth=linewidth, color='green')
	ax3.set_yticks([0., 1.05], [0., 1.])
	ax3.set_xlabel('Time [s]', fontsize=12)
	ax3.set_xlim([time_interval[0], time_interval[1]])

	pl.show(block=False)


########################################################################################################################
def plot_classification_results_comparison(Res, rules):

	lefts = [2.-0.4, 4-0.4, 6.-0.4, 8-0.4, 10-0.4, 12-0.4, 14-0.4]
	heights_1 = []; heights_2 = []; heights_3 = []
	for n in Res:
		heights_1.append(n['mPerf'])
		heights_2.append(n['MSE'])
		heights_3.append(n['Hamm_loss'])
	labels = rules

	fig1 = pl.figure()
	ax1 = fig1.add_subplot(131)
	ax2 = fig1.add_subplot(132)
	ax3 = fig1.add_subplot(133)
	ax1.bar(lefts, heights_1, color='g')
	ax1.plot(range(len(lefts)*2+1), np.ones(len(lefts)*2+1)*(1./3.), '--r', linewidth=2)
	ax1.set_xticks([2, 4, 6, 8, 10, 12, 14])
	ax1.set_xticklabels(labels)
	ax1.set_ylabel('Performance')
	ax2.bar(lefts, heights_2, color='b')
	ax2.set_xticks([2, 4, 6, 8, 10, 12, 14])
	ax2.set_xticklabels(labels)
	ax2.set_ylabel('MSE')
	ax3.bar(lefts, heights_3, color='y')
	ax3.set_xticks([2, 4, 6, 8, 10, 12, 14])
	ax3.set_xticklabels(labels)
	ax3.set_ylabel('Hamming Loss')

	pl.show(block=False)


########################################################################################################################
def plot_multi_input(rate_signal, input_spk_list, time_interval=[2700.0, 7700.0]):
	"""
	Create a single figure with 3 raster plots of the input signal...
	"""
	fig = pl.figure()
	left, width = 0.1, 0.8
	rect1 = [left, 0.65, width, 0.3] #left, bottom, width, height
	rect2 = [left, 0.35, width, 0.3]
	rect3 = [left, 0.05, width, 0.3]

	ax1 = fig.add_axes(rect1, axisbg='DarkOrange')
	ax1t = ax1.twinx()
	ax1.set_xticklabels([])
	ax1.set_ylabel('Neuron')
	ax1t.set_ylabel('Rate')
	ax1t.set_ylim([min(rate_signal[0]) - 20, max(rate_signal[0]) + 20])
	ax2 = fig.add_axes(rect2, axisbg='Indigo', sharex=ax1)
	ax2t = ax2.twinx()
	ax2.set_xticklabels([])
	ax2.set_ylabel('Neuron')
	ax2t.set_ylabel('Rate')
	ax2t.set_ylim([min(rate_signal[1])-20, max(rate_signal[1])+20])
	ax3 = fig.add_axes(rect3, axisbg='Lime', sharex=ax1)
	ax3t = ax3.twinx()
	ax3.set_xlabel('Time [ms]')
	ax3.set_ylabel('Neuron')
	ax3t.set_ylabel('Rate')
	ax3t.set_ylim([min(rate_signal[2])-20, max(rate_signal[2])+20])

	for n in range(3):
		spks = input_spk_list[n].raw_data()
		spkT = spks[:, 0]
		spkN = spks[:, 1]
		spk_times = [spkT[nn] for nn,t in enumerate(spkT) if time_interval[1] > t > time_interval[0]]
		spk_ids = [spkN[nn] for nn,t in enumerate(spkT) if time_interval[1] > t > time_interval[0]]
		neuron_ids = np.unique(spkN)
		simple_raster(spk_times, spk_ids, neuron_ids, 'k', globals()['ax{0}'.format(str(n+1))], 1.)
		globals()['ax{0}t'.format(str(n+1))].plot(rate_signal[n], color='white', alpha=0.6)
		globals()['ax{0}'.format(str(n + 1))].set_xlim([time_interval[0], time_interval[1]])
	ax3.set_xticks([10000., 11000., 12000., 13000., 14000., 15000.])
	pl.show()


########################################################################################################################
def print_topology(ELayer, ILayer):
	"""
	Prints and plots network topology and connections
	"""
	import nest.topology as tp

	# Print network
	nest.PrintNetwork(depth=3)

	fig = tp.PlotLayer(ELayer, nodesize=20, nodecolor='blue')
	tp.PlotLayer(ILayer, fig=fig, nodesize=20, nodecolor='red')
	pl.show(block=False)


########################################################################################################################
def plot_input_neurons(E_neurons, I_neurons, Input_neurons, pars_dict):
	"""
	Visualize the input neurons positions in space
	"""

	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1

	fig = pl.figure()
	labels = alphabet
	cm = pl.get_cmap('Paired')
	fig.suptitle('Input Neurons', fontsize=16)
	ax = pl.axes()

	for k in range(len(Input_neurons)):
		Pos = []
		color = cm(1.*k/len(Input_neurons))
		idxs = Input_neurons[k]
		if 'E' in Input_Population:
			for n in idxs:
				Pos.append(Pos_E[E_neurons.index(n)])

		elif 'I' in Input_Population:
			for n in idxs:
				Pos.append(Pos_I[I_neurons.index(n)])

		elif 'EI' in Input_Population:
			neur_list = list(itertools.chain(*[E_neurons, I_neurons]))
			pos_list = list(itertools.chain(*[Pos_E, Pos_I]))
			for n in idxs:
				Pos.append(pos_list[neur_list.index(n)])

		Pos = np.array(Pos)
		ax.plot(Pos[:, 0], Pos[:, 1], 'o', c=color, label=labels[k], markersize=10)

	ax.legend(loc='upper left', fontsize=14)
	pl.draw()
	pl.show(block=False)


########################################################################################################################
def plot_topology(Topology_dict, colors, ax=[]):


	for k, v in Topology_dict.items():
		globals()[k] = v

	if not len(ax):
		fig, ax = pl.subplots()

	datafile = Topology_dict['stored_data']
	datafile = '/home/neuro/Desktop/EuroSPIN/CODE/sequencelearning/InputEncoding' + datafile[1:]
	Data = extract_data_fromfile(datafile)
	Ids = Data[:, 0]
	xPos = Data[:, 1]
	yPos = Data[:, 2]

	E_Pos = np.zeros((len(E_neurons), 2))
	I_Pos = np.zeros((len(I_neurons), 2))
	for idx, n in enumerate(E_neurons):
		E_Pos[idx, :] = [xPos[np.where(Ids == n)[0][0]], yPos[np.where(Ids == n)[0][0]]]
	for idx, n in enumerate(I_neurons):
		I_Pos[idx, :] = [xPos[np.where(Ids == n)[0][0]], yPos[np.where(Ids == n)[0][0]]]
	ax.plot(E_Pos[:, 0], E_Pos[:, 1], '^b', markersize=8)
	ax.plot(I_Pos[:, 0], I_Pos[:, 1], 'or', markersize=5)

	if len(np.array(Input_neurons).shape) == 1:
		inp_Pos = np.zeros((len(Input_neurons), 2))
		for idx, nn in enumerate(Input_neurons):
			inp_Pos[idx, :] = [xPos[np.where(Ids == nn)[0][0]], yPos[np.where(Ids == nn)[0][0]]]

		ax.plot(inp_Pos[:, 0], inp_Pos[:, 1], 'og', label='Input Neurons', markersize=4)

	else:
		inp_Pos = []
		for n in range(len(Input_neurons)):
			pp = np.zeros((len(Input_neurons[n]), 2))
			for idx, nn in enumerate(Input_neurons[n]):
				pp[idx, :] = [xPos[np.where(Ids == nn)[0][0]], yPos[np.where(Ids == nn)[0][0]]]
			inp_Pos.append(pp)

			ax.plot(inp_Pos[n][:, 0], inp_Pos[n][:, 1], 'o', color=colors[n], label='Input Population {0}'.format(
				str(n)), markersize=4)

	ax.set_xlim([-1., 101.])
	ax.set_ylim([-1., 101.])


########################################################################################################################
def activity_map(topology_dict, pars_dict, act_spk_list, time_interval, cm='jet', display=True):
	"""
	...
	"""
	dims = np.sqrt(pars_dict['Net_Pars']['N'])

	positions = np.zeros((pars_dict['Net_Pars']['N'], 2))
	spk_list = act_spk_list.time_slice(time_interval[0], time_interval[1])
	rates = spk_list.mean_rates()
	act_map = np.zeros((dims, dims))
	if pars_dict['Options']['Topology'] == 'None':

		for ids, idx in enumerate(itertools.chain(*[topology_dict['E_neurons'], topology_dict['I_neurons']])):
			x = ids % dims
			y = np.floor(ids / dims)
			positions[ids, :] = [x, y]
			act_map[int(x), int(y)] = rates[ids]
	else:
		datafile = topology_dict['stored_data']
		if datafile[0] == '.':
			datafile = '/home/neuro/Desktop/EuroSPIN/CODE/sequencelearning/InputEncoding'+datafile[1:]
		elif datafile[6:8] == 'fr':
			datafile = '/media/neuro/00416BFA16D1B14D/DATA/SingleNetData/BurstInput_PlasticNet__PatRate=50' \
			           '.0_nInputTrains=100.0_IU=E_nIU=80.0_LAYERS' #'/home/neuro/bwUniCl/'+datafile[
			# 25:]
		Data = extract_data_fromfile(datafile)
		Ids = Data[:, 0]
		xPos = Data[:, 1]
		yPos = Data[:, 2]

		for ids, idx in enumerate(itertools.chain(*[topology_dict['E_neurons'], topology_dict['I_neurons']])):
			dd = np.where(Ids == idx)[0][0]
			positions[ids, :] = [xPos[dd], yPos[dd]]

			if pars_dict['Options']['Topology'] == 'lattice':
				act_map[int(positions[ids, 0]), int(positions[ids, 1])] = rates[ids]
			else:
				act_map[int(round(positions[idx, 0])), int(round(positions[idx, 1]))] += rates[ids]

	if display:
		fig = pl.figure()
		ax1 = fig.add_subplot(121)
		im = ax1.scatter(positions[:, 0], positions[:, 1], c=rates, cmap=cm)
		pl.colorbar(im)
		ax1.set_xlim([0., dims])
		ax1.set_ylim([0., dims])
		ax2 = fig.add_subplot(122)
		im2 = ax2.imshow(act_map, cmap=cm)
		pl.colorbar(im2)
		pl.show(block=False)

	return act_map


##############################################################################
# Auxiliary sub-functions
#=============================================================================
def simple_raster(spk_times, spk_ids, neuron_ids, color, ax, lw):
	"""
	Create a simple line raster plot
	"""

	new_spk_ids = []
	for i in spk_ids:
		new_spk_ids.append(np.where(i == neuron_ids)[0][0])

	#for idx, n in enumerate(spk_times):
	#	ax.vlines(n, new_spk_ids[idx]+0.5, new_spk_ids[idx]+1.5, color=color, linewidth=lw)
	#ax.set_ylim(min(new_spk_ids)+.5, max(new_spk_ids)+.5)

	ax.plot(spk_times, new_spk_ids, linestyle='None', marker='o', markerfacecolor=color,
	        markeredgecolor=color, markersize=lw)

	return new_spk_ids


#========================================================================
def spike_histogram(data, hist_binwidth):

	ts = data[:, 1]
	neurons = data[:, 0]

	t_bins = np.arange(np.amin(ts), np.amax(ts), float(hist_binwidth))
	n, bins = _histogram(ts, bins=t_bins)
	num_neurons = len(np.unique(neurons))
	heights = 1000 * n / (hist_binwidth * num_neurons)
	return t_bins, heights


#========================================================================
def _histogram(a, bins=10, range=None, normed=False):

	from numpy import asarray, iterable, linspace, sort, concatenate

	a = asarray(a).ravel()
	if range is not None:
		mn, mx = range
		if mn > mx:
			raise AttributeError, "max must be larger than min in range parameter."
	if not iterable(bins):
		if range is None:
			range = (a.min(), a.max())
			mn, mx = [mi + 0.0 for mi in range]
		if mn == mx:
			mn -= 0.5
			mx += 0.5
		bins = linspace(mn, mx, bins, endpoint=False)
	else:
		if(bins[1:] - bins[:-1] < 0).any():
			raise AttributeError, "bins must increase monotonically."
	# best block size probably depends on processor cache size
	block = 65536
	n = sort(a[:block]).searchsorted(bins)
	for i in xrange(block, a.size, block):
		n += sort(a[i:i + block]).searchsorted(bins)
	n = concatenate([n, [len(a)]])
	n = n[1:] - n[:-1]

	if normed:
		db = bins[1] - bins[0]
		return 1.0 / (a.size * db) * n, bins
	else:
		return n, bins



'''def arrowplot(axes, x, y, narrs=30, dspace=0.5, direc='pos', hl=0.3, hw=6, c='black'):
	"""
	narrs  :  Number of arrows that will be drawn along the curve
	dspace :  Shift the position of the arrows along the curve. Should be between 0. and 1.
	direc  :  can be 'pos' or 'neg' to select direction of the arrows
	hl     :  length of the arrow head
	hw     :  width of the arrow head
	c      :  color of the edge and face of the arrow head
	"""
	# r is the distance spanned between pairs of points
	r = [0]
	for i in range(1, len(x)):
		dx = x[i]-x[i-1]
		dy = y[i]-y[i-1]
		dz = z[i]-z[i-1]
		r.append(np.sqrt(dx*dx+dy*dy+dz*dz))
		r = np.array(r)

	# rtot is a cumulative sum of r, it's used to save time
	rtot = []
	for i in range(len(r)):
		rtot.append(r[0:i].sum())
	rtot.append(r.sum())

	# based on narrs set the arrow spacing
	aspace = r.sum() / narrs

	if direc is 'neg':
		dspace = -1.*abs(dspace)
	else:
		dspace = abs(dspace)

	arrowData = []  # will hold tuples of x,y,theta for each arrow
	arrowPos = aspace*(dspace)  # current point on walk along data
                                # could set arrowPos to 0 if you want
                                #  an arrow at the beginning of the curve

	ndrawn = 0
	rcount = 1
	while arrowPos < r.sum() and ndrawn < narrs:
		x1, x2 = x[rcount-1], x[rcount]
		y1, y2 = y[rcount-1], y[rcount]
		z1, z2 = z[rcount-1], z[rcount]
		da = arrowPos-rtot[rcount]
		theta = np.arctan2((x2-x1), (y2-y1), (z2-z1))
		ax = np.sin(theta)*da+x1
		ay = np.cos(theta)*da+y1
		az = np.cos()
		arrowData.append((ax,ay,theta))
		ndrawn += 1
		arrowPos+=aspace
		while arrowPos > rtot[rcount+1]:
			rcount+=1
			if arrowPos > rtot[-1]:
				break

	# could be done in above block if you want
	for ax,ay,theta in arrowData:
	# use aspace as a guide for size and length of things
	# scaling factors were chosen by experimenting a bit

	dx0 = np.sin(theta)*hl/2. + ax
	dy0 = np.cos(theta)*hl/2. + ay
	dx1 = -1.*np.sin(theta)*hl/2. + ax
	dy1 = -1.*np.cos(theta)*hl/2. + ay

	if direc is 'neg':
		ax0 = dx0
		ay0 = dy0
		ax1 = dx1
		ay1 = dy1
	else:
		ax0 = dx1
		ay0 = dy1
		ax1 = dx0
		ay1 = dy0

	axes.annotate('', xy=(ax0, ay0), xycoords='data', xytext=(ax1, ay1), textcoords='data',
	              arrowprops=dict(headwidth=hw, frac=1., ec=c, fc=c))


	axes.plot(x,y, color = c)
	axes.set_xlim(x.min()*.9,x.max()*1.1)
	axes.set_ylim(y.min()*.9,y.max()*1.1)
	'''