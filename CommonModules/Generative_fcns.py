__author__ = 'duarte'
########################################################################################################################
#                                              GENERATIVE FUNCTIONS
########################################################################################################################
from CommonModules.Auxiliary_fcns import *
import nest
import numpy as np
import itertools
import nest.topology as tp


def set_positions(pars_dict):
	##############################################################################
	# Extract Parameters:
	#############################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1
	##############################################################################
	# No topological features
	##############################################################################
	if Topology == 'None':
		pos = []

	##############################################################################
	# Random positions
	##############################################################################
	else:
		if Topology == 'random':
			## Set Neuron Positions (randomly in [0., sqrt(N)]...)
			pos = (np.sqrt(N) * np.random.random_sample((int(N), 2))).tolist()

		elif Topology == 'lattice':
			## Set Neuron Positions (corresponding to integer points...)
			assert (np.sqrt(N) % 1) == 0., 'Please choose a value of N with an integer sqrt..'
			xs = np.linspace(0., np.sqrt(N) - 1, np.sqrt(N))
			pos = [[x, y] for y in xs for x in xs]
			np.random.shuffle(pos)

	return pos


def set_layers_and_models(pars_dict, display=False):
	####################################################################################################################
	# Extract Parameters:
	####################################################################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1
	Topology_dict = dict()
	####################################################################################################################
	# No topological features
	####################################################################################################################
	if Topology == 'None':
		E_neurons, I_neurons = set_network(pars_dict)

		Topology_dict['E_neurons'] = E_neurons
		Topology_dict['I_neurons'] = I_neurons
		input_neuron_ids = set_input_neurons(pars_dict, E_neurons, I_neurons)

	else:
		################################################################################################################
		## Set Neuron Models
		################################################################################################################
		Neuron_Pars_Dict = {'V_m': E_L,
		                    'E_L': E_L,
		                    'C_m': C_m,
		                    't_ref': tau_ref,
		                    'V_th': V_th,
		                    'V_reset': V_reset,
		                    'E_ex': E_ex,
		                    'E_in': E_in,
		                    'g_L': g_leak,
		                    'tau_syn_ex': tau_E,
		                    'tau_syn_in': tau_I,
		                    'I_e': I_b,
		                    'tau_minus': tau_minus}

		nest.SetDefaults(Neuron_model, Neuron_Pars_Dict)

		################################################################################################################
		# Set Layer dictionaries:
		################################################################################################################
		common_layer_props = {'center': [np.round(np.sqrt(N)) / 2.,
		                                 np.round(np.sqrt(N)) / 2.],
		                      'extent': [np.round(np.sqrt(N)), np.round(np.sqrt(N))],
		                      'edge_wrap': True,
		                      'elements': Neuron_model}

		pos = set_positions(pars_dict)

		E_layer_dict = common_layer_props.copy()
		E_layer_dict.update({'positions': pos[:int(nE)]})
		I_layer_dict = common_layer_props.copy()
		I_layer_dict.update({'positions': pos[int(nE):]})

		E_layer = tp.CreateLayer(E_layer_dict)
		I_layer = tp.CreateLayer(I_layer_dict)

		Topology_dict['E_layer'] = E_layer
		Topology_dict['I_layer'] = I_layer

		if Input_Population['Target'] == 'all':
			# TODO: subdivide into random and positional ???
			if Input_Population['Nature'] == 'E':
				idxs = range(int(nE)); np.random.shuffle(idxs)
				idxs = idxs[:int(nIU)]
				input_neuron_ids = np.array(nest.GetLeaves(E_layer)[0])[idxs].tolist()
			elif Input_Population['Nature'] == 'I':
				idxs = range(int(nI)); np.random.shuffle(idxs)
				idxs = idxs[:int(nIU)]
				input_neuron_ids = np.array(nest.GetLeaves(I_layer)[0])[idxs].tolist()
			elif Input_Population['Nature'] == 'EI':
				idxs = range(int(N)); np.random.shuffle(idxs)
				idxs = idxs[:int(nIU)]
				input_neuron_ids = np.array(list(itertools.chain(*[nest.GetLeaves(E_layer)[0], nest.GetLeaves(I_layer)[
					0]])))[idxs].tolist()

		elif Input_Population['Target'] == 'symbol-specific':
			if 'random' in Input_Population['Extra']:
				assert 'positional' not in Input_Population['Extra'], 'Symbol-specific populations can be either ' \
				                                                      'random or positional, not both...'
				if Input_Population['Nature'] == 'E':
					idxs = range(int(nE)); np.random.shuffle(idxs)
					target = nest.GetLeaves(E_layer)[0]
				elif Input_Population['Nature'] == 'I':
					idxs = range(int(nI)); np.random.shuffle(idxs)
					target = nest.GetLeaves(I_layer)[0]
				elif Input_Population['Nature'] == 'EI':
					idxs = range(int(N)); np.random.shuffle(idxs)
					target = list(itertools.chain(*[nest.GetLeaves(E_layer)[0], nest.GetLeaves(I_layer)[0]]))

				inps = np.arange(0, (len(alphabet)+1)*nIU, nIU)
				input_neuron_ids = []
				for n in range(len(alphabet)):
					idx = idxs[int(inps[n]):int(inps[n+1])]
					input_neuron_ids.append(np.array(target)[idx].tolist())

			elif 'positional' in Input_Population['Extra']:
				assert 'random' not in Input_Population['Extra'], 'Symbol-specific populations can be either ' \
				                                                      'random or positional, not both...'
				if Input_Population['Nature'] == 'E':
					inp_neuron_ids = nest.GetLeaves(E_layer)[0]
				elif Input_Population['Nature'] == 'I':
					inp_neuron_ids = nest.GetLeaves(I_layer)[0]
				elif Input_Population['Nature'] == 'EI':
					inp_neuron_ids = list(itertools.chain(*[nest.GetLeaves(E_layer)[0], nest.GetLeaves(I_layer)[0]]))

				inp_centers = pick_centers(inp_neuron_ids, len(alphabet), Options, Pos=None)

				input_neuron_ids = []
				for n in range(len(alphabet)):
					dists = []
					for idx, nn in enumerate(inp_neuron_ids):
						dists.append(tp.Distance(inp_centers[n], [nn])[0])
					sorted_dist = np.argsort(dists)[:nIU]
					input_neuron_ids.append(np.array(inp_neuron_ids)[sorted_dist].tolist())
					for iii in input_neuron_ids[n]:
						inp_neuron_ids.remove(iii)

	Topology_dict['Input_neurons'] = input_neuron_ids

	return Topology_dict



def plot_topology(Topology_dict):

	import pylab as pl

	for k, v in Topology_dict.items():
		globals()[k] = v

	fig, ax = pl.subplots()

	E_Pos = np.array(tp.GetPosition(nest.GetLeaves(E_layer)[0]))
	I_Pos = np.array(tp.GetPosition(nest.GetLeaves(I_layer)[0]))

	ax.plot(E_Pos[:, 0], E_Pos[:, 1], 'ob', markersize=6)
	ax.plot(I_Pos[:, 0], I_Pos[:, 1], 'or', markersize=6)

	if len(np.array(Input_neurons).shape) == 1:
		inp_Pos = []
		for idx, nn in enumerate(Input_neurons):
			inp_Pos.append(tp.GetPosition([nn])[0])

		inp_Pos = np.array(inp_Pos)
		ax.plot(inp_Pos[:, 0], inp_Pos[:, 1], 'og', label='Input Neurons', markersize=4)

	else:
		import matplotlib.cm as cm
		colors = iter(cm.spectral(np.linspace(0, 1, len(Input_neurons))))
		inp_Pos = []
		for n in range(len(Input_neurons)):
			pp = []
			for idx, nn in enumerate(Input_neurons[n]):
				pp.append(tp.GetPosition([nn])[0])
			inp_Pos.append(np.array(pp))

			ax.plot(inp_Pos[n][:, 0], inp_Pos[n][:, 1], 'o', color=next(colors), label='Input Population {0}'.format(
				str(n)), markersize=4)

	ax.set_xlim([-1., 101.])
	ax.set_ylim([-1., 101.])
	pl.show(block=False)