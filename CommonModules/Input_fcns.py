__author__ = 'neuro'
import numpy as np
from SymbolSeqTools import *
import NeuroTools.stgen_mine as Gen
import NeuroTools.signals_mine as signals
import pickle
from Plotting_routines import *


###========================
def generate_symbol_seq(input_pars_dict):
	"""
	Generates a symbolic temporal sequence according to the rules
	specified in the parameters, by calling the relevant constructors from
	SymbolSeqTools.
	The input dictionary should contain all the necessary entries to generate the
	grammar object:
		- states; alphabet; transition_pr; start_states; end_states;
		options (task); nStrings; int_sym;
		- these parameters must be specified in the Pars_Script and Parameters,
		prior to calling this function
	Note: The input_params_dict should be a python dictionary
	"""

	# Iterate through the dictionary entries and create all the individual variables from them:
	for k, v in input_pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1

	Grammar = FSG(states, alphabet, transitions, start_states, end_states, options)
	StringSet = generate(Grammar, nStrings)
	Seq = concatenate(StringSet, int_sym)
	Seq = correct_symbolseq(Seq, Grammar)

	return Grammar, Seq


#========================================================
def encode_input(grammar, seq, input_pars_dict, noise=False):
	"""
	Encodes a symbolic sequence as a set of k signals (k = |a|)...
	Arguments:
		- grammar: an FSG object grammar
		- seq: a full symbol sequence previously generated (by generate_symbol_seq)
		- input_params_dict: a python dictionary with all input-relevant parameters
		- noise: add WN to the rate signal (more realistic...)
	Output:
		- step_signal: a set of k binary time series, active (1) when symbol k_{n} is present
		in the input
		- rate_signal: a set of k noisy signals to be used as the instantaneous rate for
		inhomogeneous Poisson inputs (if applicable)
		- time_data: vector of time corresponding to the generated signals
	"""

	# Iterate through the dictionary entries and create all the individual variables from them:
	for k, v in input_pars_dict.items():
		globals()[k] = v

	k = len(grammar.alphabet)

	# create binary time series for each symbol unit
	u_hat = np.zeros((k, len(seq)))
	for i, x in enumerate(grammar.alphabet):
		idx = np.where(np.array(list(seq)) == x)
		u_hat[i, idx] = 1.

	# unfold the time series so that each symbol lasts dur and introduce
	# the I_stim_I
	u = np.repeat(u_hat, stim_dur/resolution, 1)
	# include silence:
	step_signal = np.copy(u)
	for i in range(int(I_stim_I)):
		idxs = np.arange(0., np.shape(step_signal)[1], (stim_dur/resolution)+i).tolist()
		step_signal = np.insert(step_signal, idxs, 0., 1)

	# this is justifiable, but may cause problems...)
	# convolve the signal with the kernel functions to generate the
	# firing rate time series for each of the Poisson generators...
	s = np.zeros((np.shape(step_signal)))
	signal = []
	tau_r = 15.
	tau_d = 100.
	x = np.arange(0., 1000., resolution)
	g = np.exp(-x / tau_d) - np.exp(-x / tau_r)
	for i in range(k):
		tmp_a = np.where(step_signal[i])[0]
		tmp_c = np.where(np.diff(tmp_a) > 1.)

		s[i][tmp_a[0]] = 1.
		for n in tmp_c[0]:
			s[i][tmp_a[int(n) + 1]] = 1.
		nw = np.convolve(s[i], g)
		nw = rescale2(nw.tolist(), I_back_r, peak_amp)
		nw = nw[:len(s[i])]
		signal.append(nw)

	rate_signal = np.array(signal)
	time_data = np.arange(0., np.shape(rate_signal)[1], resolution)

	if noise:
		WN = nz_rate * np.random.randn(len(time_data))
		rate_signal += WN
		rate_signal[np.where(rate_signal < 0.)] = 0.

	return step_signal, rate_signal, time_data


#================================
def generate_burst_spktrains(grammar, input_params_dict, rate_signal, step_signal, time_data, display=False):
	"""
	Converts the rate_signal (which represents the firing rate of
	inhomogeneous Poisson processes) into spike train objects, to be
	delivered to the input neuron populations..
	Returns a list of k NeuroTools spikelist objects...
	"""
	x = Gen.StGen()
	k = len(grammar.alphabet)
	nIU = input_params_dict['nInputTrains']
	SpkTimes = []; SpkT = []
	SpkIds = []; SpkId = []
	SpkTrains = []

	for n in range(k):
		rate = rate_signal[n]
		SpkT = []; SpkId = []
		for i in range(int(nIU)):
			tmp = Gen.StGen.inh_poisson_generator(x, rate, time_data, max(time_data))
			tmp1 = np.ones(len(tmp.spike_times))
			SpkT.append(tmp.spike_times)
			SpkId.append(i*tmp1)
		SpkTimes.append(np.hstack(SpkT))
		SpkIds.append(np.hstack(SpkId))

		tmp = [(SpkIds[n][nn], SpkTimes[n][nn]) for nn in range(len(SpkTimes[n]))]
		SpkTrains.append(signals.SpikeList(tmp, np.unique(SpkIds[n])))

	if display:
		time_interval = [0., 2000.]
		plot_input_example_burst(input_params_dict, grammar, rate_signal, step_signal, time_interval)
		plot_multi_input(rate_signal, SpkTrains, time_interval=[2700.0, 7700.0])

	return SpkTrains


def generate_frozen_spiketrains(grammar, input_pars, step_signal, time_data, display=False):
	"""
	Generates a sequence of spiking activity, with Frozen Poisson spike
	patterns (# = |a|), embedded in a noisy background activity
	"""
	for k1, v in input_pars.items():
		globals()[k1] = v
	x = Gen.StGen()
	symbols = grammar.alphabet
	k = len(symbols)
	rates = np.zeros(np.shape(step_signal))
	for i in range(k):
		a = np.where(step_signal[i])
		b = np.ones(len(step_signal[i]))
		b *= I_back_r
		b[a] = nz_rate
		rates[i] = b

	# generate spike templates
	spike_templates = store_templates(k, int(nInputTrains), peak_amp, stim_dur, ToFile=False)

	# generate background signal (nInputTrains Poisson spike trains with rate rates)
	rt = np.sum(rates, 0)/3.
	rt[np.where(rt < I_back_r)] = nz_rate

	background_times = []; background_ids = []
	for n in range(int(nInputTrains)):
		background = Gen.StGen.inh_poisson_generator(x, rt, time_data, max(time_data))
		background_times.append(background.spike_times)
		background_ids.append(n*np.ones(len(background.spike_times)))

	BID = np.hstack(background_ids)
	BT = np.hstack(background_times)
	tmp = [(BID[nn], BT[nn]) for nn in range(len(BID))]
	Background = signals.SpikeList(tmp, np.unique(BID))

	# add spike templates (all templates have times relative to 0...)
	SpkTrains = []
	SpkTimes = []
	SpkIds = []
	for n in range(k):
		SpkId = []; SpkT = []
		s = np.zeros(np.shape(step_signal))

		tmpa = np.where(step_signal[n])[0]
		tmpc = np.where(np.diff(tmpa) > 1.)

		s[n][tmpa[0]] = 1.
		for nn in tmpc[0]:
			s[n][tmpa[int(nn) + 1]] = 1.

		template_starts = np.where(s[n])[0]

		for t in template_starts:
			for nn in range(len(spike_templates[n])):
				SpkT.append(spike_templates[n][nn]+t)
				Id = np.ones(len(spike_templates[n][nn]))
				SpkId.append(nn*Id)

		SpkTimes.append(np.hstack(SpkT))
		SpkIds.append(np.hstack(SpkId))

		tmp = [(SpkIds[n][nn], SpkTimes[n][nn]) for nn in range(len(SpkTimes[n]))]
		SpkTrains.append(signals.SpikeList(tmp, np.unique(SpkIds[n])))

	Combined = Background.copy()

	for n in range(k):
		Combined.merge(SpkTrains[n])

	if display:
		time_interval = [0., 2000.]
		plot_spike_template_input(Background, SpkTrains, step_signal, time_interval)

	return Combined


def generate_template(nTrains, rate, duration):
	"""
	Generate input spike patterns to use as templates for the determinations
	of computational power. The templates are then stored, so they can be re-used
	"""

	x = Gen.StGen(); Times = []; Ids = []

	for n in range(nTrains):
		Times.append(Gen.StGen.poisson_generator(x, rate, t_start = 0., t_stop = duration, array=True))
		Ids.append(n)

	return Times


def store_templates(nTemplates, nTrains, rate, duration, ToFile=True):
	"""
	Calls generate_template function to generate nTemplates template
	spike patters composed of nTrains Poisson spike trains at a rate
	rate and duration.
	Stores the generated templates in a file...
	"""

	fileName = open('Spike_Templates', 'w')
	Spk_Templates = []
	for n in range(nTemplates):
		Spk_Templates.append(generate_template(nTrains, rate, duration))

	if ToFile:
		pickle.dump(Spk_Templates, fileName)

		fileName.close()
	else:
		return Spk_Templates


def generate_jittered_templates(nTemplates, nJittered, nTrains, rate, duration, jitter_mu, jitter_sig):

	Spk_Templates = [];
	Jittered_Trains = [];
	for n in range(nTemplates):
		Spk_Templates.append(generate_template(nTrains, rate, duration))

		for nn in range(nJittered):
			Jittered_Train = []
			for nnn in range(nTrains):
				Jittered_Train.append((Spk_Templates[n][nnn]) + (jitter_sig * numpy.random.randn(len(Spk_Templates[n][nnn])) + jitter_mu))

			Jittered_Trains.append(Jittered_Train)
		return Jittered_Trains


def store_jittered_templates(Jittered_Trains):

	fileName = open('Jittered_templates', 'w')

	pickle.dump(Jittered_Trains, fileName)

	fileName.close()


def rescale2(OldList, NewMin, NewMax):
	NewRange = float(NewMax - NewMin)
	OldMin = min(OldList)
	OldMax = max(OldList)
	OldRange = float(OldMax - OldMin)
	ScaleFactor = NewRange / OldRange
	NewList = []
	for OldValue in OldList:
		NewValue = ((OldValue - OldMin) * ScaleFactor) + NewMin
		NewList.append(NewValue)
	return NewList


'''
def set_signal_amplitudes(rate_signal, step_signal, grammar, seq):
	"""
	Re-set the peak amplitude of the input signal according to the transition probabilities
	"""

	for idx, t in enumerate(seq):
'''