__author__ = 'duarte'

import pickle
from Analysis_fcns import *
from Plotting_routines import *
import numpy as np
import time
import glob
import NeuroTools.signals_mine as signals


def process_rawdata(path, raw_files, res_file_name):
	"""
	Reads single trial data (single symbol presentation) as stored in the simulations (spiking responses to each
	individual symbol) and processes it, creating a state matrix

	Data must be located in 'path/Raw_Data/' and have the name structure 'raw_files_Response_{0}_to_{1}'. Additionally,
	the results dictionary with all the relevant parameter dictionaries must be stored in 'path/' with file name
	'res_file_name'
	"""

	print "#######################################################################################################"
	print "                              Extracting Data from files (Net_Response)  "
	print "#######################################################################################################"

	response_nature = 'stim'  # or stim+I_Stim_I
	results_dict = dict()

	############################################################
	# Read results dictionary (if not possible, re-construct the sequence from the file names and assume common
	# parameters)

	print 'Attempting to read data...'

	with open(path+res_file_name, 'r') as fp:

		try:
			results_dict = pickle.load(fp)
		except (EOFError, ValueError):
			print '************************************************'
			print 'Error on File: {0}'.format(res_file_name)
			print 'Re-building sequence from filenames...'

			# Without being able to extract the sequence from stored data, we need to extract it form the filenames:
			Seq = []
			for t in range(len(glob.glob('{0}*'.format(path+'Raw_Data/')))):
				tmp = '{0}{1}_Response_{2}_*'.format(path+'Raw_Data/', raw_files, str(t))
				name = glob.glob(tmp)[0]
				seq_symbol = name[-1]
				Seq.append(seq_symbol)

	if not len(results_dict.items()):
		print '************************************************'
		print 'Error: empty results dictionary '
		print 'Re-building assuming standard parameters'

		# If results_dict is empty re-build it assuming common parameters:
		results_dict['Results'] = dict()
		results_dict['Input_Pars'] = dict()
		results_dict['Net_Pars'] = dict()
		results_dict['Readout_Pars'] = dict()
		results_dict['Options'] = dict()
		results_dict['Results']['Full_Seq'] = np.array(Seq)
		results_dict['Results']['time_data'] = np.arange(0., 3300. * 300., 1.)
		results_dict['Input_Pars']['nDiscardSeq'] = 300.0
		results_dict['Input_Pars']['stim_dur'] = 200.0
		results_dict['Input_Pars']['I_stim_I'] = 100.0
		results_dict['Input_Pars']['resolution'] = 1.0
		results_dict['Input_Pars']['nStrings'] = 1100.0
		results_dict['Input_Pars']['alphabet'] = ['A', 'B', 'C']
		results_dict['Net_Pars']['nE'] = 8000.0
		results_dict['Options']['Topology'] = 'None'
		results_dict['Readout_Pars'] = {'Readout_Task': 'stimulus_classification',
		                                'filter_tau': 30.0,
		                                'learn_rule': 'ridge',
		                                't_0': 200.0,
		                                'train_fraction': 0.8}

	####################################################################################################################
	# Set the time units based on parameters and set full sequence..
	total_length = len(results_dict['Input_Pars']['alphabet']) * results_dict['Input_Pars']['nStrings']
	t_unit = results_dict['Input_Pars']['stim_dur'] + results_dict['Input_Pars']['I_stim_I']
	time_0 = results_dict['Sim_Pars']['PlasticityT'] + results_dict['Sim_Pars']['TransientT'] - 1
	SimT = total_length * t_unit + results_dict['Sim_Pars']['PlasticityT']
	times = np.arange(time_0, SimT, t_unit)

	Seq = results_dict['Results']['Full_Seq']
	if len(Seq) == results_dict['Input_Pars']['nStrings'] * len(results_dict['Input_Pars']['alphabet']):
		Seq = Seq[int(results_dict['Input_Pars']['nDiscardSeq']):]
	elif len(Seq) < results_dict['Input_Pars']['nStrings'] * len(results_dict['Input_Pars']['alphabet']):
		print 'Irregular Sequence: {0} Extra initial symbols'.format(str(results_dict['Input_Pars']['nStrings'] * len(
			results_dict['Input_Pars']['alphabet']) - len(Seq)))

	Seq = Seq[:int(total_length - results_dict['Input_Pars']['nDiscardSeq'])]
	nSyms = np.unique(Seq)

	####################################################################################################################
	# Iterate through the raw_data files, read the spiking data, generate SpikeList objects and store them in the
	# Net_response list
	# This list is the same size as the number of symbols presented, with sub-lists containing all the responses to
	# each symbol

	Net_response = [[] for _ in range(len(nSyms))]

	for idx, t in enumerate(Seq):

		fname = '{0}Raw_Data/{1}_Response_{2}_to_{3}'.format(path, raw_files, str(idx), t)

		if is_not_empty_file(fname):

			Data = numpy.loadtxt(fname)

			if response_nature == 'stim':
				time_interval = [times[idx], times[idx] + results_dict['Input_Pars']['stim_dur']]
			else:
				time_interval = [times[idx], times[idx] + t_unit]

			SpkTimes = Data[:, 1]
			NeuronId = Data[:, 0]

			tmp = [(NeuronId[n], SpkTimes[n]) for n in range(len(SpkTimes))]
			Spk_list = signals.SpikeList(tmp, numpy.unique(NeuronId).tolist(), t_start=time_interval[0],
			                             t_stop=time_interval[1])

			Net_response[np.where(nSyms == t)[0][0]].append(Spk_list)
		else:
			print 'Error reading file {0}'.format(fname)

	####################################################################################################################
	# Extract state matrix...
	# Possible methods: filter, counts, rates
	state_matrix = extract_state_matrix(Net_response, Seq, results_dict, method='filter', t_0=200., t_1=30.,
	                                    display=False)

	####################################################################################################################
	# Store Data:

	fname = path + raw_files + '_PROCESSED_DATA'
	Data = dict()
	Data['state_matrix'] = state_matrix
	Data['Seq'] = Seq
	Data['pars_dict'] = results_dict

	with open(fname, 'w') as fp:
		pickle.dump(Data, fp)


########################################################################################################################
def analyse_processed_data(path, pr_file_name, metrics=['classification']):
	"""
	Reads the file generated by process_rawdata() and uses it to compute some important metrics...
	"""

	print "#######################################################################################################"
	print "                                 Analysing Data    "
	print "#######################################################################################################"

	print "Analysing: {0}".format(pr_file_name)

	####################################################################################################################
	with open(path + pr_file_name, 'r') as fp:
		data = pickle.load(fp)

	state_matrix = data['state_matrix']
	Seq = data['Seq']
	symbols = np.unique(Seq)
	results_dict = data['pars_dict']
	data2 = dict()
	data2['Results'] = dict()

	if 'classification' in metrics:
		################################################################################################################
		# Readout classification performance:
		################################################################################################################
		# Divide state matrix into train and test sets:
		state_train, state_test, target_train, target_test = set_state_and_target(symbols, Seq, results_dict, state_matrix)

		results_dict['Readout_Pars']['learn_rule'] = 'ridge'

		w_out, fit_obj = train_readout(results_dict, state_train, target_train, display=True)
		if np.shape(w_out)[0]:
			norm_wOut = measure_readout_stability(w_out)
		else:
			norm_wOut = np.nan

		readout_out, fit_obj = test_readout(w_out, fit_obj, state_test, results_dict)

		print "|W_out| = {0}".format(norm_wOut)

		Perf = measure_readout_performance(readout_out, target_test)

		print "Performance = {0}".format(Perf['mPerf'])

		data2['Results']['CL_Performance'] = Perf
		data2['Results']['CL_Readout_output'] = readout_out
		data2['Results']['CL_norm_wOut'] = norm_wOut

	if 'MI' in metrics:
		################################################################################################################
		# Compute Mutual Information (based on global spike counts)
		################################################################################################################
		I1, I2 = compute_information_explicitly(state_matrix, Seq, results_dict, n_bins=100)
		I1_1, I2_1 = compute_information_explicitly(state_matrix, Seq, results_dict, n_bins=10)

		print "Mutual Information (100 bins): {0}".format(str(np.mean(I2)))
		print "Mutual Information (10 bins): {0}".format(str(np.mean(I2_1)))

		#state_mat_2 = extract_state_matrix(Net_response, Seq, results_dict, method='counts', t_0=200., t_1=30.,
			# display=True)

		#I1_2, I2_2 = compute_information_explicitly(state_mat_2, Seq, results_dict, n_bins=100)
		#I1_3, I2_3 = compute_information_explicitly(state_mat_2, Seq, results_dict, n_bins=10)

		#print "Mutual Information (100 bins, counts): {0}".format(str(np.mean(I2_2)))
		#print "Mutual Information (10 bins, counts): {0}".format(str(np.mean(I2_3)))

		data2['Results']['I1'] = I1   # ...

	if 'n-back' in metrics:
		################################################################################################################
		# Compute n-back memory
		###############################################################################################################
		results_dict['Readout_Pars']['Readout_Task'] = 'n-back_memory'

		Perf = []
		norm_wOut = []

		n_b = [1., 2., 3., 4., 5., 6., 7., 8., 9., 10.]
		print "n-back Pattern Memory: "
		for n_back in n_b:

			print "n = {0}".format(str(n_back))
			results_dict['Readout_Pars']['n_back'] = n_back

			# Divide state matrix into train and test sets:
			state_train, state_test, target_train, target_test = set_state_and_target(symbols, Seq, results_dict, state_matrix)
			w_out, fit_obj = train_readout(results_dict, state_train, target_train, display=True)

			if np.shape(w_out)[0]:
				norm_wOut.append(measure_readout_stability(w_out))
			else:
				norm_wOut.append(np.nan)

			readout_out, fit_obj = test_readout(w_out, fit_obj, state_test, results_dict)

			Perf.append(measure_readout_performance(readout_out, target_test))

			print "Performance = {0}".format(Perf[int(n_back-1)]['mPerf'])

		data2['Results']['MEM_Performance'] = Perf
		data2['Results']['MEM_norm_wOut'] = norm_wOut

	return data2