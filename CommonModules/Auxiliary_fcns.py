__author__ = 'neuro'
########################################################################################################################
#                                              AUXILIARY FUNCTIONS
########################################################################################################################
import nest
import numpy as np
import itertools
import nest.topology as tp


def set_topology(pars_dict, display=False):
	"""
	Set topology parameters according to options specified in parameters...
	"""
	##########################################################
	# Extract Parameters:
	##########################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1
	Topology_dict = dict()
	##########################################################
	# No topological features
	##########################################################
	if Topology == 'None':
		E_neurons, I_neurons = set_network(pars_dict)

		Topology_dict['E_neurons'] = E_neurons
		Topology_dict['I_neurons'] = I_neurons
		input_neuron_ids = set_input_neurons(pars_dict, E_neurons, I_neurons)

	else:
		######################################################
		## Set Neuron Models
		######################################################
		Neuron_Pars_Dict = {'V_m': E_L,
		                    'E_L': E_L,
		                    'C_m': C_m,
		                    't_ref': tau_ref,
		                    'V_th': V_th,
		                    'V_reset': V_reset,
		                    'E_ex': E_ex,
		                    'E_in': E_in,
		                    'g_L': g_leak,
		                    'tau_syn_ex': tau_E,
		                    'tau_syn_in': tau_I,
		                    'I_e': I_b,
		                    'tau_minus': tau_minus}

		nest.SetDefaults(Neuron_model, Neuron_Pars_Dict)

		######################################################
		# Set Layer dictionaries:
		######################################################
		common_layer_props = {'center': [np.round(np.sqrt(N)) / 2.,
		                                 np.round(np.sqrt(N)) / 2.],
		                      'extent': [np.round(np.sqrt(N)), np.round(np.sqrt(N))],
		                      'edge_wrap': True,
		                      'elements': Neuron_model}

		pos = set_positions(pars_dict)

		E_layer_dict = common_layer_props.copy()
		E_layer_dict.update({'positions': pos[:int(nE)]})
		I_layer_dict = common_layer_props.copy()
		I_layer_dict.update({'positions': pos[int(nE):]})

		E_layer = tp.CreateLayer(E_layer_dict)
		I_layer = tp.CreateLayer(I_layer_dict)

		Topology_dict['E_layer'] = E_layer
		Topology_dict['I_layer'] = I_layer

		if Input_Population['Target'] == 'all':
			# TODO: subdivide into random and positional ???
			if Input_Population['Nature'] == 'E':
				idxs = range(int(nE)); np.random.shuffle(idxs)
				idxs = idxs[:int(nIU)]
				input_neuron_ids = np.array(nest.GetLeaves(E_layer)[0])[idxs].tolist()
			elif Input_Population['Nature'] == 'I':
				idxs = range(int(nI)); np.random.shuffle(idxs)
				idxs = idxs[:int(nIU)]
				input_neuron_ids = np.array(nest.GetLeaves(I_layer)[0])[idxs].tolist()
			elif Input_Population['Nature'] == 'EI':
				idxs = range(int(N)); np.random.shuffle(idxs)
				idxs = idxs[:int(nIU)]
				input_neuron_ids = np.array(list(itertools.chain(*[nest.GetLeaves(E_layer)[0], nest.GetLeaves(I_layer)[
					0]])))[idxs].tolist()

		elif Input_Population['Target'] == 'symbol-specific':
			if 'random' in Input_Population['Extra']:
				assert 'positional' not in Input_Population['Extra'], 'Symbol-specific populations can be either ' \
				                                                      'random or positional, not both...'
				if Input_Population['Nature'] == 'E':
					idxs = range(int(nE)); np.random.shuffle(idxs)
					target = nest.GetLeaves(E_layer)[0]
				elif Input_Population['Nature'] == 'I':
					idxs = range(int(nI)); np.random.shuffle(idxs)
					target = nest.GetLeaves(I_layer)[0]
				elif Input_Population['Nature'] == 'EI':
					idxs = range(int(N)); np.random.shuffle(idxs)
					target = list(itertools.chain(*[nest.GetLeaves(E_layer)[0], nest.GetLeaves(I_layer)[0]]))

				inps = np.arange(0, (len(alphabet)+1)*nIU, nIU)
				input_neuron_ids = []
				for n in range(len(alphabet)):
					idx = idxs[int(inps[n]):int(inps[n+1])]
					input_neuron_ids.append(np.array(target)[idx].tolist())

			elif 'positional' in Input_Population['Extra']:
				assert 'random' not in Input_Population['Extra'], 'Symbol-specific populations can be either ' \
				                                                      'random or positional, not both...'
				if Input_Population['Nature'] == 'E':
					inp_neuron_ids = nest.GetLeaves(E_layer)[0]
				elif Input_Population['Nature'] == 'I':
					inp_neuron_ids = nest.GetLeaves(I_layer)[0]
				elif Input_Population['Nature'] == 'EI':
					inp_neuron_ids = list(itertools.chain(*[nest.GetLeaves(E_layer)[0], nest.GetLeaves(I_layer)[0]]))

				inp_centers = pick_centers(inp_neuron_ids, len(alphabet), Options, Pos=None)
				Topology_dict['input_centers'] = inp_centers
				input_neuron_ids = []
				for n in range(len(alphabet)):
					dists = []
					for idx, nn in enumerate(inp_neuron_ids):
						dists.append(tp.Distance(inp_centers[n], [nn])[0])
					sorted_dist = np.argsort(dists)[:nIU]
					input_neuron_ids.append(np.array(inp_neuron_ids)[sorted_dist].tolist())
					for iii in input_neuron_ids[n]:
						inp_neuron_ids.remove(iii)

	Topology_dict['Input_neurons'] = input_neuron_ids

	if display:
		from CommonModules.Plotting_routines import plot_topology
		plot_topology(Topology_dict)

	return Topology_dict


########################################################################################################################
def set_positions(pars_dict):
	##############################################################################
	# Extract Parameters:
	#############################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1
	##############################################################################
	# No topological features
	##############################################################################
	if Topology == 'None':
		pos = []

	##############################################################################
	# Random positions
	##############################################################################
	else:
		if Topology == 'random':
			## Set Neuron Positions (randomly in [0., sqrt(N)]...)
			pos = (np.sqrt(N) * np.random.random_sample((int(N), 2))).tolist()

		elif Topology == 'lattice':
			## Set Neuron Positions (corresponding to integer points...)
			assert (np.sqrt(N) % 1) == 0., 'Please choose a value of N with an integer sqrt..'
			xs = np.linspace(0., np.sqrt(N) - 1, np.sqrt(N))
			pos = [[x, y] for y in xs for x in xs]
			np.random.shuffle(pos)

	return pos


########################################################################################################################
def set_network(pars_dict):
	"""
	Set neuron model parameters and creates the neuronal populations
	"""

	########################################################################
	# Extract Parameters
	########################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1

	assert Topology == 'None', 'set_network can only be used with no topological specifications, run set_topology ' \
	                           'instead'
	#-----------------------------------------------------------------------
	# Set neuron parameters
	#-----------------------------------------------------------------------
	Neuron_Pars_Dict = {'V_m': E_L,
	                    'E_L': E_L,
	                    'C_m': C_m,
	                    't_ref': tau_ref,
	                    'V_th': V_th,
	                    'V_reset': V_reset,
	                    'E_ex': E_ex,
	                    'E_in': E_in,
	                    'g_L': g_leak,
	                    'tau_syn_ex': tau_E,
	                    'tau_syn_in': tau_I,
	                    'I_e': I_b,
	                    'tau_minus': tau_minus}

	nest.SetDefaults(Neuron_model, Neuron_Pars_Dict)

	#-----------------------------------------------------------------------
	# Create neuronal populations
	#-----------------------------------------------------------------------
	E_neurons = nest.Create(Neuron_model, int(nE))
	I_neurons = nest.Create(Neuron_model, int(nI))

	return E_neurons, I_neurons


########################################################################################################################
def set_synapses(pars_dict):
	"""
	Set synapse parameters
	"""

	########################################################################
	# Extract Parameters
	########################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1

	if 'iSTDP' in Plasticity:
		iSTDP_dict = {'Wmax': wmax,
		              'Wmin': wmin,
		              'eta': eta,
		              'ro': ro,
		              'alpha': alpha_I,
		              'tau_iSTDP': tau_iSTDP,
		              'scale': g_bar_I}
		nest.CopyModel('inh_stdp_synapse', 'EI_Synapse')
		nest.SetDefaults('EI_Synapse', iSTDP_dict)
	else:
		nest.CopyModel('static_synapse', 'EI_Synapse')

	if 'eSTDP' in Plasticity:
		eSTDP_dict = {'tau_plus': tau_eSTDP,
		              'lambda': lamb,
		              'alpha': alpha_E,
		              'scale': g_bar_E,
		              'Wmax': W_MAX,
		              'mu_minus': 1.,
		              'mu_plus': 0.}
		nest.CopyModel('stdp_synapse', 'EE_Synapse')
		nest.SetDefaults('EE_Synapse', eSTDP_dict)
	else:
		nest.CopyModel('static_synapse', 'EE_Synapse')

	nest.CopyModel('static_synapse', 'IE_Synapse')
	nest.CopyModel('static_synapse', 'II_Synapse')
	nest.CopyModel('static_synapse', 'Ext_Synapse')

	weights = dict()
	weights['wEE'] = sig_E * np.random.randn(int(nE), int(pEE * nE)) + mu_E
	weights['wIE'] = sig_E * np.random.randn(int(nI), int(pIE * nE)) + mu_E
	weights['wEI'] = sig_I * np.random.randn(int(nE), int(pEI * nI)) + mu_I
	weights['wII'] = sig_I * np.random.randn(int(nI), int(pII * nI)) + mu_I

	return weights


########################################################################################################################
def set_input_neurons(pars_dict, E_neurons, I_neurons):
	"""
	Determines which neurons receive the input, according to the specified parameters..
	"""
	########################################################################
	# Extract Parameters
	########################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1

	if Input_Population['Nature'] == 'EI':
		input_population = list(itertools.chain(*[E_neurons, I_neurons]))
	elif Input_Population['Nature'] == 'E':
		input_population = E_neurons
	elif Input_Population['Nature'] == 'I':
		input_population = I_neurons

	if nIU != len(input_population):
		idxs = range(int(len(input_population)))
		np.random.shuffle(idxs)
		input_population = [input_population[nn] for nn in idxs[:int(nIU)]]

	return input_population


########################################################################################################################
def create_inputs(pars_dict, topology_dict, input_SpkTrains):
	"""
	Create input spike generators and connect them to the main network...
	"""
	for k, v in pars_dict['Input_Pars'].items():
		globals()[k] = v
	for k, v in pars_dict['Synapse_Pars'].items():
		globals()[k] = v
	for k, v in pars_dict['Sim_Pars'].items():
		globals()[k] = v

	mu = pars_dict['Options']['Extra_Pars']['mu_in']
	sig = pars_dict['Options']['Extra_Pars']['sig_in']

	if pars_dict['Options']['Input_signal'] == 'Frozen':
		Inputs = nest.Create('spike_generator', int(nInputTrains))
		Input_neurons = topology_dict['Input_neurons']

		assert 'all' in Input_Population['Target'], 'Target needs to be all!'
		wIN = sig * np.random.randn(int(nInputTrains), len(Input_neurons)) + mu

		for n in range(int(nInputTrains)):
			nest.DivergentConnect([Inputs[n]], Input_neurons, weight=list(wIN[n] * g_bar_E),
			                      delay=delays, model='Ext_Synapse')
			Spks = input_SpkTrains[n]
			nest.SetStatus([Inputs[n]], {'spike_times': np.round(Spks.spike_times, 1), 'start': TransientT})

	elif pars_dict['Options']['Input_signal'] == 'Rate':

		for n in range(len(input_SpkTrains)):
			Inputs = nest.Create('spike_generator', int(nInputTrains))
			Input_neurons = topology_dict['Input_neurons'][n]

			wIN = sig * np.random.randn(int(nInputTrains), len(Input_neurons)) + mu

			for nn in range(int(nInputTrains)):
				nest.DivergentConnect([Inputs[nn]], Input_neurons, weight=list(wIN[nn] * g_bar_E),
				                      delay=delays, model='Ext_Synapse')
				Spks = input_SpkTrains[n][nn]
				nest.SetStatus([Inputs[nn]], {'spike_times': np.round(Spks.spike_times, 1), 'start': PlasticityT})


########################################################################################################################
def set_standard_devices(pars_dict):
	"""
	Creates the standard devices for the network simulation and sets their properties
	(go get the main code 'cleaner')
	"""

	########################################################################
	# Extract Parameters
	########################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1

	#-----------------------------------------------------------------------
	# Create and Set Devices
	#-----------------------------------------------------------------------
	SpkDetector = nest.Create('spike_detector')
	nest.SetStatus(SpkDetector, {'start': PlasticityT+TransientT, 'record_to': ['file'],
	                             'to_file': True, 'to_memory': False,
	                             'label': 'SpkDetector'})

	#nest.CopyModel('multimeter', 'singleNeuron_multimeter')
	nest.CopyModel('multimeter', 'population_multimeter')
	#nest.SetDefaults('singleNeuron_multimeter', {'record_from': ['g_ex', 'g_in', 'V_m'],
	#                                             'record_to': ['file'], 'to_file': True,
	#                                             'to_memory': False, 'interval': dt,
	#                                             'label': 'SingleNeuron_multimeter',
	#                                             'withtime': True, 'start': TransientT})
	nest.SetDefaults('population_multimeter', {'record_from': ['g_ex', 'g_in', 'V_m'],
	                                           'record_to': ['accumulator'],
	                                           'start': PlasticityT+TransientT})
	#singleNeuron_multimeter = nest.Create('singleNeuron_multimeter')
	population_multimeter = nest.Create('population_multimeter')

	PG = nest.Create('poisson_generator')
	nest.SetStatus(PG, {'rate': Ext_rate * (pEE * nE)})

	return SpkDetector, population_multimeter, PG


########################################################################################################################
def connect_network(weights, pars_dict, topology_dict):
	"""
	Connect neuronal populations
	"""
	########################################################################
	# Extract Parameters
	########################################################################
	for k, v in pars_dict.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			globals()[k1] = v1

	for k, v in weights.items():
		globals()[k] = v

	for k, v in topology_dict.items():
		globals()[k] = v

	weights['nE'] = nE
	weights['nI'] = nI
	weights['pEE'] = pEE
	weights['pIE'] = pIE
	weights['pEI'] = pEI
	weights['pII'] = pII
	weights['g_bar_E'] = g_bar_E
	weights['g_bar_I'] = g_bar_I
	#-----------------------------------------------------------------------
	# Connect the neuronal populations
	#-----------------------------------------------------------------------
	if Topology == 'None':
		E_neurons = topology_dict['E_neurons']    # to circumvent the referenced before assignment problem
		I_neurons = topology_dict['I_neurons']

		connect_standard_random(weights, E_neurons, I_neurons, delays, Plasticity, conn_type=['EE', 'EI', 'IE', 'II'])
		print 'No topology, random connectivity...'
		connected = ['EE', 'EI', 'IE', 'II']
		print 'Connections established: EE, EI, IE, II'

	else:
		if not ('explicit_assemblies' in Connectivity) and not ('local_inhibition' in Connectivity) and not (
			'explicit_paths' in Connectivity):
			print 'No Extra Connectivity properties (explicit_assemblies / local_inhibition)...'
			E_neurons = nest.GetLeaves(E_layer)[0]
			I_neurons = nest.GetLeaves(I_layer)[0]
			topology_dict['E_neurons'] = E_neurons
			topology_dict['I_neurons'] = I_neurons

			if 'random' in Connectivity:

				connect_standard_random(weights, E_neurons, I_neurons, delays, Plasticity, conn_type=['EE', 'EI', 'IE', 'II'])

				print 'Random connectivity...'
				connected = ['EE', 'EI', 'IE', 'II']
				print 'Connections established: EE, EI, IE, II'

			elif 'distance_dependent' in Connectivity:

				topology_dict = connect_global_distance_dependent(Extra_Pars, Plasticity, topology_dict, weights)

				print 'Distance-dependent connectivity...'
				for p in Extra_Pars['target']:
					print 'Connections established: {0}'.format(p)

				connected = Extra_Pars['target']

		else:
			print 'Extra Connectivity properties: '

			if 'local_inhibition' in Connectivity:
				from Analysis_fcns import extract_data_fromfile

				print 'Setting up local inhibition...'
				ratio = Extra_Pars['I_ratio']
				in_neurons = np.reshape(np.array(Input_neurons), np.array(Input_neurons).shape[0]*np.array(Input_neurons)
					.shape[1])

				# pick the local and global I neurons (depending on the input neurons positions) - remember you have the
				# input centers stored in topology_dict
				nI_local = ratio * nI
				I_neurons = nest.GetLeaves(I_layer)[0]

				if Input_Population['Nature'] == 'I':
					if nI_local < len(in_neurons):
						np.random.shuffle(in_neurons)
						I_local = in_neurons[:nI_local].tolist()
					else:
						I_local = in_neurons.tolist()
						remaining_idxs = [n for n in I_neurons if n not in I_local]
						# append more random neurons
						np.random.shuffle(remaining_idxs)
						for nnn in remaining_idxs[:int(nI_local - len(I_local))]:
							I_local.append(nnn)

				elif Input_Population['Nature'] == 'EI':
					I_local = []
					for nn in in_neurons.tolist():
						if nn in I_neurons:
							I_local.append(nn)
					if len(I_local) > nI_local:
						I_local = I_local[:int(nI_local)]
					elif len(I_local) < nI_local:
						remaining_idxs = [n for n in I_neurons if n not in I_local]
						rem_I_local = nI_local - len(I_local)

						if 'positional' in Input_Population['Extra']:
							for n in input_centers:
								I_dist = []
								for nn in remaining_idxs:
									I_dist.append(tp.Distance(n, [nn])[0])
								ids = np.argsort(I_dist)
								for nnn in ids[:int(rem_I_local/len(input_centers))].tolist():
									I_local.append(remaining_idxs[nnn])
							I_local = np.reshape(np.array(I_local), np.array(I_local).shape[0]).tolist()
							if len(I_local) < nI_local:
								remaining_idxs = [n for n in I_neurons if n not in I_local]
								np.random.shuffle(remaining_idxs)
								for nnn in remaining_idxs[:int(nI_local - len(I_local))]:
									I_local.append(nnn)
						else:
							remaining_idxs = [n for n in I_neurons if n not in I_local]
							np.random.shuffle(remaining_idxs)
							for nnn in remaining_idxs[:int(nI_local - len(I_local))]:
								I_local.append(nnn)

				elif Input_Population['Nature'] == 'E':
					I_local = []
					nI_local_per_assembly = np.round(nI_local / len(Input_neurons))

					if 'positional' in Input_Population['Extra']:
						for n in input_centers:
							I_dist = []
							for nn in I_neurons:
								I_dist.append(tp.Distance(n, [nn])[0])
							ids = np.argsort(I_dist)
							I_local.append(ids[:int(nI_local_per_assembly)].tolist())
						I_local = np.reshape(np.array(I_local), np.array(I_local).shape[0]*np.array(I_local).shape[1])\
							.tolist()
					else:
						remaining_idxs = I_neurons
						# append more random neurons
						np.random.shuffle(remaining_idxs)
						I_local.append(remaining_idxs[:int(nI_local)][0])

				#####################################################
				tp.DumpLayerNodes([E_layer[0], I_layer[0]], 'Layers')
				nest.ResetKernel()

				# Reset neuron and synapse parameters
				set_synapses(pars_dict)

				Neuron_Pars_Dict = {'V_m': E_L,
				                    'E_L': E_L,
				                    'C_m': C_m,
				                    't_ref': tau_ref,
				                    'V_th': V_th,
				                    'V_reset': V_reset,
				                    'E_ex': E_ex,
				                    'E_in': E_in,
				                    'g_L': g_leak,
				                    'tau_syn_ex': tau_E,
				                    'tau_syn_in': tau_I,
				                    'I_e': I_b,
				                    'tau_minus': tau_minus}

				nest.SetDefaults(Neuron_model, Neuron_Pars_Dict)

				# reset layers
				common_layer_props = {'center': [np.round(np.sqrt(N)) / 2.,
				                                 np.round(np.sqrt(N)) / 2.],
				                      'extent': [np.round(np.sqrt(N)), np.round(np.sqrt(N))],
				                      'edge_wrap': True}

				Data = extract_data_fromfile('Layers')
				Ids = Data[:, 0]
				xPos = Data[:, 1]
				yPos = Data[:, 2]
				E_Positions = []; I_local_Positions = []; I_global_Positions = []
				I_local_ids = []; I_global_ids = []
				for n in range(int(nE)):
					E_Positions.append([xPos[n], yPos[n]])

				for n in range(int(nI)):
					idx = nE+n
					if Ids[idx] in I_local:
						I_local_Positions.append([xPos[idx], yPos[idx]])
						I_local_ids.append(Ids[idx])
					else:
						I_global_Positions.append([xPos[idx], yPos[idx]])
						I_global_ids.append(Ids[idx])

				E_layer_dict = common_layer_props.copy()
				E_layer_dict.update({'positions': E_Positions, 'elements': Neuron_model})

				I_pos = np.copy(I_local_Positions).tolist()
				for n in I_global_Positions:
					I_pos.append(n)
				for idx1, n in enumerate(Input_neurons):
					for idx2, nn in enumerate(n):
						if nn in I_global_ids:
							Input_neurons[idx1][idx2] = nn+1

				topology_dict['Input_neurons'] = Input_neurons

				nest.CopyModel(Neuron_model, 'I_local')
				I_local_dict = common_layer_props.copy()
				I_global_dict = common_layer_props.copy()
				I_local_dict.update({'elements': 'I_local', 'positions': I_local_Positions})
				I_global_dict.update({'elements': Neuron_model, 'positions': I_global_Positions})

				E_lr = tp.CreateLayer(E_layer_dict)
				I_lr = tp.CreateLayer([I_local_dict, I_global_dict])
				topology_dict['E_layer'] = E_lr
				topology_dict['I_layer'] = I_lr

				if 'iSTDP_local' in Plasticity:
					print 'Dynamic local synapses'
					iSTDP_dict = {'Wmax': wmax,
					              'Wmin': wmin,
					              'eta': eta,
					              'ro': ro,
					              'alpha': alpha_I,
					              'tau_iSTDP': tau_iSTDP,
					              'scale': g_bar_I}
					nest.CopyModel('inh_stdp_synapse', 'EI_local_Synapse')
					nest.SetDefaults('EI_local_Synapse', iSTDP_dict)
					weight = 1.
				else:
					nest.CopyModel('static_synapse', 'EI_local_Synapse')
					weight = g_bar_I

				conn_dict = {'connection_type': 'divergent',
				             'mask': {'circular': {'radius': 20.}},
				             'kernel': {'gaussian': {'p_center': 1.0, 'sigma': Extra_Pars['sig_I']}},
				             'synapse_model': 'EI_local_Synapse',
				             'weights': weight} #{'gaussian': {'p_center': weight, 'sigma': 0.25 * weight}}}

				tp.ConnectLayers([I_lr[0]], E_lr, conn_dict)
				E_neurons = nest.GetLeaves(E_lr)[0]
				I_neurons_gl = nest.GetLeaves([I_lr[1]])[0]
				connect_standard_random(weights, E_neurons, I_neurons_gl, delays, Plasticity, conn_type=['EI'])
				I_neurons = itertools.chain(*[nest.GetLeaves([I_lr[0]])[0], nest.GetLeaves([I_lr[1]])[0]])
				connected = ['EI']; I_nrn = []
				for i in I_neurons:
					I_nrn.append(i)
				topology_dict['E_neurons'] = E_neurons
				topology_dict['I_neurons'] = I_nrn

				print 'Connections established: EI (local and global)'

			if 'explicit_assemblies' in Connectivity:
				assert Input_Population['Nature'] == 'E' or 'EI', 'Input assemblies must contain E neurons'

				print 'Embedding Specific cell assemblies...'

				# just connect normally, and then iterate through the input neurons, find connections among them and
				# change their weight
				if 'random' in Connectivity:
					gamma = Extra_Pars['gamma']

					E_neurons = nest.GetLeaves(E_layer)[0]
					I_neurons = nest.GetLeaves(I_layer)[0]

					connect_standard_random(weights, E_neurons, I_neurons, delays, Plasticity, conn_type=['EE'])

					for n, nn in enumerate(Input_neurons):
						aEE = nest.GetConnections(nn, nn)
						for nnn in aEE:
							nest.SetStatus([nnn], {'weight': gamma * nest.GetStatus([nnn], 'weight')[0]})

				elif 'distance_dependent' in Connectivity:
					ext_pars = Extra_Pars
					ext_pars['target'] = 'EE'
					topology_dict = connect_global_distance_dependent(ext_pars, Plasticity, topology_dict, weights)

					for n, nn in enumerate(Input_neurons):
						aEE = nest.GetConnections(nn, nn)
						for nnn in aEE:
							nest.SetStatus([nnn], {'weight': gamma * nest.GetStatus([nnn], 'weight')[0]})

				print 'Connections established: EE'
				connected = ['EE']

			###################################################################
			print 'Random connectivity in remaining populations...'

			pops = ['EE', 'EI', 'IE', 'II']
			remaining_pops = [n for n in pops if n not in connected]

			connect_standard_random(weights, E_neurons, I_neurons, delays, Plasticity, conn_type=remaining_pops)
			for p in remaining_pops:
				print 'Connections established: {0}'.format(p)

			if not 'E_neurons' in topology_dict:
				topology_dict['E_neurons'] = nest.GetLeaves(E_layer)[0]
			if not 'I_neurons' in topology_dict:
				topology_dict['I_neurons'] = nest.GetLeaves(I_layer)[0]

	return topology_dict


########################################################################################################################
def connect_global_distance_dependent(Extra_Pars, Plasticity, topology_dict, weights):

	for k, v in weights:
		globals()[k] = v

	E_layer = topology_dict['E_layer']
	I_layer = topology_dict['I_layer']

	common_conn_pars = {'connection_type': 'divergent',
	                    'mask': {'circular': {'radius': 50.}},
	                    'kernel': {Extra_Pars['profile']: {'p_center': 1.0, 'sigma': Extra_Pars['sigC']}}
						}

	if 'EE' in Extra_Pars['target']:
		if 'eSTDP' in Plasticity:
			weight = 1.
		else:
			weight = g_bar_E
		EE_conn_dict = common_conn_pars.copy()
		EE_conn_dict.update({'number_of_connections': int(pEE * nE),
		                     'synapse_model': 'EE_Synapse',
		                     'weights': weight}) #{'gaussian': {'p_center': weight, 'sigma': 0.25 * weight}}})
		topology_dict['EE_dict'] = EE_conn_dict
		tp.ConnectLayers(E_layer, E_layer, EE_conn_dict)
	if 'EI' in Extra_Pars['target']:
		if 'iSTDP' in Plasticity:
			weight = 1.
		else:
			weight = g_bar_I
		EI_conn_dict = common_conn_pars.copy()
		EI_conn_dict.update({'number_of_connections': int(pEI * nI),
		                     'synapse_model': 'EI_Synapse',
		                     'weights': {'gaussian': {'p_center': weight, 'sigma': 0.25 * weight}}})
		topology_dict['EI_dict'] = EI_conn_dict
		tp.ConnectLayers(I_layer, E_layer, EI_conn_dict)
	if 'IE' in Extra_Pars['target']:
		IE_conn_dict = common_conn_pars.copy()
		IE_conn_dict.update({'number_of_connections': int(pIE * nE),
		                     'synapse_model': 'IE_Synapse',
		                     'weights': {'gaussian': {'p_center': g_bar_E, 'sigma': 0.25 * weight}}})
		topology_dict['IE_dict'] = IE_conn_dict
		tp.ConnectLayers(E_layer, I_layer, IE_conn_dict)
	if 'II' in Extra_Pars['target']:
		II_conn_dict = common_conn_pars.copy()
		II_conn_dict.update({'number_of_connections': int(pII * nI),
		                     'synapse_model': 'II_Synapse',
		                     'weights': {'gaussian': {'p_center': g_bar_I, 'sigma': 0.25 * weight}}})

	return topology_dict


########################################################################################################################
def connect_standard_random(weights, E_neurons, I_neurons, delays, Plasticity, conn_type):

	for k, v in weights.items():
		globals()[k] = v

	for ind, target in enumerate(E_neurons):
		if 'EE' in conn_type:
			if 'eSTDP' in Plasticity:
				nest.RandomConvergentConnect(E_neurons, [target], int(nE * pEE), weight=list(wEE[ind]), delay=delays,
				                             model='EE_Synapse')
			else:
				nest.RandomConvergentConnect(E_neurons, [target], int(nE * pEE), weight=list(g_bar_E * wEE[ind]),
				                             delay=delays, model='EE_Synapse')
		if 'EI' in conn_type:
			if 'iSTDP' in Plasticity:
				nest.RandomConvergentConnect(I_neurons, [target], int(nI * pEI), weight=list(wEI[ind]), delay=delays,
				                             model='EI_Synapse')
			else:
				nest.RandomConvergentConnect(I_neurons, [target], int(nI * pEI), weight=list(g_bar_I * wEI[ind]),
				                             delay=delays, model='EI_Synapse')

	for ind, target in enumerate(I_neurons):
		if 'II' in conn_type:
			nest.RandomConvergentConnect(I_neurons, [target], int(nI * pII), weight=list(g_bar_I * wII[ind]), delay=delays,
			                             model='II_Synapse')
		if 'IE' in conn_type:
			nest.RandomConvergentConnect(E_neurons, [target], int(nE * pIE), weight=list(g_bar_E * wIE[ind]), delay=delays,
			                             model='IE_Synapse')


########################################################################################################################
def connect_standard_devices(E_neurons, I_neurons, g_bar_E, delays, PG, SpkDetector, population_multimeter):
	"""
	Connects devices...
	"""
	#-----------------------------------------------------------------------
	# Connect the devices
	#-----------------------------------------------------------------------
	nest.DivergentConnect(PG, list(itertools.chain(*[E_neurons, I_neurons])), weight=g_bar_E, delay=delays,
	                      model='Ext_Synapse')

	nest.ConvergentConnect(list(itertools.chain(*[E_neurons, I_neurons])), SpkDetector)

	#singleNeuron_idx = np.random.randint(min(E_neurons), max(E_neurons), 1)
	nest.DivergentConnect(population_multimeter, list(itertools.chain(*[E_neurons, I_neurons])))
	#nest.Connect(singleNeuron_multimeter, singleNeuron_idx)


#=======================================================================================================================
# Auxiliary sub-functions
#=======================================================================================================================
########################################################################################################################
def pick_centers(targets, n_centers, pars_dict_, Pos=None):
	"""
	Pick the center elements for the symbol-specific input populations
	based on the distance between the cluster centers.. The idea is to
	maximize the distance in input space between the symbol-specific
	neuronal assemblies
	"""
	import random
	# TODO: 'max_dist' when Positions are given instead of targets...

	for k, v in pars_dict_.items():
		globals()[k] = v

	if len(np.array(targets).shape) is not 1:
		targets = np.concatenate(targets)

	if not Pos:
		Positions = np.zeros((len(targets), 2))
		for idx, n in enumerate(targets):
			Positions[idx, :] = tp.GetPosition([n])[0]
		centers = []
	else:
		Positions = Pos
		center_pos = []

	if 'max_dist' in Input_Population['Extra']:

		center_1 = random.sample(targets, 1)
		center_pos_1 = tp.GetPosition(center_1)

		euc_dist = []
		for idx, n in enumerate(targets):
			euc_dist.append(tp.Distance(center_1, [n])[0])

			#euc_dist.append(np.sqrt((((center_pos_1[0]-n[0])**2)+(center_pos_1[1]-n[1])**2)))
		tmp = np.argsort(euc_dist)
		center_2 = [targets[tmp[-1]]]
		center_pos_2 = tp.GetPosition(center_2)
		dist_0 = euc_dist[tmp[-1]]
		centr_n = 2

		while centr_n < n_centers:
			for nn in range(centr_n):
				globals()['dist_{0}'.format(str(nn + 1))] = []

			diameter = []
			for n in targets:
				shape_diameter = dist_0
				for nn in range(centr_n):
					x = locals()['center_{0}'.format(str(nn + 1))]
					#y = locals()['center_{0}'.format(str(nn + 1))][1]

					globals()['dist_{0}'.format(str(nn+1))] = tp.Distance(x, [n])[0]   #np.sqrt(((x-n[0])**2) + ((y-n[
					# 1])**2))

					shape_diameter += globals()['dist_{0}'.format(str(nn+1))]
				diameter.append(shape_diameter)
			tmp = np.argsort(diameter)
			centr_n += 1
			locals()['center_{0}'.format(str(centr_n))] = [targets[tmp[-1]]]

		for nn in range(n_centers):
			if not Pos:
				#idx = np.where(Positions == locals()['center_pos_{0}'.format(str(nn+1))])[0][0]
				#centers.append(idx)
				centers.append(locals()['center_{0}'.format(str(nn+1))])
			else:
				center_pos.append(locals()['center_pos_{0}'.format(str(nn+1))].tolist())
	else:
		if not Pos:
			centers = random.sample(targets, n_centers)
		else:
			center_pos = random.sample(Positions, n_centers)

	if not Pos:
		return centers
	else:
		return center_pos


########################################################################################################################
def find_nearest_elements(tget_neurons, center_element, N):
	"""
	Returns the GIDs of the N elements in tget_neurons closest to the center_element:
		- tget_neurons should be a list of GIDs of the elements where to search
		- center_element is the GID of the center element
		- N is the cutoff number of elements to return
	"""

	Dists = np.array(tp.Distance([center_element], tget_neurons))
	idxs = Dists.argsort()[:N]

	neurons = [tget_neurons[n] for n in idxs]

	return neurons
