__author__ = 'neuro'

import numpy
import nest
import NeuroTools.signals_mine as signals
import itertools
import os
import operator

########################################################################################################################
def compute_spiking_stats(SpkDet, E_neurons, I_neurons, nPairs_Corr, time_interval, display=True, splitEI=False):
	"""
	Reads in and extracts the information contained in the spiking detectors,
	and computes the relevant statistics
	"""

	SpkData = extract_data_fromfile(nest.GetStatus(SpkDet)[0]['filenames'])

	if len(numpy.shape(SpkData)) != 2:
		SpkData = numpy.reshape(SpkData, (int(len(SpkData)/2), 2))

	SpkTimes = SpkData[:, 1]
	NeuronId = SpkData[:, 0]

	tmp = [(NeuronId[n], SpkTimes[n]) for n in range(len(SpkTimes))]
	Spk_list = signals.SpikeList(tmp, numpy.unique(NeuronId).tolist(), t_start=time_interval[0],
	                             t_stop=time_interval[1])

	Mean_fRate = Spk_list.mean_rates()
	Std_fRate = Spk_list.mean_rate_std()
	CV_ISI = Spk_list.cv_isi(True)
	CC_filtered = Spk_list.pairwise_cc_filtered(nPairs_Corr, None, 1., signals.bi_exponential_kernel, [50., 250.])
	CC_pearson = Spk_list.pairwise_pearson_corrcoeff(nPairs_Corr, time_bin=1.)
	CC = dict()
	CC['filtered'] = CC_filtered
	CC['pearson'] = CC_pearson

	if display:
		print '\n################################################################'
		print ' ACTIVITY REPORT:'
		print '################################################################'
		print 'Network Size:  ' + str(len(E_neurons) + len(I_neurons)) + '  (' + str(len(E_neurons)) + ' Exc. + ' + str(
			len(I_neurons)) + ' Inh.)'
		print 'Average Firing Rate: %.2f Hz' % numpy.mean(Mean_fRate)
		print 'Mean CV-ISI: %.2f' % numpy.mean(CV_ISI)
		print 'Spike CC: %.2f' % numpy.mean(CC['pearson'])

	if splitEI:
		tmp = [(NeuronId[n], SpkTimes[n]) for n in range(len(SpkTimes)) if NeuronId[n] in E_neurons]
		E_Spk_list = signals.SpikeList(tmp, numpy.unique(E_neurons).tolist(), t_start=time_interval[0],
	                             t_stop=time_interval[1])
		tmp = [(NeuronId[n], SpkTimes[n]) for n in range(len(SpkTimes)) if NeuronId[n] in I_neurons]
		I_Spk_list = signals.SpikeList(tmp, numpy.unique(I_neurons).tolist(), t_start=time_interval[0],
		                               t_stop=time_interval[1])

		Mean_fRate_E = E_Spk_list.mean_rates()
		Mean_fRate_I = I_Spk_list.mean_rates()
		Std_fRate_E = E_Spk_list.mean_rate_std()
		Std_fRate_I = I_Spk_list.mean_rate_std()
		CV_ISI_E = E_Spk_list.cv_isi(True)
		CV_ISI_I = I_Spk_list.cv_isi(True)
		CC_E = E_Spk_list.pairwise_cc_filtered(nPairs_Corr, None, 1., signals.bi_exponential_kernel, [50., 250.])
		CC_I = I_Spk_list.pairwise_cc_filtered(nPairs_Corr, None, 1., signals.bi_exponential_kernel, [50., 250.])

		return Mean_fRate, Mean_fRate_E, Mean_fRate_I, Std_fRate, Std_fRate_E, Std_fRate_I, CV_ISI, CV_ISI_E, \
		       CV_ISI_I, CC, CC_E, CC_I

	else:
		return Mean_fRate, Std_fRate, CV_ISI, CC


########################################################################################################################
def compute_AIness(Pop_Rates, Pop_irreg, Pop_synch, N):
	"""
	Determine the level of AI-type activity based on the spiking statistics
	"""

	Set1 = numpy.where(Pop_Rates < 30.)
	Set2 = numpy.where(Pop_irreg > 0.7)
	Set3 = numpy.where(Pop_synch < 0.5)

	Analysis_Dict = dict()
	Analysis_Dict['Pop_Rates'] = Pop_Rates
	Analysis_Dict['Pop_irreg'] = Pop_irreg
	Analysis_Dict['Pop_synch'] = Pop_synch

	AI_score = (numpy.size(numpy.intersect1d(numpy.intersect1d(Set1[0], Set2[0]), Set3[0]))*100.)/N

	return AI_score, Analysis_Dict


########################################################################################################################
def compute_analogue_stats(multimeter, N):
	"""
	Reads-in and extracts the information contained in the multimeters
	and computes the relevant statistics
	(assumes multimeter has been set to accumulator mode)
	time assumes the record step is 1 ms
	"""
	Total_Vm = nest.GetStatus(multimeter)[0]['events']['V_m']/N
	Total_GE = nest.GetStatus(multimeter)[0]['events']['g_ex']/N
	Total_GI = nest.GetStatus(multimeter)[0]['events']['g_in']/N

	return numpy.mean(Total_GE), numpy.mean(Total_GI), numpy.mean(Total_Vm)


########################################################################################################################
def parse_input_specific_spikes(SpkDet, step_signal, time_data, pars_dict, display=True):
	"""
	Divide the spiking activity into chunks corresponding to the net response to each input
	pattern
		Output: List of nSymbols lists, each containing nTrials objects (NeuroTools SpikeList objects), i.e.,
		for each symbol, list the network response on each trial presentation
	"""
	TransientT = pars_dict['Sim_Pars']['TransientT']
	dur = pars_dict['Input_Pars']['stim_dur']

	if display:
		import progressbar as p
		import time

		print "\n \n"
		print "==================================================================="
		print "          Parsing Network Response: "
		print "-------------------------------------------------------------------"

	# Discard initial transient:
	discard_idx = numpy.where(time_data < TransientT)
	signal = step_signal[:, max(discard_idx[0])+1:]
	times = time_data[max(discard_idx[0])+1:]

	SpkData = extract_data_fromfile(nest.GetStatus(SpkDet)[0]['filenames'])

	SpkTimes = SpkData[:, 1]
	NeuronId = SpkData[:, 0]

	starts = numpy.zeros((numpy.shape(signal)))
	start_idx = []

	for i in range(len(signal)):
		tmp_a = numpy.where(signal[i])[0]
		tmp_c = numpy.where(numpy.diff(tmp_a) > 1.)

		starts[i][tmp_a[0]] = 1.
		for n in tmp_c[0]:
			starts[i][tmp_a[int(n) + 1]] = 1.
		start_idx.append(numpy.where(starts[i]))

	List_of_Lists = []
	if display:
		ctr = 0
		widgets = ['Generating SpikeList Objects: ', p.Percentage(), ' ', p.Bar(marker='>', left='[', right=']'),
		           ' ', p.ETA(), ' ']
		pbar = p.ProgressBar(widgets=widgets, maxval=len(signal)*pars_dict['Sim_Pars']['nTrials'])
		pbar.start()

	for n in range(len(signal)):
		Spk_list = []
		for nn in start_idx[n][0]:
			Start = times[nn]
			tmp_a = numpy.where(SpkTimes > Start)[0]
			tmp_b = numpy.where(SpkTimes < Start+dur)[0]
			tmp_c = numpy.intersect1d(tmp_a, tmp_b)

			Times = SpkTimes[tmp_c]
			Ids = NeuronId[tmp_c]
			tmp = [(Ids[nnn], Times[nnn]) for nnn in range(len(Times))]

			Spk_list.append(signals.SpikeList(tmp, numpy.unique(Ids).tolist(), t_start=Start, t_stop=Start+dur))
			if display:
				time.sleep(0.01)
				ctr += 1
				pbar.update(ctr)

		List_of_Lists.append(Spk_list)
	if display:
		pbar.finish()

	return List_of_Lists


########################################################################################################################
def compute_state_separation(net_response, pars_dict, input_spikes, filter_tau=30., display=True):
	"""
	Computes average distance between (filtered) network responses to each pattern/input
	Returns vector of average distance over time and mean at the end of the pattern
	"""

	nSymbols = len(net_response)
	Symbols = numpy.sort(numpy.unique(pars_dict['Input_Pars']['alphabet']))
	Comp = numpy.triu(numpy.ones((nSymbols, nSymbols)))
	#Comp[numpy.bool8(numpy.eye(len(Comp)))] = 0
	nComparisons = len(numpy.nonzero(Comp)[0])
	nTrials = len(net_response[0])
	N = pars_dict['Net_Pars']['N']
	nIU = pars_dict['Input_Pars']['nInputTrains']
	time_vec = numpy.arange(0.1, pars_dict['Input_Pars']['stim_dur']+0.1, 0.1)
	Mean_Dist = []; MeanFinal = []; Mean_inpDist = []; Mean_inpFinal = []

	if display:
		import pylab as pl
		import progressbar as p
		import time

		fig = pl.figure()
		ax1 = fig.add_subplot(221)
		ax2 = fig.add_subplot(222)
		ax3 = fig.add_subplot(223)
		ax4 = fig.add_subplot(224)
		print "==================================================================="
		print "     Computing State Separation ({0} Symbols, {1} Distances): ".format(str(nSymbols), str(nComparisons))
		print "-------------------------------------------------------------------"
		pbar = p.ProgressBar(widgets=[p.SimpleProgress(sep=' of '), ' ', p.ETA()], maxval=nComparisons).start()
		ctr = 0
	DistMat = numpy.zeros(numpy.shape(Comp))
	inpDistMat = numpy.zeros(numpy.shape(Comp))

	for n in range(nComparisons):
		idx1 = numpy.nonzero(Comp)[0][n]
		idx2 = numpy.nonzero(Comp)[1][n]

		tmp_a = net_response[idx1]
		tmp_b = net_response[idx2]
		Distances = []; D = []; inpD = []; inpDistances = []

		for nn in range(nTrials):
			tmp_a_single = tmp_a[nn].spikes_to_states_exp(N, 0.1, filter_tau)
			tmp_b_single = tmp_b[nn].spikes_to_states_exp(N, 0.1, filter_tau)
			distance = []
			for t in range(min(numpy.shape(tmp_a_single)[1], numpy.shape(tmp_b_single)[1])):
				distance.append(numpy.linalg.norm(tmp_a_single[:, t] - tmp_b_single[:, t]))
			D.append(distance[-1])
			Distances.append(distance)

			times_a = tmp_a[nn].time_parameters()
			times_b = tmp_b[nn].time_parameters()
			cp_inp = input_spikes.copy()
			input_a = cp_inp.time_slice(times_a[0], times_a[1])
			input_b = cp_inp.time_slice(times_b[0], times_b[1])
			inp_a_single = input_a.spikes_to_states_exp(nIU, 0.1, filter_tau)
			inp_b_single = input_b.spikes_to_states_exp(nIU, 0.1, filter_tau)

			distance = []
			for t in range(min(numpy.shape(inp_a_single)[1], numpy.shape(inp_b_single)[1])):
				distance.append(numpy.linalg.norm(inp_a_single[:, t] - inp_b_single[:, t]))
			inpD.append(distance[-1])
			inpDistances.append(distance)

		Mean_Dist.append(numpy.mean(Distances, 0))
		MeanFinal.append(numpy.mean(D))
		DistMat[idx1, idx2] = numpy.mean(D)
		Mean_inpDist.append(numpy.mean(inpDistances, 0))
		Mean_inpFinal.append(numpy.mean(inpD))
		inpDistMat[idx1, idx2] = numpy.mean(inpD)

		if display:
			ctr += 1
			ax1.plot(time_vec, Mean_Dist[n], linewidth=3, label=r'||x_{0} - x_{1}||'.format(Symbols[idx1],
			                                                                              Symbols[idx2]))
			ax3.plot(time_vec, Mean_inpDist[n], '--', linewidth=3, label=r'||{0} - {1}||'.format(Symbols[idx1],
			                                                                                   Symbols[idx2]))
			plt2 = ax2.imshow(DistMat, interpolation='nearest')
			plt4 = ax4.imshow(inpDistMat, interpolation='nearest')
			time.sleep(0.01)
			pbar.update(ctr)
	if display:
		ax1.set_title('State Distances')
		ax1.set_xlabel('Time from onset [ms]')
		ax1.set_ylabel('distance')
		ax1.legend(loc=4)
		ax3.set_title('Input Distances')
		ax3.set_xlabel('Time from onset [ms]')
		ax3.set_ylabel('distance')
		ax3.legend(loc=4)
		pl.colorbar(plt2, ax=ax2)
		pl.colorbar(plt4, ax=ax4)
		pl.show(block=False)
		pbar.finish()

	return Mean_Dist, MeanFinal, DistMat, Mean_inpDist, Mean_inpFinal, inpDistMat


########################################################################################################################
def compute_state_dist_simple(net_response, pars_dict, input_spikes, filter_tau=30., display=True):
	"""
	Computes average distance between (filtered) network responses to each pattern/input
	Returns mean at the end of the pattern
	"""

	nSymbols = len(net_response)
	Comp = numpy.triu(numpy.ones((nSymbols, nSymbols)))
	nComparisons = len(numpy.nonzero(Comp)[0])
	nTrials = len(net_response[0])
	N = pars_dict['Net_Pars']['nE']
	nIU = pars_dict['Input_Pars']['nInputTrains']
	MeanFinal = []
	Mean_inpFinal = []

	if display:
		import progressbar as p
		import time
		print "==================================================================="
		print "     Computing State Separation ({0} Symbols, {1} Distances): ".format(str(nSymbols), str(nComparisons))
		print "-------------------------------------------------------------------"
		pbar = p.ProgressBar(widgets=[p.SimpleProgress(sep=' of '), ' ', p.ETA()], maxval=nComparisons).start()
		ctr = 0
	DistMat = numpy.zeros(numpy.shape(Comp))
	inpDistMat = numpy.zeros(numpy.shape(Comp))

	for n in range(nComparisons):
		idx1 = numpy.nonzero(Comp)[0][n]
		idx2 = numpy.nonzero(Comp)[1][n]

		tmp_a = net_response[idx1]
		tmp_b = net_response[idx2]
		D = []
		inpD = []

		for nn in range(nTrials):
			time_int_a = tmp_a[nn].t_stop
			time_int_b = tmp_b[nn].t_stop

			resp_a = tmp_a[nn].time_slice(time_int_a - (filter_tau+10.), time_int_a)
			resp_b = tmp_b[nn].time_slice(time_int_b - (filter_tau+10.), time_int_b)

			resp_a = resp_a.id_slice(list(numpy.arange(2, 8002, 1)))
			resp_b = resp_b.id_slice(list(numpy.arange(2, 8002, 1)))

			tmp_a_single = resp_a.spikes_to_states_exp(N, 0.1, filter_tau, 2)
			tmp_b_single = resp_b.spikes_to_states_exp(N, 0.1, filter_tau, 2)

			t = min(numpy.shape(tmp_a_single)[1], numpy.shape(tmp_b_single)[1])
			D.append(numpy.linalg.norm(tmp_a_single[:, t-1] - tmp_b_single[:, t-1]))

			if len(input_spikes):
				times_a = tmp_a[nn].time_parameters()
				times_b = tmp_b[nn].time_parameters()
				cp_inp = input_spikes.copy()
				input_a = cp_inp.time_slice(times_a[0], times_a[1])
				input_b = cp_inp.time_slice(times_b[0], times_b[1])
				inp_a_single = input_a.spikes_to_states_exp(nIU, 0.1, filter_tau)
				inp_b_single = input_b.spikes_to_states_exp(nIU, 0.1, filter_tau)

				t = min(numpy.shape(inp_a_single)[1], numpy.shape(inp_b_single)[1])
				inpD.append(numpy.linalg.norm(inp_a_single[:, t] - inp_b_single[:, t]))

		MeanFinal.append(numpy.mean(D))
		DistMat[idx1, idx2] = numpy.mean(D)

		if len(input_spikes):
			Mean_inpFinal.append(numpy.mean(inpD))
			inpDistMat[idx1, idx2] = numpy.mean(inpD)

		if display:
			ctr += 1
			time.sleep(0.01)
			pbar.update(ctr)
	if display:
		pbar.finish()

	return MeanFinal, DistMat, Mean_inpFinal, inpDistMat


########################################################################################################################
def set_state_and_target(symbols, seq, pars_dict, state_matrix):
	"""
	Specifies the target output for the readout, depending on the specific task
	"""

	for k, v in pars_dict['Readout_Pars'].items():
		globals()[k] = v

	# generate global target
	target = numpy.zeros((len(symbols), len(seq)))
	for t, ii in enumerate(seq):
		target[numpy.where(symbols == ii)[0][0], t] = 1.

	if Readout_Task == 'stimulus_classification':

		train_len = len(seq) * train_fraction

	elif Readout_Task == 'n-back_memory':

		train_len = (len(seq)-n_back) * train_fraction

		# re-define state matrix (discard first n_back states)
		state_matrix = state_matrix[:, n_back:]

		# set target (discard last n_back symbols)
		target = target[:, :-n_back]

	elif Readout_Task == 'prediction':

		train_len = (len(seq) - n_for) * train_fraction

		# re-define state matrix (discard first n_back states)
		state_matrix = state_matrix[:, :-n_for]

		# set target (discard last n_back symbols)
		target = target[:, n_for:]

	# split state matrix
	state_train = state_matrix[:, 0:train_len]
	state_test = state_matrix[:, train_len:]

	# split target
	target_train = target[:, 0:train_len]
	target_test = target[:, train_len:]

	return state_train, state_test, target_train, target_test


########################################################################################################################
def train_readout(pars_dict, state_train, target_train, display=True):
	"""
	Train readout ...
	"""

	for k, v in pars_dict['Readout_Pars'].items():
		globals()[k] = v

	if learn_rule == 'pinv':
		wOut = numpy.dot(numpy.linalg.pinv(numpy.transpose(state_train)), numpy.transpose(target_train))
		fit_obj = []

	elif learn_rule == 'FORCE':
		if display:
			import progressbar as p
			import time

			ctr = 0
			print "==================================================================="
			print "                 Training Readout (FORCE algorithm) "
			print "-------------------------------------------------------------------"
			widgets = ['Processing: ', p.Percentage(), ' ', p.Bar(marker='>', left='[', right=']'),
		               ' ', p.ETA(), ' ']
			pbar = p.ProgressBar(widgets=widgets, maxval=numpy.shape(state_train)[1])
			pbar.start()

		P = numpy.eye(numpy.shape(state_train)[0])
		wOut = numpy.zeros((numpy.shape(state_train)[0], numpy.shape(target_train)[0]))
		fit_obj = []
		for t in range(numpy.shape(state_train)[1]):
			# Get the readout output
			z = numpy.dot(wOut.T, state_train[:, t])

			# Update inverse correlation matrix
			k = numpy.dot(P, state_train[:, t])
			rPr = numpy.dot(state_train[:, t].T, k)
			c = 1.0 / (1.0 + rPr)
			P -= numpy.dot(k, (k.T*c))

			# Update the error
			e = z - target_train[:, t]

			#  Update the output weights
			dW = - (numpy.dot(numpy.array([k]).T, numpy.array([e])) * c)
			wOut += dW
			if display:
				time.sleep(0.01)
				ctr += 1
				pbar.update(ctr)
		if display:
			pbar.finish()

	elif learn_rule == 'ridge':
		import sklearn.linear_model

		# Get w_out by ridge regression:
		# a) Obtain regression parameters by cross-validation
		alphas = numpy.array([0.0000001, 0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 1.])
		reg = sklearn.linear_model.RidgeCV(alphas, fit_intercept=False)
		# b) fit using the best alpha...
		reg.fit(state_train.T, target_train.T)
		# c) get the regression coefficients
		wOut = reg.coef_
		fit_obj = reg

	elif learn_rule == 'logistic':
		import sklearn.linear_model

		reg = sklearn.linear_model.LogisticRegression(penalty='l2', dual=False, fit_intercept=False)
		reg.fit(state_train.T, numpy.argmax(target_train, 0))
		wOut = reg.coef_
		fit_obj = reg

	elif learn_rule == 'perceptron':
		import sklearn.linear_model

		reg = sklearn.linear_model.Perceptron(fit_intercept=False)
		reg.fit(state_train.T, numpy.argmax(target_train, 0))
		wOut = reg.coef_
		fit_obj = reg

	elif learn_rule == 'svm-linear':
		import sklearn.svm

		reg = sklearn.svm.SVC(kernel='linear')
		reg.fit(state_train.T, numpy.argmax(target_train, 0))
		wOut = reg.coef_
		fit_obj = reg

	elif learn_rule == 'svm-rbf':
		wOut = []
		import sklearn.svm

		reg = sklearn.svm.SVC(kernel='rbf')
		print("Performing 5-fold CV for svm-rbf hyperparameters...")
		# use exponentially spaces C...
		C_range = 10.0 ** numpy.arange(-2, 9)
		# ... and gamma
		gamma_range = 10.0 ** numpy.arange(-5, 4)
		param_grid = dict(gamma=gamma_range, C=C_range)
		# pick only a subset of train dataset...
		target_test = target_train[:, :target_train.shape[1]/2]
		state_test = state_train[:, :target_train.shape[1]/2]
		cv = sklearn.cross_validation.StratifiedKFold(y=numpy.argmax(target_test, 0), n_folds=5)
		grid = sklearn.grid_search.GridSearchCV(reg, param_grid=param_grid, cv=cv)
		# use the test dataset (it's much smaller...)
		grid.fit(state_test.T, numpy.argmax(target_test, 0))
		print("The best classifier is: ", grid.best_estimator_)

		# use best parameters:
		reg = grid.best_estimator_
		reg.fit(state_train.T, numpy.argmax(target_train, 0))
		fit_obj = reg

	return wOut, fit_obj


########################################################################################################################
def test_readout(w_out, fit_obj, state_test, pars_dict):
	"""
	Apply the trained wOut to the test set...
	"""
	for k, v in pars_dict['Readout_Pars'].items():
		globals()[k] = v

	if (learn_rule == 'pinv') or (learn_rule == 'FORCE'):

		if numpy.shape(w_out)[1] == numpy.shape(state_test)[0]:
			out = numpy.dot(w_out, state_test)
		elif numpy.shape(w_out)[0] == numpy.shape(state_test)[0]:
			out = numpy.dot(numpy.transpose(w_out), state_test)
		fit_obj = []

	else:
		out = fit_obj.predict(state_test.T)

	return out, fit_obj


########################################################################################################################
def measure_readout_performance(out, target):
# TODO: NNL, COS, KL (probabilistic predictions)

	# Winner-takes-all (for class labels) and binary matrices:
	class_label = numpy.argmax(target, 0)
	binary_target = target
	if len(out.shape) > 1:
		binary_result = out
		if out.shape[1] == class_label.shape[0]:
			class_result = numpy.argmax(out, 0)
		elif out.shape[0] == class_label.shape[0]:
			class_result = numpy.argmax(out, 1)

		# Point-biserial CC (between out and binary_target):
		import scipy.stats.mstats
		ouT = out.transpose()
		pbserial_Perf = []
		for n in range(binary_target.shape[0]):
			pbserial_Perf.append(scipy.stats.mstats.pointbiserialr(binary_target[n], ouT[n]))

	else:
		class_result = out
		binary_result = numpy.zeros(target.shape)
		for i in range(target.shape[1]):
			binary_result[int(out[i]), i] = 1.
		pbserial_Perf = []

	import sklearn.metrics

	# Error
	if binary_result.shape == binary_target.shape:
		correct = binary_target.T
		estim = binary_result.T
	else:
		correct = binary_target.T
		estim = binary_result

	MSE = sklearn.metrics.mean_squared_error(correct, estim)
	MAE = sklearn.metrics.mean_absolute_error(correct, estim)

	# Classification Accuracy
	mPerf = sklearn.metrics.accuracy_score(class_label, class_result)
	Hamm_loss = sklearn.metrics.hamming_loss(class_label, class_result)

	# save results in dictionary
	results_dict = dict()
	results_dict['mPerf'] = mPerf
	results_dict['MSE'] = MSE
	results_dict['MAE'] = MAE
	results_dict['Hamm_loss'] = Hamm_loss
	results_dict['Pb_Perf'] = pbserial_Perf

	return results_dict


########################################################################################################################
def measure_readout_stability(w_out):
	"""
	Determine the stability of the readout solution...
	"""
	return numpy.linalg.norm(w_out)


########################################################################################################################
def compute_mutual_information(state_matrix, seq, pars_dict, method='random', n_bins=10):
	"""
	Computes stimulus-response relations using information-theoretic concepts.
	The dimensionality of the state matrix needs to be reduced dramatically to be computationally possible
	to compute the entropies. This can be done in 2 ways:
	- method == 'random' -> randomly pick 10 neurons, compute and repeat 100 times
	"""
	import pyentropy as ent
	symbols = numpy.sort(numpy.unique(seq))
	N = pars_dict['Net_Pars']['nE']

	stim_seq = numpy.zeros((len(symbols), len(seq)))
	for t, ii in enumerate(seq):
		stim_seq[numpy.where(symbols == ii)[0][0], t] = 1.

	stim_range = [0, 1]
	stim = stim_seq.astype(int)
	I = []

	if state_matrix.dtype == 'int':
		# reduce dimensionality to 10 neurons randomly picked
		if method == 'random':
			for i in range(100):
				neurons_idx = numpy.random.randint(0., N, 10)
				resp_a = state_matrix[neurons_idx, :]
				resp = numpy.zeros(resp_a.shape)
				for n in range(resp_a.shape[1]):
					resp[:, n] = ent.quantise_discrete(resp_a[:, n], 5)
				resp_range = numpy.unique(resp)

				system = ent.DiscreteSystem(resp.astype(int), (resp.shape[0], max(resp_range) + 1), stim, (len(symbols),
				                                                                                           len(stim_range)))
				system.calculate_entropies(method='qe', calc=['HX', 'HY', 'HXY'])
				I.append(system.I())

	elif state_matrix.dtype == 'float':

		class_seq = numpy.argmax(stim_seq, 0)

		min_st = numpy.min(state_matrix)
		max_st = numpy.max(state_matrix)
		bins = numpy.linspace(min_st, max_st, n_bins)
		disc_states = numpy.zeros(state_matrix.shape)

		for idx, t in enumerate(seq):
			disc_states[:, idx] = numpy.digitize(state_matrix[:, idx], bins)

		resp = disc_states.astype(int)
		resp_range = numpy.unique(resp)

		for i in range(100):
			neurons_idx = numpy.random.randint(0, N, 10)
			resp_a = resp[neurons_idx, :]

			system = ent.DiscreteSystem(resp_a, (10, 6), stim, (3, 2))
			system.calculate_entropies(method='qe', calc=['HX', 'HY', 'HXY'])
			I.append(system.I())

	#a = resp.shape[0]; b = max(resp_range)+1; c = len(symbols); d = len(stim_range)
	#system = ent.DiscreteSystem(resp, (8000, 11), stim, (3, 2))
	#system.calculate_entropies(method='qe', calc=['HX', 'HY', 'HXY'])
	#I.append(system.I())

	return I


########################################################################################################################
def compute_information_explicitly(state_matrix, seq, pars_dict, n_bins=10):
	"""
	Computes stimulus-response relations using information-theoretic concepts.
	"""

	symbols = numpy.sort(numpy.unique(seq))
	N = pars_dict['Net_Pars']['nE']

	stim_seq = numpy.zeros((len(symbols), len(seq)))
	for t, ii in enumerate(seq):
		stim_seq[numpy.where(symbols == ii)[0][0], t] = 1.
	class_seq = numpy.argmax(stim_seq, 0)
	for t in symbols:
		globals()['ctr_{0}'.format(t)] = 0
		globals()['States_{0}'.format(t)] = numpy.zeros((N, len(numpy.where(seq == t)[0])))

	min_st = numpy.min(state_matrix)
	max_st = numpy.max(state_matrix)
	bins = numpy.linspace(min_st, max_st, n_bins)
	disc_states = numpy.zeros(state_matrix.shape)

	for idx, t in enumerate(seq):
		disc_states[:, idx] = numpy.digitize(state_matrix[:, idx], bins)
		globals()['States_{0}'.format(t)][:, globals()['ctr_{0}'.format(t)]] = disc_states[:, idx]
		globals()['ctr_{0}'.format(t)] += 1

	resp_prob = []
	stim_prob = []
	joint_prob_table = numpy.zeros((len(symbols), len(bins)))
	ii1 = 0
	for ii in range(len(symbols)):
		stim_prob.append(len(numpy.where(class_seq == ii)[0]) / float(len(seq)))
	for ii in range(len(bins)):
		ii += 1
		resp_prob.append(len(numpy.where(disc_states == ii)[0])/float(disc_states.size))
		if resp_prob[-1] == 0.:
			resp_prob[-1] = numpy.spacing(0)
	for ii in range(len(symbols)):
		for iii in range(len(bins)):
			ii1 = iii+1
			joint_prob_table[ii, iii] = len(numpy.where(globals()['States_{0}'.format(symbols[ii])] == ii1)[
				0])/float(globals()['States_{0}'.format(symbols[ii])].size)
			if joint_prob_table[ii, iii] == 0.:
				joint_prob_table[ii, iii] = numpy.spacing(0)

	tmp2 = []
	for i in range(len(symbols)):
		tmp1 = []
		for ii in range(len(bins)):
			tmp1.append(joint_prob_table[i, ii] * numpy.log2(joint_prob_table[i, ii] / (stim_prob[i]*resp_prob[
				ii])+numpy.finfo(float).eps))
		tmp2.append(numpy.sum(tmp1))
	I1 = numpy.sum(tmp2)

	tmp1 = []
	for i in range(len(bins)):
		tmp1.append(resp_prob[i] * numpy.log2(resp_prob[i]))
	resp_ent = -numpy.sum(tmp1)
	tmp2 = []
	for i in range(len(symbols)):
		tmp3 = []
		for ii in range(len(bins)):
			tmp3.append(joint_prob_table[i, ii] * numpy.log2(joint_prob_table[i, ii]))
		tmp2.append(stim_prob[i]*numpy.sum(tmp3))
	noise_entropy = - numpy.sum(tmp2)

	I2 = resp_ent - noise_entropy

	return I1, I2


########################################################################################################################
def compute_response_variability(net_responses, pars_dict, seq):
	"""
	Computes trial-to-trial variability...
	"""
	'''
	symbols = numpy.unique(numpy.sort(seq))
	for k in symbols:
		globals()['states_{0}'.format(k)] = []
		globals()['HD_{0}'.format(k)] = []
	state_matrix_1 = extract_state_matrix(net_responses, seq, pars_dict, method='binary', t_0=200., t_1=30.,
	                                      display=True))

	for n, t in enumerate(seq):
		globals()['states_{0}'.format(t)].append(state_matrix_1[:, n])

	for k, t in enumerate(symbols):
		for n in range(len(globals()['states_{0}'.format(t)])-1):
			globals()['HD_{0}'.format(t)].append(hamming_dist(globals()['states_{0}'.format(t)][n],
			                                                  globals()['states_{0}'.format(t)][n+1]))
	'''


########################################################################################################################
def find_cell_assemblies(net_response, seq, pars_dict, display=True):
	"""
	Using the method is Maass paper...
	"""
	# 1 - compute peri-event time histograms (PETH) of all neurons in response to each
	# pattern
	symbols = numpy.sort(numpy.unique(seq))
	for n in symbols:
		globals()['ctr_{0}'.format(str(n))] = 0

	N = pars_dict['Net_Pars']['nE']

	time_vec = numpy.arange(0.1, pars_dict['Input_Pars']['stim_dur'] + 0.1, 0.1)

	if not pars_dict['Options']['Topology'] == 'None':
		NeuronIds = range(2, int(N) + 2)
	else:
		NeuronIds = range(1, int(N) + 1)

	if display:
		import progressbar as p
		import time

		ctr = 0
		print "==================================================================="
		print "           Looking for cell assemblies "
		print "-------------------------------------------------------------------"
		widgets = ['Processing: ', p.Percentage(), ' ', p.Bar(marker='>', left='[', right=']'),
		           ' ', p.ETA(), ' ']
		pbar = p.ProgressBar(widgets=widgets, maxval=len(seq))
		pbar.start()

	for idx, t in enumerate(seq):
		response = net_response[numpy.where(symbols == t)[0][0]][globals()['ctr_{0}'.format(str(t))]]
		globals()['ctr_{0}'.format(str(t))] += 1

		response = response.id_slice(NeuronIds)
		rates = response.firing_rate(1.)

		r = numpy.zeros_like(rates).astype(float)
		for n in range(rates.shape[0]):
			tmp = smooth(rates[n, :], window_len=40, window='hamming')
			r[n, :] = tmp

		numer = numpy.zeros(N)
		denom = numpy.zeros(N)
		center = numpy.zeros(N)
		for n in range(rates.shape[0]):
			for tt in range(200):
				tmp_val = r[n, tt] * numpy.exp((tt/200.) * 2*numpy.pi*1j)
			numer[n] = numpy.sum(tmp_val)
			denom[n] = numpy.sum(r[n, :])

			center[n] = (200./(2.*numpy.pi)) * numpy.angle(numer[n]/denom[n])

		if display:
			time.sleep(0.01)
			ctr += 1
			pbar.update(ctr)
	if display:
		pbar.finish()


########################################################################################################################
def compute_inout_correlations(net_response, seq, inp_spks, tp_dict, bin_w=1., display=True):
	"""
	Compute input-output correlations to determine the 'impact' of each signal on the output spike pattern
	(total amount of variance of the output spike pattern that can be explained by each input signal)
	"""
	from sets import Set

	symbols = numpy.sort(numpy.unique(seq))
	sy = symbols.copy().tolist()
	sy.append('Bg')
	Correlations = dict()
	for ii, n in enumerate(symbols):
		for nn in sy:
			Correlations['corr_stim{0}_pop{1}'.format(str(ii), str(nn))] = []
		globals()['ctr_{0}'.format(str(n))] = 0

	input_neurons = tp_dict['Input_neurons']
	E_neurons = tp_dict['E_neurons']
	s1 = Set(list(itertools.chain.from_iterable(input_neurons)))
	s2 = Set(E_neurons)
	s2.difference_update(s1)
	bg_neurons = list(s2)
	tmp = numpy.array(input_neurons).copy()
	all_neuronIds = tmp.tolist()
	all_neuronIds.append(bg_neurons)

	if display:
		import progressbar as p
		import time

		ctr = 0
		print "==================================================================="
		print "                 Computing I/O Correlations                        "
		print "-------------------------------------------------------------------"
		widgets = ['Processing: ', p.Percentage(), ' ', p.Bar(marker='>', left='[', right=']'),
		           ' ', p.ETA(), ' ']
		pbar = p.ProgressBar(widgets=widgets, maxval=len(seq))
		pbar.start()

	for idx, t in enumerate(seq):
		symId = numpy.where(symbols == t)[0][0]
		response = net_response[symId][globals()['ctr_{0}'.format(str(t))]]
		globals()['ctr_{0}'.format(str(t))] += 1

		t_2 = response.t_start
		t_3 = response.t_stop

		for n, s in enumerate(sy):
			NeuronIds = all_neuronIds[n]
			responses = response.id_slice(NeuronIds)
			rates = numpy.mean(responses.firing_rate(bin_w), 0)
			mean_rate = responses.mean_rate()
			std_rate = responses.mean_rate_std()

			for nn in range(len(inp_spks)):
				input = inp_spks[nn].time_slice(t_2, t_3)
				inp_rate = numpy.mean(input.firing_rate(bin_w), 0)
				mean_inp_rate = input.mean_rate()
				std_inp_rate = input.mean_rate_std()

				C_1 = numpy.mean((rates - mean_rate) * (inp_rate - mean_inp_rate)) / (std_inp_rate*std_rate+\
				      numpy.finfo(float).eps)

				Correlations['corr_stim{0}_pop{1}'.format(str(nn), str(s))].append(C_1)
		if display:
			time.sleep(0.01)
			ctr += 1
			pbar.update(ctr)
	if display:
		pbar.finish()

	return Correlations


########################################################################################################################
def k_pca_classification(net_response, seq, pars_dict, t_1=30., method='PCA'):
	"""
	Reads-in the whole time-series of network activity in response to each individual stimulus,
	performs a dimensionality reduction via PCA and stores the PCA coordinates
	Reads the activity during the test phase and computes distances between each new PCA cluster and
	the training clusters.. Classification is performed by minimizing such distances...
	Not computationally feasible...
	"""
	import sklearn.decomposition
	import sklearn.metrics

	symbols = numpy.sort(numpy.unique(seq))
	for n in symbols:
		globals()['responses_{0}'.format(str(n))] = []

	seq_train = seq[:int(pars_dict['Readout_Pars']['train_fraction']*len(seq))]
	seq_test = seq[int(pars_dict['Readout_Pars']['train_fraction']*len(seq)):]

	# Reduce the data to analyse... Randomly pick 10 network responses...
	idx = numpy.random.randint(0, int(pars_dict['Readout_Pars']['train_fraction']*len(net_response[0])), 10)

	N = pars_dict['Net_Pars']['N']
	time_vec = numpy.arange(0.1, pars_dict['Input_Pars']['stim_dur'] + 0.1, 0.1)

	for n in idx:

		for t in symbols:

			# extract network response
			response = net_response[numpy.where(symbols == t)[0][0]][n]

			# apply exponential filter to the spiking activity
			states = response.spikes_to_states_exp(N, 0.1, t_1)

			# store full activity in response to each individual stimulus (takes a lot of memory)
			globals()['responses_{0}'.format(str(t))].append(states)

	pca = sklearn.decomposition.PCA(n_components='mle')
	for n in symbols:
		var = numpy.reshape(numpy.asarray(globals()['responses_{0}'.format(str(n))]), (N, 10* states.shape[1]))
		# Reduce dimensionality of the symbol responses
		globals()['Space_{0}'.format(str(n))] = pca.fit(var.T)
		del(globals()['responses_{0}'.format(str(n))])

	for idx, t in enumerate(seq_test):
		# extract network response
		response = net_response[numpy.where(symbols == t)[0][0]][globals()['ctr_{0}'.format(str(t))]]
		globals()['ctr_{0}'.format(str(t))] += 1

		# apply exponential filter to the spiking activity
		states = response.spikes_to_states_exp(N, 0.1, t_1)

		# TODO to be continued...
		globals()['New_space_{0}'.format(str(t))] = globals()['Space_{0}'.format(str(t))].transform(states.T)

		# compute Mahalanobis distance between the centroids of the New_space and that of the symbol Space
		sklearn.metrics.pairwise.pairwise_distances()


########################################################################################################################
def extract_state_matrix(net_response, seq, pars_dict, method='filter', t_0=200., t_1=30., display=True):
	"""
	Reads the network responses to the stimuli and, depending on the specified method,
	returns a NxT matrix, corresponding to the state of each neuron (1,...,N) in response
	to each stimulus (1,...,T).
	Allowed methods:
		- filter: applies an exponential filter to the spike trains, with tau=t_1
		and takes a snapshot of the filtered spike trains at time t_0, which sets the state
		- counts: state variable corresponds to spike counts of each neuron over the duration
		of the stimulus, or over the interval [t_0, t_1]
		- binary: returns a binary matrix...
	"""
	symbols = numpy.sort(numpy.unique(seq))
	for n in symbols:
		globals()['ctr_{0}'.format(str(n))] = 0

	N = pars_dict['Net_Pars']['nE']
	time_vec = numpy.arange(0.1, pars_dict['Input_Pars']['stim_dur']+0.1, 0.1)
	StateMatrix = numpy.zeros((N, len(seq)))

	if not pars_dict['Options']['Topology'] == 'None':
		NeuronIds = range(2, int(N)+2)
	else:
		NeuronIds = range(1, int(N)+1)

	if display:
		import progressbar as p
		import time

		ctr = 0
		print "==================================================================="
		print "           Extracting State Matrix ({0} symbols in sequence): ".format(str(len(seq)))
		print "-------------------------------------------------------------------"
		widgets = ['Processing: ', p.Percentage(), ' ', p.Bar(marker='>', left='[', right=']'),
		           ' ', p.ETA(), ' ']
		pbar = p.ProgressBar(widgets=widgets, maxval=len(seq))
		pbar.start()

	if method == 'filter':
		for idx, t in enumerate(seq):
			response = net_response[numpy.where(symbols == t)[0][0]][globals()['ctr_{0}'.format(str(t))]]
			globals()['ctr_{0}'.format(str(t))] += 1

			time_pars = response.time_parameters()

			if time_pars[0]+(t_0-(t_1+1)) > time_pars[0]:
				t_slice = [time_pars[0]+(t_0-(t_1+1)), time_pars[0]+t_0]
			else:
				t_slice = [time_pars[0], time_pars[0]+t_0]

			response = response.id_slice(NeuronIds)
			response = response.time_slice(t_slice[0], t_slice[1])
			states = response.spikes_to_states_exp(N, 0.1, t_1, min(NeuronIds))
			StateMatrix[:, idx] = states[:, -1]

			if display:
				time.sleep(0.01)
				ctr += 1
				pbar.update(ctr)
		if display:
			pbar.finish()

	elif method == 'counts':
		for idx, t in enumerate(seq):
			response = net_response[numpy.where(symbols == t)[0][0]][globals()['ctr_{0}'.format(str(t))]]
			globals()['ctr_{0}'.format(str(t))] += 1

			response = response.id_slice(NeuronIds)
			rates = numpy.zeros(N)
			for idss in response.id_list():
				t_2 = response.t_start + t_0
				t_3 = response.t_stop - t_1
				rates[int(idss-min(NeuronIds))] = response.spiketrains[idss].mean_rate(t_start=t_2, t_stop=t_3)

			for idx1, nn in enumerate(rates):
				StateMatrix[idx1, idx] = nn * pars_dict['Input_Pars']['stim_dur']/1000.

			if display:
				time.sleep(0.01)
				ctr += 1
				pbar.update(ctr)
		if display:
			pbar.finish()
		StateMatrix = StateMatrix.astype(int)

	elif method == 'rates':
		for idx, t in enumerate(seq):
			response = net_response[numpy.where(symbols == t)[0][0]][globals()['ctr_{0}'.format(str(t))]]
			globals()['ctr_{0}'.format(str(t))] += 1
			response.id_slice(numpy.arange(1., N, 1.).astype(int).tolist())

			rates = numpy.zeros(N)
			for id in response.id_list():
				rates[int(id)] = response.spiketrains[id].mean_rate()
			StateMatrix[:, idx] = rates

			if display:
				time.sleep(0.01)
				ctr += 1
				pbar.update(ctr)
		if display:
			pbar.finish()

	elif method == 'binary':
		for idx, t in enumerate(seq):
			response = net_response[numpy.where(symbols == t)[0][0]][globals()['ctr_{0}'.format(str(t))]]
			globals()['ctr_{0}'.format(str(t))] += 1

			time_pars = response.time_parameters()

			if time_pars[0] + (t_0 - (t_1 + 1)) > time_pars[0]:
				t_slice = [time_pars[0] + (t_0 - (t_1 + 1)), time_pars[0] + t_0]
			else:
				t_slice = [time_pars[0], time_pars[0] + t_0]

			response = response.id_slice(NeuronIds)
			response = response.time_slice(t_slice[0], t_slice[1])
			states = numpy.zeros((N, len(numpy.arange(t_slice[0], t_slice[1], 0.1))))
			for nnn in response.spiketrains.items():
				if nnn[0] in NeuronIds:
					states[numpy.where(nnn[0] == numpy.array(NeuronIds))[0][0], :] = nnn[1].spikes2states(0.1)
			StateMatrix[:, idx] = states[:, -1]

			if display:
				time.sleep(0.01)
				ctr += 1
				pbar.update(ctr)
		if display:
			pbar.finish()

	return StateMatrix

########################################################################################################################
def Hamming_dists_betweenResp(state_matrix, seq):
	import scipy.spatial

	st_A = []; st_B = []; st_C = []
	hamm_d_A = []; hamm_d_B = []; hamm_d_C = []
	for ii, t in enumerate(seq):
		if t == 'A':
			st_A.append(state_mat_3[:, ii])
		elif t == 'B':
			st_B.append(state_mat_3[:, ii])
		elif t == 'C':
			st_C.append(state_mat_3[:, ii])

	for i in range(len(st_A)):
		for ii in range(len(st_A)):
			if i != ii:
				hamm_d_A.append(scipy.spatial.distance.hamming(st_A[i], st_A[ii]))
	for i in range(len(st_B)):
		for ii in range(len(st_B)):
			if i != ii:
				hamm_d_B.append(scipy.spatial.distance.hamming(st_B[i], st_B[ii]))
	for i in range(len(st_C)):
		for ii in range(len(st_C)):
			if i != ii:
				hamm_d_C.append(scipy.spatial.distance.hamming(st_C[i], st_C[ii]))

	return hamm_d_A, hamm_d_B, hamm_d_C



#=======================================================================================================================
# Additional Sub-functions
#=======================================================================================================================
def extract_data_fromfile(fname):
	"""
	Read the output files written by the spike detector and converts them
	to gid, time format.
	Arguments:
		- fname = nest.GetStatus(Device)[0]['filenames']
	"""

	if nest.hl_api.is_sequencetype(fname):
		data = None

		for f in fname:
			if is_not_empty_file(f):
				if data is None:
					data = numpy.loadtxt(f)
				else:
					data = numpy.concatenate((data, numpy.loadtxt(f)))
	else:
		data = numpy.loadtxt(fname)

	return data


##############################################################################
def rescale(OldList, NewMin, NewMax):
	NewRange = float(NewMax - NewMin)
	OldMin = min(OldList)
	OldMax = max(OldList)
	OldRange = float(OldMax - OldMin)
	ScaleFactor = NewRange / OldRange
	NewList = []
	for OldValue in OldList:
		NewValue = ((OldValue - OldMin) * ScaleFactor) + NewMin
		NewList.append(NewValue)
	return NewList


#############################################################################
def is_not_empty_file(fpath):
	return True if os.path.isfile(fpath) and os.path.getsize(fpath) > 0 else False


#############################################################################
def save_workspace_vars(filename):
	"""
	Saves all the variables in the workspace in a shelve file...
	"""
	import shelve
	import types

	my_shelf = shelve.open(filename, 'n')  # 'n' for new

	for key in dir():
		if not (key.startswith('_')):
			if type(globals()[key]) != types.ModuleType or type(globals()[key]) != types.InstanceType or type(
					globals()[key]) != types.FunctionType:
				try:
					my_shelf[key] = globals()[key]
				except TypeError:
					# __builtins__, my_shelf, and imported modules cannot be shelved.
					print('ERROR shelving: {0}'.format(key))
					continue
	my_shelf.close()


def restore_workspace_vars(filename):
	"""
	Restore the entire workspace if it has been saved with shelf
	"""
	import shelve

	my_shelf = shelve.open(filename)
	for key in my_shelf:
		globals()[key] = my_shelf[key]
	my_shelf.close()


def smooth(x, window_len=9, window='hamming'):
	"""smooth the data using a window with requested size.

	This method is based on the convolution of a scaled window with the signal.
	The signal is prepared by introducing reflected copies of the signal
	(with the window size) in both ends so that transient parts are minimized
	in the begining and end part of the output signal.

	input:
		x: the input signal
		window_len: the dimension of the smoothing window
		window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
			flat window will produce a moving average smoothing.

	output:
		the smoothed signal

	example:

		t=linspace(-2,2,0.1)
		x=sin(t)+randn(len(t))*0.1
		y=smooth(x)

	see also:

	numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
	scipy.signal.lfilter
	"""

	if type(x) == type([]):
		x = numpy.array(x)

	if x.ndim != 1:
		raise ValueError, "smooth only accepts 1 dimension arrays."

	if x.size < window_len:
		raise ValueError, "Input vector needs to be bigger than window size."

	if window_len<3:
		return x

	if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
		raise ValueError, "Window is one of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"

	s = numpy.r_[2*x[0]-x[window_len:1:-1],x,2*x[-1]-x[-1:-window_len:-1]]

	if window == 'flat': #moving average
		w = ones(window_len,'d')
	else:
		w = eval('numpy.'+window+'(window_len)')
	y = numpy.convolve(w/w.sum(),s,mode='same')

	# return the smoothed signal, chopping off the ends so that it has the previous size.
	return y[window_len-1:-window_len+1]


#######################################################################################################
def hamming_dist(str1, str2):
	assert len(str1) == len(str2)
	return sum(itertools.imap(operator.ne, str1, str2))