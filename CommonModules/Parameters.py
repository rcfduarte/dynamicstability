__author__ = 'neuro'

import numpy as np
from Pars_Script import *


def set_params():
	"""
	Create a parameters dictionary containing all entries in Pars_Script, organized by group
	"""

	P = {
		'Sim_Pars': {

		####################################################################
		## Simulation Parameters
		####################################################################
		'dt': dt,                               # Time step
		'SimT': SimT,                           # Total Simulation Time
		'TransientT': TransientT,               # Initial transient time
		'PlasticityT': PlasticityT
		},

		'Net_Pars': {
		####################################################################
		## Network Parameters
		####################################################################
		#### Population Size
		'N': N,                                 # Total Number of neurons
		'nE': nE,                               # E neurons
		'nI': nI                                # I neurons
		},

		'Neuron_Pars': {
		####################################################################
		## Neuron Parameters
		####################################################################
		'Neuron_model': model,              # conductance-based iaf neurons
		'E_L': E_L,                         # [mV] - Resting Membrane Potential
		'V_reset': V_reset,                 # [mV] - Reset Potential
		'V_th': V_th,                       # [mV] - Spike threshold
		'tau_ref': tau_ref,                 # [ms] - Refractory time
		'g_leak': g_leak,                   # [nS] - leak conductance
		'E_ex': E_AMPA,                     # [mV] - Exc reversal potential
		'E_in': E_GABA,                     # [mV] - Inh reversal potential
		'tau_E': tau_E,                     # [ms] - Exc conductance decay time constant
		'tau_I': tau_I,                     # [ms] - Inh conductance decay time constant
		'I_b': I_b,                         # [pA] - Background current
		'C_m': C_m,                         # [pF] - Membrane Capacity
		'tau_minus': tau_minus              # [ms] - length of (STDP) window...
		},

		'Synapse_Pars': {
		####################################################################
		## Synapse Parameters
		####################################################################
		'pEE': pEE,                         # Probability of connection (E->E)
		'pEI': pEI,                         # Probability of connection (I->E)
		'pIE': pIE,                         # Probability of connection (E->I)
		'pII': pII,                         # Probability of connection (I->I)
		'mu_E': mu_E,                       # parameters of Gaussian distributions from which the w are drawn
		'sig_E': sig_E,
		'mu_I': mu_I,
		'sig_I': sig_I,
		'g_bar_E': g_bar_E,                 # [nS] - weight of E synapses
		'g_bar_I': g_bar_I,                 # [nS] - weight of I synapses
		'inh_scale': inh_scale,             # scaling factor for inhibitory ws
		'delays': delays                    # [ms] - conduction delays
		},

		'Input_Pars': {
		####################################################################
		## Task/Input Parameters
		####################################################################
		'Ext_rate': Ext_rate                # [Hz] - rate of external Poisson input
		},

		'Options': {
		####################################################################
		## Implementation Options (see Pars_Scripts for more details)
		####################################################################
		'Input_Population': Input_population,  # Nature of input neurons [E/I/EI]
		'Input_signal': Input_signal,          # Nature of input signal [rate, burst]
		'Topology': Topology,                  # Network topology
		'Plasticity': Plasticity,              # Plasticity mechanisms
		'Task': Task,                          # Task type
		'Connectivity': Connectivity,          # Connectivity type
		'Extra_Pars': Extra_Pars
		},

		'Data_Pars': {
		####################################################################
		## Data Handling Parameters
		####################################################################
		'data_label': data_label,             # String with the desired data label
		'data_path': data_path                # path for data storage
		},

		'System_Pars': {
		####################################################################
		## System Parameters
		####################################################################
		'nodes': nodes,                     # how many compute nodes to use
		'proc_per_node': proc_per_node,     # how many processors to use per compute node
		'mem': mem,                         # how much total memory required (in GB)
		'walltime': walltime,               # estimated duration (max)
		'queue': queue                      # queue to use (short, medium, long)
		},

		'Plasticity_Pars': {
		####################################################################
		## Plasticity Parameters
		####################################################################
		# Defined later
		},

	    'Readout_Pars':{
		####################################################################
		## Readout Parameters
		####################################################################
		# Defined later (depend in task and input...)
	    }
	}

	return P


################################################################################
def set_params_derived(P):
	"""
	This function takes a dictionary P as input and sets the option-dependent
	parameter values, as well as the parameters whose values depend on other parameters
	"""

	i_sc = P['Synapse_Pars']['inh_scale']
	ge = P['Synapse_Pars']['g_bar_E']
	P['Synapse_Pars']['g_bar_I'] = (i_sc * ge)  # [nS] - weight of I synapses

	###################################################################
	# SET TASK-SPECIFIC PARAMETERS
	###################################################################
	if P['Options']['Task'] == 'InputEncoding':
		P['Input_Pars']['alphabet'] = alphabet
		P['Input_Pars']['states'] = states
		P['Input_Pars']['transition_pr'] = transitions
		P['Input_Pars']['start_states'] = start_states
		P['Input_Pars']['end_states'] = end_states
		P['Input_Pars']['Task'] = Input_Task
		P['Input_Pars']['nStrings'] = nStrings
		P['Input_Pars']['nDiscardSeq'] = nDiscardSeq
		P['Input_Pars']['stim_dur'] = stim_dur
		P['Input_Pars']['I_stim_I'] = I_stim_I
		P['Input_Pars']['I_back_r'] = I_back_r
		P['Input_Pars']['peak_amp'] = peak_amp
		P['Input_Pars']['resolution'] = resolution
		P['Input_Pars']['int_sym'] = int_sym
		P['Input_Pars']['nInputTrains'] = nInputTrains
		P['Input_Pars']['nz_rate'] = nz_rate
		P['Sim_Pars']['nTrials'] = nTrials

		P['Readout_Pars']['learn_rule'] = learn_rule
		P['Readout_Pars']['Readout_Task'] = Readout_Task
		P['Readout_Pars']['train_fraction'] = train_fraction
		P['Readout_Pars']['t_0'] = t_0
		P['Readout_Pars']['filter_tau'] = filter_tau

		if 'E' in P['Options']['Input_Population']['Nature']:
			P['Input_Pars']['nIU'] = P['Options']['Input_Population']['Fraction'] * nE     # Number of input-specific
			# neurons
		elif 'I' in P['Options']['Input_Population']['Nature']:
			P['Input_Pars']['nIU'] = P['Options']['Input_Population']['Fraction'] * nI
		elif 'EI' in P['Options']['Input_Population']['Nature']:
			P['Input_Pars']['nIU'] = P['Options']['Input_Population']['Fraction'] * N

		if P['Readout_Pars']['Readout_Task'] == 'n-back_memory':
			P['Readout_Pars']['n_back'] = n_back
		elif P['Readout_Pars']['Readout_Task'] == 'prediction':
			P['Readout_Pars']['n_for'] = n_for

	elif (P['Options']['Task'] == 'KernelQuality') or (P['Options']['Task'] == 'Generalization'):
		P['Input_Pars']['dur'] = stim_dur
		P['Input_Pars']['IStimI'] = I_stim_I
		P['Sim_Pars']['nTrials'] = nTrials
		if 'E' in P['Options']['Input_Population']['Nature']:
			P['Input_Pars']['nIU'] = P['Options']['Input_Population']['Fraction'] * nE     # Number of input-specific
			# neurons
		elif 'I' in P['Options']['Input_Population']['Nature']:
			P['Input_Pars']['nIU'] = P['Options']['Input_Population']['Fraction'] * nI
		elif 'EI' in P['Options']['Input_Population']['Nature']:
			P['Input_Pars']['nIU'] = P['Options']['Input_Population']['Fraction'] * N

	elif P['Options']['Task'] == 'Lyapunov':
		P['Sim_Pars']['nTrials'] = nTrials
	else:
		P['Input_Pars']['nIU'] = 0.

	####################################################################
	# SET PLASTICITY PARAMETERS
	####################################################################
	if 'iSTDP' in P['Options']['Plasticity']:
		################################################################
		## 							iSTDP
		################################################################
		P['Plasticity_Pars']['tau_iSTDP'] = tau_iSTDP  # [ms] - Decay constant of synaptic trace, inh STDP rule
		P['Plasticity_Pars']['eta'] = eta_iSTDP        # learning rate
		P['Plasticity_Pars']['ro'] = ro                # [Hz] - relative strength of non-Hebbian weigth decrease
													# relative to the Hebbian...
		P['Plasticity_Pars']['alpha_I'] = alpha       # Presynaptic Offset...
		P['Plasticity_Pars']['wmin'] = w_min          # Minimum inh. synaptic weight
		P['Plasticity_Pars']['wmax'] = (10. * (((-i_sc) * ge)))  # Maximum inh. synpatic weight

	if 'eSTDP' in P['Options']['Plasticity']:
		################################################################
		##							eSTDP
		################################################################
		P['Plasticity_Pars']['tau_eSTDP'] = tau_eSTDP   # [ms] - time window...
		P['Plasticity_Pars']['lamb'] = lamb             # [pS] - average amount of potentiation after one pairing
		P['Plasticity_Pars']['alpha_E'] = alpha_E       # [pS] - average amount of depression after one pairing (tune)
		P['Plasticity_Pars']['W_MAX'] = W_MAX

	return P


##############################################################################
def set_default_paramaters():
	"""
	Set the default parameters
	"""

	P = {
		####################################################################
		## Simulation Parameters
		####################################################################
		'Sim_Pars': {
			'dt': 0.1,                    # Time step
			'SimT': 1000.,                # Total Simulation Time
			},
		####################################################################
		## Network Parameters
		####################################################################
		'Net_Pars': {
			'N': 10000.,               # Total Number of neurons
			'nE': 8000.,               # E neurons
			'nI': 2000.                # I neurons
			},
		####################################################################
		## Neuron Parameters
		####################################################################
		'Neuron_Pars': {
			'Neuron_model': 'iaf_cond_exp',  # conductance-based iaf neurons
			'E_L': -70.,                     # [mV] - Resting Membrane Potential
			'V_reset': -60.,                 # [mV] - Reset Potential
			'V_th': -50.,                    # [mV] - Spike threshold
			'tau_ref': 5.,                   # [ms] - Refractory time
			'g_leak': 16.7,                  # [nS] - leak conductance
			'E_ex': 0.,                      # [mV] - Exc reversal potential
			'E_in': -80.,                    # [mV] - Inh reversal potential
			'tau_E': 5.,                     # [ms] - Exc conductance decay time constant
			'tau_I': 10.,                     # [ms] - Inh conductance decay time constant
			'I_b': 0.,                       # [pA] - Background current
			'C_m': 250.                      # [pF] - Membrane Capacity
			},
		####################################################################
		## Synapse Parameters
		####################################################################
		'Synapse_Pars': {
			'eps': 0.1,                     # for similar connection probabilities
			'pEE': 0.1, # Probability of connection (E->E)
			'pEI': 0.1, # Probability of connection (I->E)
			'pIE': 0.1, # Probability of connection (E->I)
			'pII': 0.1, # Probability of connection (I->I)
			'mu_E': 1., # mean of Gaussian distribution from which the w are drawn
			'sig_E': 0.25,
			'mu_I': 1.,
			'sig_I': 0.25,
			'g_bar_E': 1.8, # [nS] - weight of E synapses
			'inh_scale': -12., # scaling factor for inhibitory ws
			'delays': 1.5            # [ms] - conduction delays
			},
		####################################################################
		## Task/Input Parameters
		####################################################################
		'Input_Pars': {
			'Ext_rate': 5.            # [Hz] - rate of external Poisson input
			},
		####################################################################
		## Plasticity Parameters
		####################################################################
		'Plasticity_Pars': {

			},
		####################################################################
		## Options
		####################################################################
		'Options': {
			'Input_population': 'EI',
			'Input_delivery': 'weighted',
			'Input_signal': 'Frozen',
			'Net_Structure': 'None',
			'Plasticity': 'None',
			'Task': 'None'
		},
		####################################################################
		## Data Handling Parameters
		####################################################################
		'Data_Pars': {
			'data_label': '', # String with the desired data label
			'data_path': '/scratch/duarte/' # path for data storage
		}
	}

	return P

