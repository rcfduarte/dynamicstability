import os
import settings
import pickle
import sys
sys.path.append('../')
from CommonModules.Parameters import *
from KernelQuality_Analysis import *

PD = set_params()

array_id = os.getenv("PBS_ARRAYID")


def computation(x, x_var, y, y_var, z, z_var, PD, i):
	"""
	Main function: Gets the parameters to scan and their values as arguments and
	runs the main simulations
	Starts by setting the correct parameters in the dictionary prior to calling the main simulation
	functions
	"""

	print("=======================================================================")
	print("                      {0} Task".format(PD['Options']['Task']))
	print("=======================================================================")
	print(" Simulation {0}: For {1} = {2}, {3} = {4}".format(z, x_var, x, y_var, y))

	# Set variables in dictionary
	for k, v in PD.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			if k1 == x_var:
				PD[k][k1] = x
			if k1 == y_var:
				PD[k][k1] = y
			if k1 == z_var:
				PD[k][k1] = z

	PD = set_params_derived(PD)

	if PD['Options']['Task'] == 'KernelQuality':
		fileName1 = open('./Spike_Templates', 'r')
		Spk_Templates = pickle.load(fileName1)
		fileName1.close()
	elif PD['Options']['Task'] == 'Generalization':
		fileName1 = open('./Jittered_templates', 'r')
		Spk_Templates = pickle.load(fileName1)
		fileName1.close()

	#### Run Main Simulation ###
	PD_Result = kernel_quality(PD, Spk_Templates, i)

	### Save Data ###
	label = PD['Data_Pars']['data_label']
	fil = open("%s" % label + "SIM:{0}PARS_{1}={2}_{3}={4}".format(z, x_var, x, y_var, y), "w")
	pickle.dump(PD_Result, fil)
	fil.close()

