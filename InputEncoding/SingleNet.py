__author__ = 'duarte'
import sys
sys.path.append('../')
from CommonModules.Parameters import *
from CommonModules.Analysis_fcns import *
from CommonModules.Auxiliary_fcns import *
from CommonModules.Input_fcns import *
from CommonModules.SymbolSeqTools import *
import numpy as np
import nest
import os


PD = set_params()
pars_dict = set_params_derived(PD)

##############################################################
# Set input variables and options
##############################################################
Input_Pars_Dict = pars_dict['Input_Pars']

for k, v in Input_Pars_Dict.items():
	globals()[k] = v
options = 'None'
Grammar = FSG(states, alphabet, transition_pr, start_states, end_states, options)
Seq = np.tile(alphabet, nStrings)
Seq[nDiscardSeq:] = np.random.permutation(Seq[nDiscardSeq:])

###############################################################
# Generate input signals
###############################################################
step_signal, rate_signal, time_data = encode_input(Grammar, Seq, Input_Pars_Dict, noise=True)

if pars_dict['Options']['Input_signal'] == 'Rate':
	input_SpkTrains = generate_burst_spktrains(Grammar, Input_Pars_Dict, rate_signal, step_signal, time_data,
	                                           display=False)
	#for idx, n in enumerate(input_SpkTrains):
	#	input_SpkTrains[idx] = n.time_slice(TransientT, TransientT + SimT)

elif pars_dict['Options']['Input_signal'] == 'Frozen':
	input_SpkTrains = generate_frozen_spiketrains(Grammar, Input_Pars_Dict, step_signal, time_data, display=False)
	#input_SpkTrains = input_SpkTrains.time_slice(TransientT, TransientT + SimT)
########################################################################
# Extract all Parameters
########################################################################
for k, v in pars_dict.items():
	globals()[k] = v
	for k1, v1 in (globals()[k]).items():
		globals()[k1] = v1

########################################################################
# Reset NEST kernel
########################################################################
nest.ResetKernel()
nest.SetKernelStatus({'resolution': dt,
                      'print_time': False,
                      'data_path': data_path,
                      'data_prefix': data_label,
                      'overwrite_files': True,
                      'local_num_threads': proc_per_node})
SimT = (stim_dur+I_stim_I)*len(Grammar.alphabet)*nStrings

########################################################################
# 					CREATE NEURONS AND DEVICES
########################################################################
tp_dict = set_topology(pars_dict)
weights = set_synapses(pars_dict)
tp_dict = connect_network(weights, pars_dict, tp_dict)

E_neurons = tp_dict['E_neurons']
I_neurons = tp_dict['I_neurons']

create_inputs(pars_dict, tp_dict, input_SpkTrains)

#-----------------------------------------------------------------------
# Randomize initial Vm
#-----------------------------------------------------------------------
nest.SetStatus(E_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nE))))
nest.SetStatus(I_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nI))))

#-----------------------------------------------------------------------
# Create and Set Devices
#-----------------------------------------------------------------------
SpkDetector = nest.Create('spike_detector')
nest.CopyModel('multimeter', 'singleNeuron_multimeter')
#nest.CopyModel('multimeter', 'population_multimeter')
nest.SetDefaults('singleNeuron_multimeter', {'record_from': ['g_ex', 'g_in', 'V_m'],
                                             'record_to': ['memory'], 'to_file': False,
                                             'to_memory': True, 'interval': dt,
                                             'label': data_label+'inpNeurons_multimeter',
                                             'withtime': True, 'start': 0.})
#nest.SetDefaults('population_multimeter', {'record_from': ['V_m'],
#                                'record_to': ['accumulator'], 'start': PlasticityT+TransientT})
#E_population_multimeter = nest.Create('population_multimeter')
#I_population_multimeter = nest.Create('population_multimeter')

#nest.DivergentConnect(E_population_multimeter, E_neurons)
#nest.DivergentConnect(I_population_multimeter, I_neurons)

inp_neurons_multimeter = nest.Create('singleNeuron_multimeter', 3)
bg_neuron_multimeter = nest.Create('singleNeuron_multimeter', 1)
nest.SetStatus(bg_neuron_multimeter, {'label': data_label+'bgNeuron_multimeter'})

nest.SetStatus(SpkDetector, {'record_to': ['memory'], 'start': 0., #PlasticityT + TransientT,
                                'to_file': False, 'to_memory': True,
	                             'label': 'SpkDetector'})

background_PG = nest.Create('poisson_generator')

nest.SetStatus(background_PG, {'rate': Ext_rate * (pEE * nE)})

########################################################################
# 							CONNECT
########################################################################
nest.DivergentConnect(background_PG, list(itertools.chain(*[E_neurons, I_neurons])), weight=g_bar_E, delay=delays,
                      model='Ext_Synapse')

nest.ConvergentConnect(list(itertools.chain(*[E_neurons, I_neurons])), SpkDetector)
inp_mulm_targets = np.random.randint(0, len(tp_dict['Input_neurons'][0]), 3)

for n_mm in range(len(alphabet)):
	idx = tp_dict['Input_neurons'][n_mm][inp_mulm_targets[n_mm]]
	nest.Connect([inp_neurons_multimeter[n_mm]], [idx])

bg_neuron_id = np.random.randint(0, len(E_neurons), 1)
while bg_neuron_id in np.concatenate(tp_dict['Input_neurons']):
	bg_neuron_id = np.random.randint(0, len(E_neurons), 1)

nest.Connect(bg_neuron_multimeter, [bg_neuron_id[0]])
########################################################################
# Simulate
########################################################################
data_tag = data_path + data_label + '_PatRate={0}_nInputTrains={1}_IU={2}'.format(str(peak_amp),
                str(nInputTrains), Input_Population['Nature']+ '_' + Input_Population['Target'])

wEE = np.zeros([len(E_neurons), len(E_neurons)])
wEI = np.zeros([len(I_neurons), len(E_neurons)])

aEE = nest.GetConnections(E_neurons, E_neurons)
aEI = nest.GetConnections(I_neurons, E_neurons)
c_EE = nest.GetStatus(aEE, keys='weight')
c_EI = nest.GetStatus(aEI, keys='weight')

for idx, n in enumerate(aEE):
	wEE[n[0] - min(E_neurons), n[1] - min(E_neurons)] += c_EE[idx]
for idx, n in enumerate(aEI):
	wEI[n[0] - min(I_neurons), n[1] - min(E_neurons)] += c_EI[idx]

with open(data_tag+'_Weights_EE_Step=0.npy', 'w') as fp1:
	np.save(fp1, wEE)
with open(data_tag+'_Weights_EI_Step=0.npy', 'w') as fp2:
	np.save(fp2, wEI)

nest.Simulate(TransientT)# + SimT)

if Options['Plasticity'] != 'None':
	wEE = np.zeros([len(E_neurons), len(E_neurons)])
	wEI = np.zeros([len(I_neurons), len(E_neurons)])
	aEE = nest.GetConnections(E_neurons, E_neurons)
	aEI = nest.GetConnections(I_neurons, E_neurons)
	c_EE = nest.GetStatus(aEE, keys='weight')
	c_EI = nest.GetStatus(aEI, keys='weight')

	for idx, n in enumerate(aEE):
		wEE[n[0] - min(E_neurons), n[1] - min(E_neurons)] += c_EE[idx]
	for idx, n in enumerate(aEI):
		wEI[n[0] - min(I_neurons), n[1] - min(E_neurons)] += c_EI[idx]

	with open(data_tag+'_Weights_EE_Step=Trans.npy', 'w') as fp1:
		np.save(fp1, wEE)
	with open(data_tag+'_Weights_EI_Step=Trans.npy', 'w') as fp2:
		np.save(fp2, wEI)


nest.Simulate(SimT)

if Options['Plasticity'] != 'None':
	wEE = np.zeros([len(E_neurons), len(E_neurons)])
	wEI = np.zeros([len(I_neurons), len(E_neurons)])
	aEE = nest.GetConnections(E_neurons, E_neurons)
	aEI = nest.GetConnections(I_neurons, E_neurons)
	c_EE = nest.GetStatus(aEE, keys='weight')
	c_EI = nest.GetStatus(aEI, keys='weight')

	for idx, n in enumerate(aEE):
		wEE[n[0] - min(E_neurons), n[1] - min(E_neurons)] += c_EE[idx]
	for idx, n in enumerate(aEI):
		wEI[n[0] - min(I_neurons), n[1] - min(E_neurons)] += c_EI[idx]

	with open(data_tag+'_Weights_EE_Step=End.npy', 'w') as fp1:#
		np.save(fp1, wEE)
	with open(data_tag+'_Weights_EI_Step=End.npy', 'w') as fp2:
		np.save(fp2, wEI)


#########################################################################
# Analysis
#########################################################################
#nest.raster_plot.from_device(SpkDetector)
time_interval = [0., SimT]

SpkTimes = nest.GetStatus(SpkDetector)[0]['events']['times']
NeuronId = nest.GetStatus(SpkDetector)[0]['events']['senders']
tmp = [(NeuronId[n], SpkTimes[n]) for n in range(len(SpkTimes))]
Spk_list = signals.SpikeList(tmp, np.unique(NeuronId).tolist(), t_start=time_interval[0], t_stop=time_interval[1])
bg_single_data = extract_data_fromfile(nest.GetStatus(bg_neuron_multimeter)[0]['filenames'])
#E_pop_Vm = nest.GetStatus(E_population_multimeter)[0]['events']['Vm']
#I_pop_Vm = nest.GetStatus(I_population_multimeter)[0]['events']['Vm']


for ids, n in enumerate(alphabet):
	globals()['inp_single_data_{0}'.format(n)] = nest.GetStatus([inp_neurons_multimeter[ids]])
	globals()['inp_single_{0}'.format(n)] = dict()

bg_single = dict()
bg_single['Vm'] = bg_single_data[:, 4]
bg_single['g_e'] = bg_single_data[:, 2]
bg_single['g_i'] = bg_single_data[:, 3]
bg_single['t'] = bg_single_data[:, 1]

input_single_neuron_stats = []
for ids, n in enumerate(alphabet):
	globals()['inp_single_{0}'.format(n)]['Vm'] = globals()['inp_single_data_{0}'.format(n)][:, 4]
	globals()['inp_single_{0}'.format(n)]['g_e'] = globals()['inp_single_data_{0}'.format(n)][:, 2]
	globals()['inp_single_{0}'.format(n)]['g_i'] = globals()['inp_single_data_{0}'.format(n)][:, 3]
	globals()['inp_single_{0}'.format(n)]['t'] = globals()['inp_single_data_{0}'.format(n)][:, 1]
	input_single_neuron_stats.append(globals()['inp_single_{0}'.format(n)])

tp.DumpLayerNodes([tp_dict['E_layer'][0], tp_dict['I_layer'][0]], data_tag+'_LAYERS')
tp_dict['stored_data'] = data_tag+'_LAYERS'

results = dict()
results['data_tag'] = data_tag
results['pars_dict'] = pars_dict
results['tp_dict'] = tp_dict
results['Full_Seq'] = Seq
results['spk_data'] = Spk_list
results['input_SpkTrains'] = input_SpkTrains
results['Seq_time_data'] = time_data
results['inp_signal'] = step_signal
results['bg_activity_single'] = bg_single
results['inp_neurons_activity'] = input_single_neuron_stats

with open(data_tag+'_SINGLE_NET_DATA', 'w') as fw:
	pickle.dump(results, fw)

for n in range(len(alphabet)):
	flist = nest.GetStatus([inp_neurons_multimeter[n]])[0]['filenames']

	for nn in flist:
		os.remove(nn)

flist = nest.GetStatus(bg_neuron_multimeter)[0]['filenames']
for nn in flist:
	os.remove(nn)
#pl.show(block=False)