__author__ = 'duarte'

import pickle
import sys
import numpy as np
import pylab as pl
sys.path.append('../')
from CommonModules.Analysis_fcns import *
from CommonModules.Plotting_routines import *

fname = 'InputEncoding_StaticNetwork_nInputTrains=50.0_nIU=10000.0'

print "Reading Data..."
with open('./DATA/Gaussian_wIn/{0}'.format(fname), 'r') as fp:
	S = pickle.load(fp)
	for k, v in S['Results'].items():
		globals()[k] = v
print "Data loaded successfully!"

########################################################################################################################
# Compute state distances:
#Mean_Dist, MeanFinal, DistMat, Mean_inpDist, Mean_inpFinal, inpDistMat = compute_state_separation(Net_Response, S,
#
# input_spikes=input_SpkTrains,
#                                                                                                  filter_tau=30.,
# display=True)

# Discard first symbols from sequence:
seq = Full_Seq[S['Input_Pars']['nDiscardSeq']:]
symbols = np.sort(np.unique(seq))

times = np.arange(10., 200., 10.)
Results = dict()
#Results['Mean_Dists'] = Mean_Dist
Perfs = []
Weights = []
MI1 = []
MI2 = []


for idx, t in enumerate(times):
	# Extract state matrix... (for anytime computing analysis, use for loop...)
	# Possible methods: filter, counts, rates
	state_matrix = extract_state_matrix(Net_Response, seq, S, method='filter', t_0=t, t_1=30., display=True)

	# Divide state matrix into train and test sets:
	state_train, state_test, target_train, target_test = set_state_and_target(symbols, seq, S, state_matrix)

	########################################################################################################################
	# Readout:
	rules = ['pinv', 'ridge', 'logistic', 'perceptron', 'svm-linear']#, 'svm-rbf']
	wOut_qual = []
	Out = []
	Res = []
	for n in rules:
		print 'Readout algorithm: {0}'.format(n)
		S['Readout_Pars']['learn_rule'] = n
		w_out, fit_obj = train_readout(S, state_train, target_train, display=True)

		if np.shape(w_out)[0]:
			norm_wOut = measure_readout_stability(w_out)
		else:
			norm_wOut = np.nan

		readout_out, fit_obj = test_readout(w_out, fit_obj, state_test, S)

		wOut_qual.append(norm_wOut)
		Out.append(readout_out)

		Perf = measure_readout_performance(readout_out, target_test)

		print "Performance = {0}".format(Perf['mPerf'])

		Res.append(Perf)

	Perfs.append(Res)
	Weights.append(wOut_qual)
	########################################################################################################################
	# Compute Mutual Information (based on global spike counts)
	# Re-compute state matrix... (for anytime computing analysis, use for loop...)
	I1, I2 = compute_information_explicitly(state_matrix, seq, S, n_bins=100)

	print "Mutual Information (method 1): {0}".format(str(np.mean(I1)))
	print "Mutual Information (method 2): {0}".format(str(np.mean(I2)))


	MI2.append(I2)
	MI1.append(I1)

Results['Performances'] = Perfs
Results['wOutQual'] = Weights
Results['MI1'] = MI1
Results['MI2'] = MI2

with open(fname+'_anytime_Results', 'w') as fp:
	pickle.dump(Results, fp)