from Acquire_Data import *
import sys
sys.path.append('../')
from CommonModules.Parameters import *
import pickle


def computation(x, x_var, y, y_var, PD, i):
	"""
	Main function: Gets the parameters to scan and their values as arguments and
	runs the main simulations
	Starts by setting the correct parameters in the dictionary prior to calling the main simulation
	functions
	"""

	print("=======================================================================")
	print("                      {0} Task".format(PD['Options']['Task']))
	print("=======================================================================")
	print(" Computing for {0} = {1}, {2} = {3}".format(x_var, x, y_var, y))

	PD = set_params_derived(PD)

	# Set variables in dictionary
	for k, v in PD.items():
		globals()[k] = v
		for k1, v1 in (globals()[k]).items():
			if k1 == x_var:
				PD[k][k1] = x
			if k1 == y_var:
				PD[k][k1] = y


	#### Run Main Simulation ###
	#PD_Result = simulate(PD, i)
	PD_Result = simulate_online_store(PD, i)
	############################

	### Save Data #####
	label = PD['Data_Pars']['data_label']
	fil = open("%s" % label + "_{0}={1}_{2}={3}".format(x_var, x, y_var, y), "w")
	pickle.dump(PD_Result, fil)
	fil.close()