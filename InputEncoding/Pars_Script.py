#***********************************************************************
#					    	PARAMETERS                                 *
#***********************************************************************
########################################################################
# Simulation Parameters
#=======================================================================
dt = 0.1 		            	  # [ms] - Simulation Time step
SimT = 10000 			          # [ms] - Total Simulation Time
nTrials = 1100.					  # Number of trials (if applicable)
PlasticityT = 0.
########################################################################
# DATA Parameters
#=======================================================================
data_label = 'BurstInput_PlasticNet_1000Samples'
data_path = './DATA/SingleNetData/' #storage/users/rd1000'
########################################################################
# SYSTEM Parameters
#=======================================================================
nodes = 1          # how many compute nodes to use
procs = 1          # how many processor cores to use in total
proc_per_node = 8  # how many processors to use per compute node
mem = 6            # how much total memory requited (in GB)
walltime = '70:00:00'
queue = 'medium'
########################################################################
# Network Parameters
#=======================================================================
### Population Size
N = 10000.                        # Total Number of neurons
nE = 0.8*N				          # E neurons
nI = 0.2*N				          # I neurons

### Neuron
model = 'iaf_cond_exp'		      # neuron model
g_leak = 16.7				      # [nS] leak conductance (other value: 10.nS)
E_L = -70.      			      # [mV] RMP (-60.)
E_GABA = -80.					  # [mV] inhibitory reversal potential
E_AMPA = 0.						  # [mV] excitatory reversal potential
V_th = -50. 					  # [mV] spiking threshold
C_m = 250.      				  # [pF] membrane capacitance (200.)
I_b = 0.					      # [pA] external current (200.)
tau_ref = 5.					  # [ms] refractory time
V_reset = -60.					  # [mV] reset potential
tau_minus = 20.

### Connectivity
epsilon = 0.1  	                  # for similar connection probabilities (0.02)
pEE	= epsilon			          # Probability of connection (E->E)
pEI	= epsilon			          # Probability of connection (I->E)
pIE = epsilon			          # Probability of connection (E->I)
pII	= epsilon			          # Probability of connection (I->I)
mu_E = 1.				          # mean of Gaussian distribution from which the w are drawn...
sig_E = 0.25                       # (and std.)
mu_I = 1.
sig_I = 0.25

### Synapse
tau_E = 5.						  # [ms] AMPA-synapse time constant (5.0)
tau_I = 10.			    		  # [ms] GABA-synapse time constant (10.0)
inh_scale = -12.                  # scaling factor for inhibitory conductances
g_bar_E = 1.8                     # average excitatory conductance
g_bar_I = inh_scale*g_bar_E       # average inhibitory conductance
delays = 1.5                      # conduction delays (constant)

### Plasticity - inhibitory STDP
eta_iSTDP = 1e-2				  # learning rate
tau_iSTDP = 20.					  # [ms] STDP time constant
ro = 5.							  # [Hz] target firing rate
alpha = 2.*ro*(tau_iSTDP*0.001)   #
w_min = 0.                        # min weight values
w_max = 100000. * (-inh_scale)        # max weight values

### Plasticity - excitatory STDP
tau_eSTDP = 20.					  # [ms] - time window...
lamb = 0.01					      # [pS] - average amount of potentiation after one pairing (from experimental data)
alpha_E = 0.917556       		  # [pS] - average amount of depression after one pairing (tune)
W_MAX = 1.                        # max weight value

########################################################################
# Input Parameters
#=======================================================================
## Input
Ext_rate = 5.					  # [Hz] Rate of external source of background input (Poisson)
stim_dur = 50.                   # [ms] Duration of input stimulus
I_stim_I = 00.                   # [ms] Inter-Stimulus Interval
I_back_r = 5.					  # [Hz] - input background rate
peak_amp = 100.     			  # [Hz] - peak amplitude of input rate signal (500)
nz_rate = 2.                      # [Hz] - firing rate of embedding noisy spikes...
nInputTrains = 10.                # number of neurons that make up each input channel
resolution = 1.       	          # [ms] - temporal resolution of the input rate signal
int_sym = '#'					  # symbol to place in between individual strings when concatenating
Input_Task = 'None'
nStrings = nTrials                # number of unique strings (trials)
nDiscardSeq = 300.                # number of sequence symbols to discard (initial transient)
TransientT = nDiscardSeq*(stim_dur+I_stim_I)
states = ['A', 'B', 'C']          # List of strings, each representing one single state of the FSM
alphabet = ['A', 'B', 'C']        # Full alphabet
transitions = [()]                # list of tupples...
start_states = []                 # String start
end_states = []                   # String end
options = []                      # Additional input options

########################################################################
# Readout
#=======================================================================
learn_rule = 'ridge'    # Readout learning rule:
							# - 'pinv' -> uses Moore-Penrose pseudo-inverse calculation
							# - 'FORCE' -> uses FORCE-RLS algorithm
							# - 'ridge' -> uses ridge regularization
							# - 'logistic' -> logistic regression
							# - 'perceptron'
							# - 'svm-linear' -> uses Support Vector Machine
							# - 'svm-rbf'
							# - (...)
train_fraction = 0.8    # Fraction of data used for training
Readout_Task = 'stimulus_classification'   # Specify the task:
										# - 'stimulus_classification'
										# - 'n-back_memory'
										# - 'stimulus_persistence'
										# - 'prediction'
										# - (...)
n_back = 0.             # (if 'n-back_memory' task)
n_for = 0.              # (if 'prediction')
t_0 = stim_dur          # Cut-off time, i.e., time point from where the state matrix is read
filter_tau = 30.        # [ms] - time constant for the exponential low-pass filter applied to the
						# spike trains to define the state matrix
########################################################################
# Options
#=======================================================================
Topology = 'lattice'          # Topological constraints:
									# - 'None' -> no specification
									# - 'lattice' -> neurons positioned
									# in the vertices of a 2D grid
									# - 'random' -> neurons positioned
									# randomly in 2D space

# Set properties of input neurons
Input_population = {'Nature': 'E',     # - target population (E/I/EI)
	                'Target': 'symbol-specific',   # - 'all' or 'symbol-specific'
	                'Fraction': 0.1,  # - fraction of neurons in the target population that receive input
	                'Extra': ['positional', 'max_dist']} # additional parameters
									# - 'random' -> symbol-specific neurons
									# picked randomly
									# - 'positional' -> symbol-specific
									# neuronal populations chosen to be
									# spatially clustered
									# - 'max_dist' -> maximize the distance
									# between input populations
Input_signal = 'Rate'     # Type of signal encoding:
									# - 'Frozen' - signal encoded as frozen Poisson template
									# (symbol-specific), embedded in noisy background
									# - 'Rate' - signal encoded as a realization of an inhomogeneous
									# Poisson process with rate given by a rate signal...
Plasticity = ['iSTDP', 'eSTDP']#'None'    # Control plasticity rules:
									# - 'None' -> all synapses are static
									# - 'iSTDP'
									# - 'eSTDP' (...)
									# - 'iSTDP_local'
Task = 'InputEncoding'          # Nature of the current task (especially for parameter searches):
									# - 'None'
									# - 'Dynamics'
									# - 'InputEncoding'
									# - 'KernelQuality'
									# - 'Generalization'
									# - ('FSG', ...)

Connectivity = ['random']    # Connectivity characteristics (several properties may be selected):
									# - 'random' -> each neuron chooses its
									# targets randomly
									# - 'distance_dependent'-> each neuron
									# connects preferentially with local
									# neighbors (with Gaussian profile, sigC) (**)
									# - 'local_inhibition' -> inhibitory
									# neurons are subdivided into locally
									# and globally inhibiting populations
									# (ratio) (**)
									# - 'explicit_assemblies' -> connections within each
									# assembly are strengthened (**)
									# - 'explicit_paths' -> connections
									# between symbol-specific subpopulations
									# subjected to transitional constraints (**)
Extra_Pars = {'profile': 'gaussian', 'sigC': 5., 'target': ['EE', 'EI'],# extra options for distance-dependent
																		# connectivity
              'I_ratio': .35, 'sig_I': 5., # extra options for local inhibition (ratio of local/global inh neurons)
              'mu_in': 1., 'sig_in': 0.,   # extra options for input connectivity

              'gamma': 5.    # if explicit_assemblies, sets the scale of the strength of assembly connections
			}



