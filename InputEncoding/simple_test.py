import sys
sys.path.append('../')
from CommonModules.Parameters import *
#from CommonModules.Analysis_fcns import *
from CommonModules.Auxiliary_fcns import *
from CommonModules.Plotting_routines import *

PD = set_params()
pars_dict = set_params_derived(PD)

tp_dict = set_topology(pars_dict)
weights = set_synapses(pars_dict)

tp_dict = connect_network(weights, pars_dict, tp_dict)



#create_inputs(pars_dict, topology_dict, input_SpkTrains)


plot_topology(tp_dict)
