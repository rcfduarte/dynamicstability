__author__ = 'duarte'

import pickle
import sys
import numpy as np
import pylab as pl
sys.path.append('../')
from CommonModules.Analysis_fcns import *
from CommonModules.Plotting_routines import *

var_x = 'nInputTrains'
var_y = 'nIU'
dom_x = np.array([1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 500.0])
dom_y = np.array([100.0, 200.0, 500.0, 1000.0, 2000.0, 5000.0, 10000.0])
Dists = []; Perfs = []; Qual = []; MI = []
Results = dict()
path = './DATA/SpikePatternClassification/Gaussian_wIn/1000 Strings/'

for idx, nn in enumerate(dom_x):
	fname = 'InputEncoding_StaticNetwork_nInputTrains={0}_nIU=100.0'.format(str(nn))

	print "Reading Data..."
	if os.path.isfile(path+fname):
		with open(path+'{0}'.format(fname), 'r') as fp:
			S = pickle.load(fp)
			for k, v in S['Results'].items():
				globals()[k] = v
		print "Data loaded successfully!"
	else:
		print "No Data for nIU = 1000.0 / nInputTrains = {0}".format(str(nn))
		continue
	########################################################################################################################
	# Compute state distances:
	#Mean_Dist, MeanFinal, DistMat, Mean_inpDist, Mean_inpFinal, inpDistMat = compute_state_separation(Net_Response, S,
	#                                    input_spikes=input_SpkTrains, filter_tau=30., display=True)

	# Discard first symbols from sequence:
	seq = Full_Seq[S['Input_Pars']['nDiscardSeq']:]
	symbols = np.sort(np.unique(seq))

	#Dists.append(Mean_Dist)

	# Extract state matrix... (for anytime computing analysis, use for loop...)
	# Possible methods: filter, counts, rates
	state_matrix = extract_state_matrix(Net_Response, seq, S, method='filter', t_0=200., t_1=30., display=True)

	# Divide state matrix into train and test sets:
	state_train, state_test, target_train, target_test = set_state_and_target(symbols, seq, S, state_matrix)

	########################################################################################################################
	# Readout:
	rules = ['pinv', 'ridge', 'logistic', 'perceptron', 'svm-linear']#, 'svm-rbf']#, 'FORCE']

	wOut_qual = []
	Out = []
	Res = []
	for n in rules:
		print 'Readout algorithm: {0}'.format(n)
		S['Readout_Pars']['learn_rule'] = n
		w_out, fit_obj = train_readout(S, state_train, target_train, display=True)

		if np.shape(w_out)[0]:
			norm_wOut = measure_readout_stability(w_out)
		else:
			norm_wOut = np.nan

		readout_out, fit_obj = test_readout(w_out, fit_obj, state_test, S)

		wOut_qual.append(norm_wOut)
		Out.append(readout_out)

		Perf = measure_readout_performance(readout_out, target_test)

		print "Performance = {0}".format(Perf['mPerf'])

		Res.append(Perf)

	########################################################################################################################
	# Compute Mutual Information (based on global spike counts)

	I1, I2 = compute_information_explicitly(state_matrix, seq, S, n_bins=100)

	print "Mutual Information (method 1): {0}".format(str(np.mean(I1)))
	print "Mutual Information (method 2): {0}".format(str(np.mean(I2)))

	Perfs.append(Res)
	Qual.append(wOut_qual)
	MI.append(I2)


Results['Performances'] = Perfs
Results['wOutQual'] = Qual
Results['MI'] = MI
#Results['Mean_Dist'] = Dists

with open(fname[:25]+'_Static_nIU={0}_nInputTrainsStudy'.format(100.0), 'w') as fp:
	pickle.dump(Results, fp)