import sys
sys.path.append('../')
from CommonModules.Parameters import *
from CommonModules.Analysis_fcns import *
from CommonModules.Auxiliary_fcns import *
from CommonModules.Input_fcns import *
from CommonModules.SymbolSeqTools import *
import numpy as np
import nest
import os


PD = set_params()
pars_dict = set_params_derived(PD)

##############################################################
# Set input variables and options
##############################################################
Input_Pars_Dict = pars_dict['Input_Pars']

for k, v in Input_Pars_Dict.items():
	globals()[k] = v
options = 'None'
Grammar = FSG(states, alphabet, transition_pr, start_states, end_states, options)
Seq = np.tile(alphabet, nStrings)
Seq[nDiscardSeq:] = np.random.permutation(Seq[nDiscardSeq:])

###############################################################
# Generate input signals
###############################################################
step_signal, rate_signal, time_data = encode_input(Grammar, Seq, Input_Pars_Dict, noise=True)

if pars_dict['Options']['Input_signal'] == 'Rate':
	input_SpkTrains = generate_burst_spktrains(Grammar, Input_Pars_Dict, rate_signal, step_signal, time_data,
	                                           display=False)
	#for idx, n in enumerate(input_SpkTrains):
	#	input_SpkTrains[idx] = n.time_slice(TransientT, TransientT + SimT)

elif pars_dict['Options']['Input_signal'] == 'Frozen':
	input_SpkTrains = generate_frozen_spiketrains(Grammar, Input_Pars_Dict, step_signal, time_data, display=False)
	#input_SpkTrains = input_SpkTrains.time_slice(TransientT, TransientT + SimT)
########################################################################
# Extract all Parameters
########################################################################
for k, v in pars_dict.items():
	globals()[k] = v
	for k1, v1 in (globals()[k]).items():
		globals()[k1] = v1

########################################################################
# Reset NEST kernel
########################################################################
nest.ResetKernel()
nest.SetKernelStatus({'resolution': dt,
                     'print_time': True,
                      'data_path': data_path,
                        'data_prefix': data_label,
                      'overwrite_files': True,
                      'local_num_threads': proc_per_node})
SimT = (stim_dur+I_stim_I)*len(Grammar.alphabet)*nStrings

########################################################################
# 					CREATE NEURONS AND DEVICES
########################################################################
tp_dict = set_topology(pars_dict)
weights = set_synapses(pars_dict)
tp_dict = connect_network(weights, pars_dict, tp_dict)

E_neurons = tp_dict['E_neurons']
I_neurons = tp_dict['I_neurons']

create_inputs(pars_dict, tp_dict, input_SpkTrains)

#-----------------------------------------------------------------------
# Randomize initial Vm
#-----------------------------------------------------------------------
nest.SetStatus(E_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nE))))
nest.SetStatus(I_neurons, 'V_m', np.array(np.random.uniform(low=E_L, high=V_th, size=int(nI))))

#-----------------------------------------------------------------------
# Create and Set Devices
#-----------------------------------------------------------------------
SpkDetector = nest.Create('spike_detector')
nest.SetStatus(SpkDetector, {'record_to': ['file'], 'start': PlasticityT + TransientT,
                                'to_file': True, 'to_memory': False,
	                             'label': 'SpkDetector'})

background_PG = nest.Create('poisson_generator')

nest.SetStatus(background_PG, {'rate': Ext_rate * (pEE * nE)})
#
nest.SetDefaults('multimeter', {'record_from': ['g_ex', 'g_in', 'V_m'],
                                             'record_to': ['memory'], 'to_file': False,
                                             'to_memory': True, 'interval': dt,
                                             'label': 'SingleNeuron_multimeter',
                                             'withtime': True, 'start': 0.})
E_mul = nest.Create('multimeter')
I_mul = nest.Create('multimeter')
E_idx = np.random.randint(min(E_neurons), max(E_neurons), 1)
I_idx = np.random.randint(min(I_neurons), max(I_neurons), 1)
nest.Connect(E_mul, E_idx)
nest.Connect(I_mul, I_idx)
########################################################################
# 							CONNECT
########################################################################
nest.DivergentConnect(background_PG, list(itertools.chain(*[E_neurons, I_neurons])), weight=g_bar_E, delay=delays,
                      model='Ext_Synapse')

nest.ConvergentConnect(list(itertools.chain(*[E_neurons, I_neurons])), SpkDetector)

########################################################################
# Simulate
########################################################################

nest.Simulate(TransientT + SimT)

#########################################################################
# Analysis
#########################################################################

Net_Response = parse_input_specific_spikes(SpkDetector, step_signal, time_data, pars_dict, display=False)
#state_matrix = extract_state_matrix(Net_response, Seq, results_dict, method='filter', t_0=200., t_1=30., display=True)
#Results = dict()

#Results['Net_Response'] = Net_Response
#Results['Full_Seq'] = Seq
#Results['Step_signal'] = step_signal
#Results['time_data'] = time_data
#Results['Input_neurons'] = Input_neurons
#Results['W_in'] = wIN
#Results['input_SpkTrains'] = input_SpkTrains
#pars_dict['Results'] = Results

